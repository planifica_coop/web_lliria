proyectosControllers.controller('ProyectoEditController', function($scope, $rootScope, $location, $routeParams, Proyecto, areas, sigoModal, $upload, $timeout, ProyectoUtils) {
    $scope.areas = areas;
    $scope.areasSelecTipos = [];
    $scope.areasAnyadir = [];
    $scope.areasEliminar = [];
    $scope.tiposAnyadir = [];
    $scope.tiposEliminar = [];
    $scope.imagenesAnyadir = [];
    $scope.imagenesEliminar = [];
    $scope.imagenes = [];
    $scope.imagenesUrl = [];
    $timeout(function() {
        $scope.alertasImagenes.push({
            tipo: 'warning',
            mensaje: 'Las imagenes deben tener un ratio alrededor de 701 x 500.'
        });
    });

    angular.extend($scope, {
        castellon: {
            lat: 39.9874595,
            lng: -0.048063,
            zoom: 10
        },
        markers: {}
    });

    $scope.datepickerOptions = {
        format: 'dd-mm-yyyy',
        language: 'es',
        autoclose: true,
        weekStart: 0
    }

    $scope.anyadirImagen = function($files) {
        console.log('Añadiendo imagen ' + $files[0] + "...");
        FileAPI.getInfo($files[0], function(err, info) {
            if (!err) {
                console.log(info.width, info.height);
                var ratio = info.width / info.height;
                if (info.width > info.height && ratio > 1.3 && ratio < 1.5) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($files[0]);
                    var loadFile = function(fileReader, index) {
                        fileReader.onload = function(e) {
                            $timeout(function() {
                                $scope.imagenesUrl.push(e.target.result);
                            });
                        }
                    }(fileReader);
                    $scope.imagenesAnyadir.push($files[0]);
                } else {
                    $timeout(function() {
                        $scope.alertasImagenes.push({
                            tipo: 'danger',
                            mensaje: 'Las imagenes deben tener un ratio alrededor de 701 x 500.'
                        });
                    });
                }
            } else {
                $timeout(function() {
                    $scope.alertasImagenes.push({
                        tipo: 'danger',
                        mensaje: 'Las imagenes deben tener un ratio alrededor de 701 x 500.'
                    });
                });
            }
        });
        console.log('Imagenes Anyadir', $scope.imagenesAnyadir);
        console.log('Imagenes Eliminar', $scope.imagenesEliminar);
    };

    $scope.eliminarImagen = function(pos, imagen) {
        console.log('Eliminar', $scope.imagenesAnyadir.indexOf(imagen));
        var posAnyadir = $scope.imagenesAnyadir.indexOf(imagen);
        if (posAnyadir > -1) {
            $scope.imagenesAnyadir.splice(posAnyadir, 1);
        } else {
            $scope.imagenesEliminar.push(imagen);
            $scope.imagenes.splice(pos, 1);
        };
        console.log('Imagenes Anyadir', $scope.imagenesAnyadir);
        console.log('Imagenes Eliminar', $scope.imagenesEliminar);
    };

    $scope.updateProyecto = function(proyecto, areasAnyadir, areasEliminar, tiposAnyadir, tiposEliminar, imagenesAnyadir, imagenesEliminar) {
        console.log('Actualizando proyecto ' + proyecto.id + '...');
        console.log(proyecto);

        ProyectoUtils.updateImagenes(proyecto, imagenesAnyadir, imagenesEliminar);
        ProyectoUtils.updateAreas(proyecto, areasAnyadir, areasEliminar)
        ProyectoUtils.updateTipos(proyecto, tiposAnyadir, tiposEliminar);

        proyecto.$update().then(function(response) {
            console.log('Proyecto actualizado.')
            $rootScope.setForm();
            $location.path(CONFIG.URL.PROYECTOS);
        }, function() {
            sigoModal.alerta('No se ha podido guardar el proyecto', 'Aceptar');
        });

    };
    $scope.actualizarArea = function(data, areasSelec, tiposSelec) {
        ProyectoUtils.estaMarcado(data, $scope.areasAnyadir, $scope.areasEliminar);
        // Falta borrar los tipos de esta area
        $scope.actualizarTipos(areasSelec);
    };

    $scope.cancelar = function() {
        $location.path(CONFIG.URL.PROYECTOS);
    };

    $scope.actualizarTipos = function(areasSelec) {
        $scope.areasSelecTipos = [];
        for (area in areasSelec) {
            $scope.areasSelecTipos.push({
                nombre: '<strong>' + areasSelec[area].nombre_es + '</strong>',
                multiSelectGroup: true
            });
            console.log('select', areasSelec[area].id);
            for (tipo in areasSelec[area].tipos) {
                $scope.areasSelecTipos.push(areasSelec[area].tipos[tipo]);

            }
            $scope.areasSelecTipos.push({
                multiSelectGroup: false
            });
        }
        console.log('Areas Anyadir:', $scope.areasAnyadir);
        console.log('Areas Eliminar:', $scope.areasEliminar);
    };

    $scope.actualizarTipo = function(data) {
        ProyectoUtils.estaMarcado(data, $scope.tiposAnyadir, $scope.tiposEliminar);
        console.log('Tipos Anyadir:', $scope.tiposAnyadir);
        console.log('Tipos Eliminar:', $scope.tiposEliminar);
    };

    $scope.alertasImagenes = [];
    $scope.textoBoton = "EDITAR";
    $scope.proyecto = Proyecto.get({
        id: $routeParams.id
    }, function(data) {
        $scope.markers.mainMarker = data;
        $scope.markers.mainMarker.focus = true;
        $scope.markers.mainMarker.draggable = true;
        var areas = [];
        $scope.imagenes = data.imagenes;
        for (i = 0; i < data.areas.length; i++) {
            $.grep($scope.areas, function(e) {
                if (e.id == data.areas[i].id) {
                    e.ticked = true;
                    areas.push(e);
                }
            });
        };
        $scope.actualizarTipos(areas);
        console.log(data.tipos);
        for (i = 0; i < data.tipos.length; i++) {
            $.grep($scope.areasSelecTipos, function(e) {
                if (e.id == data.tipos[i].id) {
                    e.ticked = true;
                }
            });
        };
    });
    $scope.$on("leafletDirectiveMap.click", function(event, args) {
        var leafEvent = args.leafletEvent;
        $scope.markers.mainMarker.lat = leafEvent.latlng.lat;
        $scope.markers.mainMarker.lng = leafEvent.latlng.lng;
    });
    $scope.tipos_cliente = TIPOS_CLIENTE;

    /* Validacion */
    $scope.submitted = false;
});