var proyectosIdi = angular.module('proyectosIdi', ['proyectosIdi.controllers', 'proyectosIdi.services']);

var proyectosIdiControllers = angular.module('proyectosIdi.controllers', ['ngTable', 'utils']);

proyectosIdi.config(function($routeProvider) {
    $routeProvider.when(CONFIG.URL.PROYECTOS_IDI, {
        templateUrl: 'src/app/proyectosIdi/proyectosIdi-list.html',
        controller: 'ProyectoIdiListController'
    }).when(CONFIG.URL.PROYECTOS_IDI + CONFIG.URL.ADD, {
        templateUrl: 'src/app/proyectosIdi/proyectosIdi-add.html',
        controller: 'ProyectoIdiAddController'
    }).when(CONFIG.URL.PROYECTOS_IDI + CONFIG.URL.EDIT + '/:id', {
        templateUrl: 'src/app/proyectosIdi/proyectosIdi-edit.html',
        controller: 'ProyectoIdiEditController'
    })
});