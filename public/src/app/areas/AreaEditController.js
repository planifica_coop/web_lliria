areasControllers.controller('AreaEditController', function($scope, $location, $routeParams, Area, areas, sigoModal) {

    $scope.updateArea = function(area) {
        console.log('Actualizada area.', area);
        area.$update().then(function(response) {
            $location.path(CONFIG.URL.AREAS);
        }, function() {
            sigoModal.alerta('No se ha podido guardar la area', 'Aceptar');
        });
    };

    $scope.cancelar = function() {
        $location.path(CONFIG.URL.AREAS);
    };

    $scope.areas = areas;
    $scope.textoBoton = "EDITAR";
    $scope.area = Area.get({
        id: $routeParams.id
    });
});