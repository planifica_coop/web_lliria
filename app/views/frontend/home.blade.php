@extends('frontend.layout.base')

@section('contenido')

  <section id="slider" class="section-video fullscreen text-light background-overlay"
           data-vide-bg="mp4:{{ url('frontend/video/valenci') }}, poster:{{ url('frontend/video/home_alcala') }}"
           data-vide-options="position: 50% 50%, posterType: jpg">
    <div class="container-fluid container-fullscreen">
      <div class="text-middle text-center text-dark">

        <p class="lead text-lg-lead" style="
              color: ghostwhite !important;
              margin-top: 100px
              ">{{ trans('general.home.subtitulo') }}</p>

        <!-- <h2 class="text-lg m-t-0"
            style="color: #FFF !important;">{{ trans('general.home.titulo') }}</h2><br> -->

        <div class="row"><img class="img-responsive col-md-4 col-md-offset-4" style="margin-top: 70px" src="{{ asset('frontend/img/logo/Logo_Pmus_Lliria_transparente.png') }}" alt="logo prums vinaros"></div><br>
        <!-- <a target="_blank" href="{{ url ('http://mapas.planifica.org/vinaros') }}"
           class="button small transparent rounded" style="width: 365.77px">{{ ucfirst(trans('general.home.boton')) }}</a>
           <br> -->
        <a target="_blank" href="{{ url ('https://docs.google.com/forms/d/1O1JyhrqPpXh3KKtr3H8ShwP8AGCabwf9j5z21_pifGc/viewform?ts=5c7d3d03&edit_requested=true') }}"
           class="button small transparent rounded">{{ ucfirst(trans('general.home.boton_encuesta')) }}</a><br>
        <img class="img-responsive col-md-6 col-md-offset-3" style="margin-top: 70px" src="{{ asset('frontend/img/logo/logo_feder.png') }}" alt="actua almussafes logo"><br>
        <!--<p class="lead text-md-lead col-md-6 col-md-offset-3" style="
              color: ghostwhite !important; margin-top: 150px
              ">{{ trans('general.home.texto') }}</p>-->
      </div>
      <div class="scrolldown-animation" id="scroll-down">
        <a class="" href="#lema"><img src="{{ url('frontend/img/scrolldown.png')
        }}" alt="">
        </a>
      </div>
    </div>
  </section>

  <!-- <section id="lema" class="p-t-80 p-b-80" style="" data-stellar-background-ratio="0.6">
    <div class="container-fluid">
      <p class="lead text-center">{{ trans('general.lema') }}</p>
    </div>
  </section>

  <section id="tramitacion" class="container-fluid" style="padding-top: 0px; padding-left: 30px; padding-right: 30px">

    <div class="heading heading-center m-b-20">
        <h2>{{ strtoupper(trans('general.tramitacion_seccion.titulo')) }}</h2>
    </div>
    <div class="container-fluid">
      <img class="img-responsive" src="{{ asset('frontend/img/tramitacion/Planning_web.png') }}">
    </div>
    
  </section>
  <br>
  <section id="lema_tramitacion" class="p-t-80 p-b-80" style="" data-stellar-background-ratio="0.6">
    <div class="container-fluid">
      <p class="lead text-justify">{{ trans('general.lema_tramitacion') }}</p>
    </div>
  </section> -->

  <script>
    $(function () {
      var heights = $("#tramitacion .col-md-6").map(function () {
                return $(this).height();
              }).get(),
              maxHeight = Math.max.apply(null, heights);

      $("#tramitacion .col-md-6").each(function () {
        $(this).height(maxHeight + 0 + "px");
      });

      function scroolTo(target) {
        var $target = $(target);

        $('html, body').stop().animate({
          'scrollTop': $target.offset().top - 100 + 'px'
        }, 900, 'swing', function () {
          window.location.hash = target;
        });
      }

      if (window.location.hash) {
        scroolTo(window.location.hash);
      }
      $('a[href^="#"]').on('click', function (e) {
        console.log($(this));
        if (!$(this).hasClass('gototop')) {
          e.preventDefault();
          scroolTo(this.hash);
        }
      });
    });
  </script>
@endsection

