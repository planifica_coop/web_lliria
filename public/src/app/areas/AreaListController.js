areasControllers.controller('AreaListController', function($scope, $location, Area, ngTableParams, sigoModal, $filter) {

    $scope.nuevaArea = function() {
        $location.path(CONFIG.URL.AREAS + CONFIG.URL.ADD);
    };

    $scope.editarArea = function(area) {
        $location.path(CONFIG.URL.AREAS + CONFIG.URL.EDIT + '/' + area.id);
    };

    $scope.eliminarArea = function(area, posicion) {
        sigoModal.confirmar('¿Desea borrar el area ' + area.nombre_es + '?', 'Sí', 'No')
            .then(function() {
                    Area.delete({
                        id: area.id
                    }, function() {
                        console.log('Area ' + areas.id + ' eliminado.');
                        $scope.areas.splice($scope.areas.indexOf(areas), 1);
                        $scope.tableParams.reload();
                    }, function() {
                        sigoModal.alerta('No se ha podido eliminar el area', 'Aceptar');
                    });
                },
                function() {
                    console.log('cancel');
                });

    };

    $scope.actualizarBusqueda = function() {
        $scope.tableParams.reload();
    }

    $scope.areas = Area.query(function() {
        $scope.tableParams.reload();
    });

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10 // count per page
    }, {
        total: $scope.areas.length, // length of data
        getData: function($defer, params) {
            var filteredData = $filter('filter')($scope.areas, $scope.busqueda);
            var orderedData = params.sorting() ?
                $filter('orderBy')(filteredData, params.orderBy()) :
                filteredData;
            var total = orderedData.length;
            params.total(total);
            if (total <= params.count())
                params.page(1);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
});