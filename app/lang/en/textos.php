<?php

return array(
	'sistema_calidad_texto'     => 'PLANIFICA realiza un esfuerzo por la mejora continua de sus servicios, así el equipo de trabajo aplica la metodología, procedimientos, documentación y forma de trabajo en general establecida por la normativa de calidad ISO 9.001:2008.',
	'i_d_i_texto'     => 'PLANIFICA tiene como uno de sus principales objetivos el impulso de proyectos de I+D+i. Dentro de las actuaciones destaca la publicación de artículos científicos, la participación en congresos y foros o la colaboración con universidades públicas.',
	'selecciona_pagina'     => 'please select your page',
	'selecciona_categoria'     => 'select category',
	'oficina_central_mas_info'     => 'Use the map above to get more information about our main office.',
	'seleccionar_categoria'     => 'seleccionar categoría',
	'monitorizacion_consumos_texto'     => 'otro de los campos en el cual Planifica está desarrollando nuevas aplicaciones es la monitorización de consumos eléctricos, hídricos o en general energéticos.',
	'consumo_electrico_planifica'     => 'consumo eléctrico de las oficinas de Planifica.',
	'urbanismo_ordenacion'     => '<p>Todo tipo de instrumentos de planeamiento y gestión del territorio:</p>
        <ul class="disc">
          <li>Planes de accion territorial, integrados y sectoriales.</li>
          <li>Planes generales de ordenación urbana de municipios.</li>
          <li>Instrumentos de desarrollo del planeamiento municipal (planes parciales, planes especiales, planes de reforma interior, estudios de detalle, etc.)</li>
          <li>Planes directores de infraestructuras.</li>
        </ul>
        <p>Además ofrece servicios relacionados con la gestión del suelo:</p>
        <ul class="disc">
          <li>Asesoramiento urbanístico y técnico a administraciones públicas, promotores privados, constructoras, entidades financieras, sociedades de inversión, etc.</li>
          <li>Estudios de viabilidad técnica, jurídica y económica de actuaciones integrales de desarrollo del suelo (desde el análisis previo y el master plan hasta los proyectos edificatorios finales, incluso la gestión de la contratación de la obra y project managment durante la misma).</li>
        </ul>',
	'ingenieria_civil'     => '<ul class="disc">
          <li>Proyectos de Urbanización, tanto de nuevos desarrollos como de remodelación de espacios urbanos existentes.</li>
          <li>Estudios de Tráfico.</li>
          <li>Estudios y proyectos de infraestucturas de obras públicas en el ámbito urbano (depósitos de aguas; depuradoras de agua; colectores; instalaciones de alumbrado público; calles, caminos y accesos; plazas y parques; etc.)</li>
        </ul>
        <p>Además ofrece asesoramiento en contratación de obras, direcciones de obra, coordinaciones de seguridad y salud, etc.</p>',
	'ingenieria_ambiental'     => '<ul class="disc">
          <li>Evaluación Ambiental Estratégica, Estudios de Impacto Ambiental, Estudios de Paisaje, Plan de Participación Ciudadana, etc.</li>
          <li>Estudios de Inundabilidad, Estudios Hidrológicos e Hidráulicos de cauces y canalizaciones, Estudios de Sistemas Urbanos de Drenaje Sostenible, Estudios de Recursos Hídricos y Ciclo Integral del Agua.</li>
          <li>Movilidad urbana sostenible.</li>
          <li>Estudios Acústicos.</li>
        </ul>',
	'arquitectura'     => '<ul class="disc">
          <li>Proyecto edificatorio de todo tipo y en toda su extensión (viviendas, industriales, comerciales, administrativos, deportivos,…): anteproyectos, proyectos básicos, proyectos de ejecución.</li>
          <li>Estudio de solares y viabilidad económica de promociones.</li>
          <li>Direcciones de obra y coordinaciones de seguridad y salud.</li>
          <li>Gestión integral de construcción y promoción. Para determinados clientes se ofrece un servicio integral, ejerciendo la figura de promotor y/o constratista principal de la obra.</li>
        </ul>',
	'ingenieria_industrial'     => '<ul class="disc">
          <li>Diseño y cálculo de las instalaciones eléctricas tanto de los edificios cómo de las urbanizaciones.</li>
          <li>Proyectos de las instalaciones de los edificios (intalación eléctrica, climatización, aguas caliente sanitaria, saneamiento, ventilación, telecomunicaciones, evacuación de pluviales, etc.)</li>
          <li>Proyectos de actividad, tramitación de todos los permisos y licencias de actividades e instalaciones.</li>
          <li>Estudios de eficiencia energética en edificaciones y urbanizaciones.</li>
        </ul>
        <p>Además, Planifica colabora habitualmente con empresas consultoras del mundo de la arquitectura y la ingeniería especializadas en medio ambiente, carreteras, hidráulica, depuradoras y estructuras, etc., cuando es necesario por la dificultad y especialización requerida de algunos proyectos. Igualmente trabaja en U.T.E. con otras consultoras de su mismo ámbito de trabajo para desarrollar proyectos emblemáticos o de amplia duración y exigencia.</p>',
        'no_encontrado_mensaje'     => 'The page you are looking for seems to be missing. Go back, or return to planifica.org to choose a new direction.',
);
