var tipos = angular.module('tipos', ['tipos.controllers', 'tipos.services']);

var tiposControllers = angular.module('tipos.controllers', ['ngTable', 'utils']);

tipos.config(function($routeProvider) {
    $routeProvider.when(CONFIG.URL.TIPOS, {
        templateUrl: 'src/app/tipos/tipos-list.html',
        controller: 'TipoListController'
    }).when(CONFIG.URL.TIPOS + CONFIG.URL.ADD, {
        templateUrl: 'src/app/tipos/tipos-add.html',
        controller: 'TipoAddController',
        resolve: {
            areas: function(Area) {
                return Area.query();
            }
        }
    }).when(CONFIG.URL.TIPOS + CONFIG.URL.EDIT + '/:id', {
        templateUrl: 'src/app/tipos/tipos-edit.html',
        controller: 'TipoEditController',
        resolve: {
            areas: function(Area) {
                return Area.query();
            }
        }
    })
});