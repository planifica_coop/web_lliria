@extends('base')
@section('content')
@include('barra-navegacion')
<!-- Region 4 Wrap -->
<div class="container region4wrap">
  <div class="row maincontent">
    <div class="twelve columns">
      <div class="row">
        <div class="eight columns centered">
          <h1 class="notfound_title">404</h1>
          <h2 class="notfound_subtitle">{{ ucfirst(trans('nombres.no_encontrado_error')) }}</h2>
        </div>
      </div>
    </div>
    <div class="twelve columns">
      <div class="row">
        <div class="six columns centered">
          <p class="notfound_description">{{ ucfirst(trans('textos.no_encontrado_mensaje')) }}</p>
        </div>
      </div>
    </div>
    <div class="twelve columns">
      <div class="row">
        <div class="four columns centered">
          <a class="button expand bottom20" href="javascript: history.go(-1)"><i class="icon-undo"></i>{{ ucfirst(trans('nombres.volver_anterior')) }}</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Region 4 Wrap -->
@stop