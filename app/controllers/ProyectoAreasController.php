<?php
class ProyectoAreasController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($proyectoId) {
        return Proyecto::find($proyectoId)->areas()->with('tipos')->get();
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($proyectoId) {
        $proyectoAreas = new ProyectoArea;
        $proyectoAreas->id_pla_proyectos = $proyectoId;
        $proyectoAreas->id_pla_areas = Input::get('id_pla_areas');
        $proyectoAreas->save();
        return $proyectoAreas;
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($proyectoId, $areaId) {
        return Proyecto::find($proyectoId)->areas()->where('id_pla_areas', $areaId);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($proyectoId, $areaId) {
        $proyectoAreas = ProyectoArea::where('id_pla_proyectos', $proyectoId)->where('id_pla_areas', $areaId)->delete();
    }
}
