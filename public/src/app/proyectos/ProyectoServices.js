var proyectosServices = angular.module('proyectos.services', ['ngResource']);

proyectosServices.factory('Proyecto', function($resource) {
    return $resource(CONFIG.URL.API + '/proyectos/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        addArea: {
            method: 'POST',
            url: CONFIG.URL.API + '/proyectos/:id/areas/'
        },
        deleteArea: {
            method: 'DELETE',
            url: CONFIG.URL.API + '/proyectos/:id/areas/:idArea'
        },
        addTipo: {
            method: 'POST',
            url: CONFIG.URL.API + '/proyectos/:id/tipos/'
        },
        deleteTipo: {
            method: 'DELETE',
            url: CONFIG.URL.API + '/proyectos/:id/tipos/:idTipo'
        },
        addImagen: {
            method: 'POST',
            url: CONFIG.URL.API + '/proyectos/:id/imagenes/'
        },
        deleteImagen: {
            method: 'DELETE',
            url: CONFIG.URL.API + '/proyectos/:id/imagenes/:idImagen'
        }
    });
});