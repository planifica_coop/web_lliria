<!DOCTYPE html>
<html lang="en" ng-app="app">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Admin - Planifica</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{URL::to('vendor/sb-admin-2/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="{{URL::to('vendor/sb-admin-2/css/plugins/metisMenu/metisMenu.min.css') }}" rel="stylesheet">
    <!-- Timeline CSS -->
    <link href="{{URL::to('vendor/sb-admin-2/css/plugins/timeline.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{URL::to('vendor/sb-admin-2/css/sb-admin-2.css') }}" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="{{URL::to('vendor/sb-admin-2/font-awesome-4.1.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Angular Multi Select -->
    <link href="{{URL::to('vendor/angular-multi-select/angular-multi-select.css') }}" rel="stylesheet">
    <!-- leaflet -->
    <link rel="stylesheet" href="{{URL::to('vendor/leaflet/dist/leaflet.css') }}" />
    <link rel="stylesheet" href="{{URL::to('vendor/nvd3/nv.d3.min.css') }}" />
    <link href="{{URL::to('src/assets/css/style.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="{{URL::to('vendor/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css') }}" rel="stylesheet">
    <link href="{{URL::to('vendor/angular-bootstrap-datepicker/dist/angular-bootstrap-datepicker.css') }}" rel="stylesheet">
  </head>
  <body>
    <div id="wrapper">
      <panel-navegacion></panel-navegacion>
      <div id="page-wrapper">
        <div class="row" ng-show="loadingView">
          <div class="col-md-2 col-md-offset-5" style="padding:100px;">
            <i class="fa fa-spinner fa-spin fa-5x"></i>
            <span translate>CARGANDO</span>...
          </div>
          <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div ng-view ng-hide="loadingView" class="animate-repeat">
        </div>
      </div>
      <!-- /page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery Version 1.11.0 -->
    <script src="{{URL::to('vendor/sb-admin-2/js/jquery-1.11.0.js') }}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{URL::to('vendor/sb-admin-2/js/bootstrap.min.js') }}"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{URL::to('vendor/sb-admin-2/js/plugins/metisMenu/metisMenu.min.js') }}"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{URL::to('vendor/sb-admin-2/js/sb-admin-2.js') }}"></script>
    <!-- Angular -->
    <script type="text/javascript" src="{{URL::to('vendor/angular/angular.min.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('vendor/angular-route/angular-route.min.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('vendor/angular-resource/angular-resource.min.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('vendor/angular-animate/angular-animate.min.js') }}"></script>
    <!-- ng-table -->
    <script type="text/javascript" src="{{URL::to('vendor/ng-table/ng-table.min.js') }}"></script>
    <!-- leaflet -->
    <script type="text/javascript" src="{{URL::to('vendor/leaflet/dist/leaflet.js') }}"></script>
    <!-- angular-leaflet -->
    <script src="{{URL::to('vendor/angular-leaflet-directive/dist/angular-leaflet-directive.min.js') }}"></script>
    <!-- Text Angular -->
    <script src="{{URL::to('vendor/textAngular/dist/textAngular.min.js') }}"></script>
    <script src="{{URL::to('vendor/textAngular/dist/textAngular-sanitize.min.js') }}"></script>
    <!-- Angular Multi Select -->
    <script src="{{URL::to('vendor/angular-multi-select/angular-multi-select.js') }}"></script>
    <!-- angular-file-upload -->
    <script src="{{URL::to('vendor/danialfarid-angular-file-upload/dist/angular-file-upload.min.js') }}"></script>
    <script src="{{URL::to('vendor/danialfarid-angular-file-upload/dist/FileAPI.min.js') }}"></script>
    <!-- Angular Translate -->
    <script src="{{URL::to('vendor/angular-translate/angular-translate.min.js') }}"></script>
    <script src="{{URL::to('vendor/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js') }}"></script>
    <script src="{{URL::to('vendor/angular-translate-loader-url/angular-translate-loader-url.min.js') }}"></script>
    <!-- angularjs-nvd3-directives -->
    <script src="{{URL::to('vendor/d3/d3.min.js') }}" charset="utf-8"></script>
    <script type="text/javascript" src="{{URL::to('vendor/nvd3/nv.d3.min.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('vendor/angularjs-nvd3-directives/dist/angularjs-nvd3-directives.min.js') }}"></script>
    <script type="text/javascript" src="vendor/bootstrap-switch/dist/js/bootstrap-switch.js"></script>
    <script type="text/javascript" src="{{URL::to('vendor/angular-bootstrap-datepicker/dist/angular-bootstrap-datepicker.js') }}" charset="utf-8"></script>
    <!-- app -->
    <script type="text/javascript">
            var global = {
                "userEmail" : "{{ $userEmail }}"
            };
            var CONFIG = {
                URL: {
                    AREAS: '/areas',
                    PROYECTOS: '/proyectos',
                    TIPOS: '/tipos',
                    ARTICULOS: '/articulos',
                    PROYECTOS_IDI: '/proyectos_idi',
                    MARCADORES: '/marcadores',
                    VIEW: '/view',
                    ADD: '/add',
                    EDIT: '/edit',
                    API: "{{URL::to('api/v1')}}"
                }
            };
    </script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>
    <script type="text/javascript" src="{{URL::to('src/app/app.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/resumen/resumen.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/areas/areas.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/areas/AreaListController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/areas/AreaAddController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/areas/AreaEditController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/areas/AreaServices.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/tipos/tipos.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/tipos/TipoListController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/tipos/TipoAddController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/tipos/TipoEditController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/tipos/TipoServices.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/proyectos/proyectos.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/proyectos/ProyectoListController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/proyectos/ProyectoAddController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/proyectos/ProyectoEditController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/proyectos/ProyectoDetailsController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/proyectos/ProyectoServices.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/proyectos/ProyectoUtils.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/articulos/articulos.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/articulos/ArticuloListController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/articulos/ArticuloAddController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/articulos/ArticuloEditController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/articulos/ArticuloServices.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/proyectosIdi/proyectosIdi.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/proyectosIdi/ProyectoIdiListController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/proyectosIdi/ProyectoIdiAddController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/proyectosIdi/ProyectoIdiEditController.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/app/proyectosIdi/ProyectoIdiServices.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/common/common.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/common/sigo-bootstrap/SigoModal.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/common/sigo-bootstrap/SigoAlert.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('src/common/paneles/PanelesDirective.js') }}"></script>
  </body>
</html>