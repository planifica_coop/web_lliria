var formUtils = angular.module('FormUtils', ['sigoModal']);

formUtils.factory('validacionUtils', function() {
    return {
        isIncorrecto: function(campo, submitted) {
            return campo.$invalid && (campo.$dirty || submitted);
        },
        isIncorrectoModelo: function(modelo, requerido, submitted) {
            return requerido && submitted && ((modelo == undefined) ? false : !modelo.length && true);
        },
        isCorrecto: function(campo, modelo) {
            return (modelo == undefined) ? false : campo.$valid && modelo;
        }
    };
});