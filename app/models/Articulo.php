<?php
class Articulo extends Eloquent
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pla_articulos';

    protected $fillable = array('nombre_es', 'nombre_en', 'nombre_fr', 'titulo_es', 'titulo_en', 'titulo_fr', 'autores', 'fecha', 'id_pla_adjuntos');
    
    public function adjunto() {
        return $this->belongsTo('Adjunto', 'id_pla_adjuntos');
    }
    
    public function getNombre() {
        if (App::getLocale() == 'es') {
            return $this->nombre_es;
        } elseif (App::getLocale() == 'en') {
            return $this->nombre_en;
        } elseif (App::getLocale() == 'fr') {
            return $this->nombre_fr;
        }
    }
    
    public function getTitulo() {
        if (App::getLocale() == 'es') {
            return $this->titulo_es;
        } elseif (App::getLocale() == 'en') {
            return $this->titulo_en;
        } elseif (App::getLocale() == 'fr') {
            return $this->titulo_fr;
        }
    }
}
