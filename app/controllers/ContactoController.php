<?php
class ContactoController extends BaseController
{
    
    public function getIndex() {
        return View::make('contacto');
    }
    
    public function postContacto() {
        try {
            $data = Input::all();
            Mail::send('emails.contacto', $data, function ($message) {
                $message->to(Input::get('email'), Input::get('nombre'))->subject(Input::get('asunto'));
            });
            return 1;
        }
        catch(\Exception $e) {
            return 0;
        }
    }
}
