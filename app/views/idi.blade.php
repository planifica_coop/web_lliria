@extends('base')
@section('content')
@include('barra-navegacion')
<!-- Region 4 Wrap -->
<!-- Region 4 Wrap -->
<div class="container region4wrap">
  <div class="row maincontent">
    <div class="twelve columns">
      <div class="page_title">
        <div class="row">
          <div class="twelve columns">
            <h1>{{ ucfirst(trans('nombres.i_d_i')) }}</h1>
          </div>
        </div>
      </div>
    </div>
    <div class="twelve columns">
      {{ ucfirst(trans('textos.i_d_i_texto')) }}
      <div class="row">
        <div class="twelve columns top20 bottom20">
          <!-- Tabs -->
          <dl class="tabs four-up"><!-- Tabs - navigation -->
          <dd class="active"><a href="#articulos">{{ ucfirst(trans('nombres.articulos_publicados_planifica')) }}</a></dd>
          <dd><a href="#proyectos_idi">{{ ucfirst(trans('nombres.proyectos_idi')) }}</a></dd>
          <dd><a href="#aplicaciones_web">{{ ucfirst(trans('nombres.aplicaciones_web')) }}</a></dd>
          <dd><a href="#monitorizacion">{{ ucfirst(trans('nombres.monitorizacion_consumos')) }}</a></dd>
          </dl><!-- End Tabs - navigation -->
          <ul class="tabs-content"><!-- Tabs - content -->
          <!-- Item Tab Content -->
          <li class="active" id="articulosTab">
            <div class="row">
              <div class="twelve columns">
                <ol class="ol-type1">
                  @foreach ($articulos as $articulo)
                  <li>
                    <strong>{{ $articulo->getNombre() }}</strong><br>
                    {{ $articulo->fecha }}<br>
                    Título del artículo: <a @if ($articulo->adjunto != null) href="{{ URL::to($articulo->adjunto->ruta) }}" @endif target="_blank">
                    {{ $articulo->getTitulo() }}
                    </a><br>
                    Autores: {{ $articulo->autores }}
                  </li>
                  @endforeach
                </ol>
              </div>
            </div>
          </li>
          <!-- End Item Tab Content -->
          <!-- Item Tab Content -->
          <li id="proyectos_idiTab">
            <div class="row">
              <div class="twelve columns">
                <ol class="ol-type1">
                  @foreach ($proyectosIdi as $proyectoIdi)
                  <li>
                    <strong>{{ $proyectoIdi->getNombre() }}</strong><br>
                    <a>{{ $proyectoIdi->getDescripcion() }}</a>
                  </li>
                  @endforeach
                </ol>
              </div>
            </div>
          </li>
          <!-- End Item Tab Content -->
          <!-- Item Tab Content -->
          <li class="active" id="aplicaciones_webTab">
            <div class="row">
              <div class="twelve columns">
                <ol class="ol-type1">
                  <li>
                    <strong>Sigo</strong>
                  </li>
                  <li>
                    <strong>Mapifica</strong>
                  </li>
                </ol>
              </div>
            </div>
          </li>
          <!-- End Item Tab Content -->
          <li id="monitorizacionTab">
            <div class="row">
              <div class="twelve columns">
              </div>
            </div>
            <div class="row">
              <div class="four columns">
                <h3>{{ ucfirst(trans('nombres.consumo_electrico_planifica')) }}</h3>
                <br>
                <div style="text-align:center;">
                  <OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" WIDTH="200" HEIGHT="100" id="gauge"><PARAM NAME="movie" VALUE="http://apps.pachube.com/scaredycat/gauge.swf?xml_source=http%3A//apps.pachube.com/scaredycat/getData.php%3Fm%3D0%26f%3D76467%26s%3D1%26u%3D7000%26l%3D1000%26n%3D5%26t%3DPlanifica%20%28W%29%26w%3Dtrue%26c1%3D33FF33%26c2%3DEFE415%26c3%3DEF8B15%26c4%3DFF3333%26in%3Dfalse" /><PARAM NAME="quality" VALUE="high" /><param name="wmode" value="transparent"><param name="allowScriptAccess" value="sameDomain" /><EMBED src="http://apps.pachube.com/scaredycat/gauge.swf?xml_source=http%3A//apps.pachube.com/scaredycat/getData.php%3Fm%3D0%26f%3D76467%26s%3D1%26u%3D7000%26l%3D1000%26n%3D5%26t%3DPlanifica%20%28W%29%26w%3Dtrue%26c1%3D33FF33%26c2%3DEFE415%26c3%3DEF8B15%26c4%3DFF3333%26in%3Dfalse" quality="high" wmode="transparent" WIDTH="200" HEIGHT="100" NAME="gauge" allowScriptAccess="sameDomain" swLiveConnect="true" TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer"></EMBED></OBJECT>
                </div>
              </div>
              <div class="eight columns">
                <br>
                <p>{{ ucfirst(trans('textos.monitorizacion_consumos_texto')) }}</p>
                <div>
                  <script type="text/javascript" src="http://www.google.com/jsapi"></script><script language="JavaScript" src="http://apps.pachube.com/google_viz/viz.js"></script><script language="JavaScript">createViz(76467,"1",500,200,"FF6600");</script>
                </div>
                <br />
              </div>
              <br />
            </div>
          </div>
        </div>
      </div>
    </li>
    </ul><!-- End Tabs - content -->
    <!-- End Tabs -->
  </div>
</div>
</div>
<!-- End Region 4 Wrap -->
@stop