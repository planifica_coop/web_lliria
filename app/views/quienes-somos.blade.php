@extends('base')
@section('content')
@include('barra-navegacion')
<!-- Region 4 Wrap -->
<div class="container region4wrap">
  <div class="row maincontent">
    <div class="twelve columns">
      <div class="page_title">
        <div class="row">
          <div class="twelve columns">
            <h1>{{ ucwords(str_replace("<br>"," ",trans('nombres.quienes_somos'))); }}</h1>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="twelve columns">
        <p>{{ ucfirst(trans('textos.quienes_somos')) }}</p>
      </div>
    </div>
    <div class="row">
      <div class="six columns">
        <h3>{{ strtoupper(trans('nombres.urbanismo_ordenacion')) }}</h3>
        {{ trans('textos.urbanismo_ordenacion') }}
      </div>
      <div class="six columns">
        <h3>{{ strtoupper(trans('nombres.ingenieria_civil')) }}</h3>
        {{ trans('textos.ingenieria_civil') }}
      </div>
      <div class="six columns">
        <h3>{{ strtoupper(trans('nombres.ingenieria_ambiental')) }}</h3>
        {{ trans('textos.ingenieria_ambiental') }}
      </div>
      <div class="six columns">
        <h3>{{ strtoupper(trans('nombres.arquitectura')) }}</h3>
        {{ trans('textos.arquitectura') }}
      </div>
      <div class="six columns">
        <h3>{{ strtoupper(trans('nombres.ingenieria_industrial')) }}</h3>
        {{ trans('textos.ingenieria_industrial') }}
      </div>
    </div>
  </div>
</div>
<!-- End Region 4 Wrap -->
@stop