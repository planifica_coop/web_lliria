proyectosIdiControllers.controller('ProyectoIdiEditController', function($scope, $rootScope, $location, $routeParams, ProyectoIdi, sigoModal) {

    $scope.updateProyectoIdi = function(proyectoIdi) {
        console.log('Actualizado proyectoIdi.');
        proyectoIdi.$update().then(function(response) {
            $location.path(CONFIG.URL.PROYECTOS_IDI);
        }, function() {
            sigoModal.alerta('No se ha podido guardar el indicador', 'Aceptar');
        });
    };

    $scope.cancelar = function() {
        $rootScope.setForm();
        $location.path(CONFIG.URL.PROYECTOS_IDI);
    };

    $scope.textoBoton = "EDITAR";
    $scope.proyectoIdi = ProyectoIdi.get({
        id: $routeParams.id
    });
    /* Validacion */
    $scope.submitted = false;
});