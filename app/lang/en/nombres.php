<?php

return array(
	'inicio' => 'home',
	'quienes_somos'     => 'who we<br>are',
	'nuestros_servicios'     => 'our <br>services',
	'internacionalizacion'     => 'internationalization',
	'sistema_calidad'     => 'quality <br>system',
	'i_d_i'     => 'R+D+i',
	'contacto'     => 'contact',
	'envianos_un_mensaje'     => 'send us a message',
	'nombre'     => 'name',
	'email'     => 'e-mail',
	'asunto'     => 'subject',
	'mensaje'     => 'message',
	'enviar'     => 'send',
	'todos'     => 'all',
	'sobre_nosotros'     => 'about us',
	'oficina_central'     => 'head office',
	'direccion'     => 'adress',
	'telefono'     => 'phone',
	'seleccionar_categoria'     => 'seleccionar categoría',
	'articulos_publicados_planifica'     => 'published articles',
	'proyectos_idi'     => 'proyectos de I+D+i',
	'monitorizacion_consumos'     => 'monitorización de consumos',
	'consumo_electrico_planifica'     => 'consumo eléctrico de las oficinas de Planifica.',
	'urbanismo_ordenacion'     => 'urbanismo y ordenación del territorio',
	'ingenieria_civil'     => 'ingeniería civil',
	'ingenieria_ambiental'     => 'ingeniería ambiental',
	'arquitectura'     => 'arquitectura',
	'ingenieria_industrial'     => 'ingeniería industrial',
	'trabajo_reciente'     => 'recent work',
	'nuestros_partners'     => 'our partners',
	'proyecto'     => 'project',
	'aplicaciones_web'     => 'web applications',
	'error'     => 'error',
	'no_encontrado_error'     => 'oops...are you lost?',
	'volver_anterior'     => 'return to previous page'
	
	

);