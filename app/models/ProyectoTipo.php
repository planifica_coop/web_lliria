<?php
class ProyectoTipo extends Eloquent
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pla_proyectos_tipos';
    
    public function proyecto() {
        return $this->hasOne('Proyecto', 'id_pla_proyectos');
    }
    
    public function tipo() {
        return $this->hasOne('Tipo', 'id_pla_tipos');
    }
}
