@extends('frontend.layout.base')

@section('contenido')

  <section id="page-title" class="page-title-video text-light"
           data-vide-bg="mp4:{{ url('frontend/video/madrid') }}, poster:{{ url('frontend/video/madrid_poster') }}"
           data-vide-options="position: 0% 50%"
           style="background: rgba(80,99,128,0.60);">
    <div class="container">
      <div class="page-title col-md-8">
        {{--<h1 style="text-transform: none;">Innova</h1>--}}
      </div>
      <div class="col-md-12">
        <h4 class="text-center" style="font-size: 2.5rem;font-weight: 100;">{{ trans('general.innovacion_seccion.subtitulo') }}</h4>
      </div>
    </div>
  </section>

  <section class="p-t-40">
    <div class="container">
      <div id="tabs-05c" class="tabs color">
        <p class="lead">{{ trans('general.innovacion_seccion.texto') }}</p>
        <ul class="tabs-navigation">
          <li class="active"><a href="#articulos">{{ trans('general.innovacion_seccion.articulos') }}</a></li>
          <li><a href="#proyectos_europeos">{{ trans('general.innovacion_seccion.proyectos_europeos') }}</a></li>
          <li><a href="#colaboracion_universidad">{{ trans('general.innovacion_seccion.colaboracion_universidad') }}</a></li>
        </ul>
        <div class="tabs-content">
          <div class="tab-pane active" id="articulos">
            <ol class="ol-type1">
              @foreach ($articulos as $articulo)
                <li>
                  <strong>{{ $articulo->getNombre() }}</strong><br>
                  {{ $articulo->fecha }}<br>
                  Título del artículo: <a @if ($articulo->adjunto != null) href="{{ URL::to($articulo->adjunto->ruta) }}" @endif target="_blank">
                    {{ $articulo->getTitulo() }}
                  </a><br>
                  Autores: {{ $articulo->autores }}
                </li>
              @endforeach
            </ol>
          </div>
          <div class="tab-pane" id="proyectos_europeos">
            <h4>{{ trans('general.innovacion_seccion.proyectos_europeos') }}</h4>
            <ol class="ol-type1">
              <li>
                <strong>BLUECITY [INTERREG IVC].</strong><br>
                Sistema de indicadores para la evaluación de la gestión del ciclo integral del agua en entornos urbanos.
              </li>
              <li>
                <strong>AQUAVAL [Programa Life+].</strong><br>
                La gestión eficiente del agua de lluvia en entornos urbanos (<a target="_blank" href="http://www.aquavalproject.eu/">www.aquavalproject.eu/</a>).

              </li>
              <li>
                <strong>E2Stormed [Programa MED].</strong><br>
                Mejora de la eficiencia energética en el ciclo del agua en ciudades mediterráneas mediante el uso de tecnologías
                innovadoras para la gestión del agua de lluvia (<a target="_blank" href="http://www.e2stormed.eu/">www.e2stormed.eu</a>).
              </li>
            <ol>
          </div>
          <div class="tab-pane" id="colaboracion_universidad">
            <ol class="ol-type1">
              @foreach ($proyectosIdi as $proyectoIdi)
                <li>
                  <strong>{{ $proyectoIdi->getNombre() }}</strong><br>
                  <a>{{ $proyectoIdi->getDescripcion() }}</a>
                </li>
              @endforeach
            </ol>
          </div>
        </div>
      </div>
    </div>
  </section>

@endsection