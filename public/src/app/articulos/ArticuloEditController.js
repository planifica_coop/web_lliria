articulosControllers.controller('ArticuloEditController', function($scope, $rootScope, $location, $routeParams, Articulo, sigoModal, Adjunto, $upload) {

    $scope.updateArticulo = function(articulo, adjunto, adjuntoEliminar) {
        console.log('Actualizado articulo.', adjunto, adjuntoEliminar);
        if (adjuntoEliminar != null) {
            Adjunto.delete({
                id: adjuntoEliminar.id
            }, function() {
                console.log('Adjunto ' + adjuntoEliminar.id + ' eliminado.');
            });
        }
        if (adjunto != null) {
            console.log('No es null');
            var file = adjunto;
            $upload.upload({
                url: CONFIG.URL.API + '/adjuntos',
                file: file,
                fileFormDataName: 'adjunto',
            }).success(function(data, status, headers, config) {
                // file is uploaded successfully
                console.log('Añadido Adjunto ' + data.id + ".");
                articulo.id_pla_adjuntos = data.id;
                articulo.$update().then(function(response) {
                    $rootScope.setForm();
                    $location.path(CONFIG.URL.ARTICULOS);
                }, function() {
                    sigoModal.alerta('No se ha podido guardar el artículo', 'Aceptar');
                });

            });
        } else {
            articulo.id_pla_adjuntos = null;
            articulo.$update().then(function(response) {
                $location.path(CONFIG.URL.ARTICULOS);
            }, function() {
                sigoModal.alerta('No se ha podido guardar el artículo', 'Aceptar');
            });
        }
    };

    $scope.cancelar = function() {
        $location.path(CONFIG.URL.ARTICULOS);
    };

    $scope.anyadirAdjunto = function($files) {
        $scope.adjunto = $files[0];
        $scope.adjuntoNombre = $files[0].name;
    };

    $scope.eliminarAdjunto = function(adjunto) {
        if ($scope.adjunto == null) {
            $scope.adjuntoEliminar = adjunto;
            $scope.articulo.id_pla_adjuntos = null;
        } else {
            $scope.adjunto = null;
            $scope.adjuntoNombre = "";
        }
    };

    $scope.adjunto = null;
    $scope.adjuntoEliminar = null;
    $scope.adjuntoNombre = "";
    $scope.textoBoton = "EDITAR";
    $scope.articulo = Articulo.get({
        id: $routeParams.id
    });
});