var app = angular.module('app', ['ngRoute', 'pascalprecht.translate', 'areas', 'proyectos', 'tipos', 'articulos', 'proyectosIdi', 'resumen', 'paneles', 'ngAnimate']);

app.config(function($routeProvider) {
    $routeProvider.otherwise({
        redirectTo: '/'
    });
});

app.config(function($logProvider) {
    $logProvider.debugEnabled(false);
});

app.config(function($translateProvider) {
    $translateProvider.useStaticFilesLoader({
        prefix: 'src/i18n/locale-',
        suffix: '.json'
    }).registerAvailableLanguageKeys(['es', 'en'], {
        'en_US': 'en',
        'en_UK': 'en',
        'es_ES': 'es',
        'es_MX': 'es'
    }).determinePreferredLanguage();
});

app.filter('capitalize', function() {
    return function(input, scope) {
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }
});

app.run(
    function($rootScope, $timeout, $location, sigoModal) {

        $rootScope.isIncorrecto = function(campo, submitted) {
            return campo.$invalid && (campo.$dirty || submitted);
        };
        $rootScope.isIncorrectoModelo = function(modelo, requerido, submitted) {
            return requerido && submitted && ((modelo == undefined) ? false : !modelo.length && true);
        };
        $rootScope.isCorrecto = function(campo, modelo) {
            return (modelo == undefined) ? false : campo.$valid && modelo;
        };

        function cambiarRuta(evento, siguienteUrl) {
            if (!$rootScope.form.$dirty) return;
            evento.preventDefault();
            sigoModal.confirmar('Se perderán los datos no guardados, quieres continuar?', 'Sí', 'No')
                .then(function() {
                    $rootScope.onRouteChangeOff();
                    $location.path(siguienteUrl.split('#')[1]);
                });
            return;
        };
        $rootScope.setForm = function(formulario) {
            $rootScope.form = formulario;
            $rootScope.onRouteChangeOff = $rootScope.$on('$locationChangeStart', cambiarRuta);
            /* Validacion */
            $rootScope.submitted = false;
        };

        $rootScope.$on('$routeChangeStart', function(e, curr, prev) {
            // console.log('Route Change Start!!');
            if (curr.$$route && curr.$$route.resolve) {
                // Show a loading message until promises are not resolved
                $rootScope.loadingView = true;
            }
        });

        $rootScope.$on('$routeChangeSuccess', function(e, curr, prev) {
            // console.log('Route Change Success!!');
            // Hide loading message
            $rootScope.loadingView = false;

        });
    }
);
app.factory('OpcionesService', function() {
    return [{
        id: 1,
        nombre: 'RESUMEN',
        url: '/',
        icono: 'fa-dashboard'
    }, {
        id: 2,
        nombre: 'SERVICIOS',
        icono: 'fa-table',
        subOpciones: [{
            id: 3,
            nombre: 'AREAS',
            url: '/areas',
            icono: 'fa-table'
        }, {
            id: 4,
            nombre: 'TIPOS',
            url: '/tipos',
            icono: 'fa-table'
        }, {
            id: 5,
            nombre: 'PROYECTOS',
            url: '/proyectos',
            icono: 'fa-table'
        }]
    }, {
        id: 6,
        nombre: 'IDI',
        icono: 'fa-table',
        subOpciones: [{
            id: 7,
            nombre: 'ARTICULOS',
            url: '/articulos',
            icono: 'fa-table'
        }, {
            id: 8,
            nombre: 'PROYECTOS_IDI',
            url: '/proyectos_idi',
            icono: 'fa-table'
        }]
    }];
});