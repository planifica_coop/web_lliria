<?php
class Imagen extends Eloquent
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pla_imagenes';

    public function proyecto()
    {
        return $this->belongsTo('Proyecto');
    }
}
