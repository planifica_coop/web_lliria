<?php
class ImagenController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($proyectoId) {
        return Proyecto::find($proyectoId)->imagenes()->get();
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($proyectoId) {
        $extensionesImagenes = array("jpg", "png", "jpeg", "gif", "tiff");
        $file = Input::file('file');
        $nombre = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $image = Image::make($file->getRealPath());
        $ratio = $image->width() / $image->height();
        if ($image->width() > $image->height() && $ratio > 1.3 && $ratio < 1.5) {
            if ($this->in_arrayi($extension, $extensionesImagenes)) {
                $destinationPath = $_SERVER['DOCUMENT_ROOT'] . Config::get('planifica.urlAdjuntosImagenes');
                
                $imagen = new Imagen;
                $imagen->nombre = $nombre;
                $imagen->extension = $extension;
                $imagen->id_pla_proyectos = $proyectoId;
                $imagen->save();

                $image->resize(701, 500);
                $image->save($destinationPath . $imagen->id . '.' . $extension);
                
                return $imagen;
                
                // $upload_success = $file->move($destinationPath, $imagen->id . '.' . $extension);
                
                // if ($upload_success) {
                //     $img = Image::make($destinationPath . $imagen->id . '.' . $extension);
                //     return $imagen;
                // } else {
                //     $imagen->delete();
                //     return Response::json('error', 400);
                // }
                
                
            } else {
                return Response::json('error', 400);
            }
        } else {
            return Response::json('error', 400);
        }
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($proyectoId, $imagenId) {
        return Imagen::find($imagenId);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($proyectoId, $imagenId) {
        $hayError = false;
        $destinationPath = $_SERVER['DOCUMENT_ROOT'] . Config::get('planifica.urlAdjuntosImagenes');
        $imagen = Imagen::find($imagenId);
        if (File::delete($destinationPath . $imagen->id . '.' . $imagen->extension)) {
            if (!$imagen->delete()) $hayError = true;
        } else {
            $hayError = true;
        }
        return ($hayError) ? Response::json('error', 400) : Response::json('success', 200);
    }
    
    //Función para hacer un in_array pero case insensitive
    private function in_arrayi($needle, $haystack) {
        foreach ($haystack as $value) {
            if (strtolower($value) == strtolower($needle)) return true;
        }
        return false;
    }
}
