<?php
class ProyectoArea extends Eloquent
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pla_proyectos_areas';
    
    public function proyecto() {
        return $this->hasOne('Proyecto', 'id_pla_proyectos');
    }
    
    public function area() {
        return $this->hasOne('Area', 'id_pla_areas');
    }
}
