articulosControllers.controller('ArticuloListController', function($scope, $location, Articulo, ngTableParams, sigoModal, $filter) {

    $scope.nuevoArticulo = function() {
        $location.path(CONFIG.URL.ARTICULOS + CONFIG.URL.ADD);
    };

    $scope.editarArticulo = function(id) {
        $location.path(CONFIG.URL.ARTICULOS + CONFIG.URL.EDIT + '/' + id);
    };

    $scope.eliminarArticulo = function(articulo, posicion) {
        sigoModal.confirmar('¿Desea borrar el articulo ' + articulo.nombre + '?', 'Sí', 'No')
            .then(function() {
                    Articulo.delete({
                        id: articulo.id
                    }, function() {
                        console.log('Articulo ' + articulo.id + ' eliminado.');
                        $scope.articulos.splice($scope.articulos.indexOf(articulo), 1);
                        $scope.tableParams.reload();
                    }, function() {
                        sigoModal.alerta('No se ha podido eliminar el articulo.', 'Aceptar');
                    });
                },
                function() {
                    console.log('cancel');
                });

    };

    $scope.actualizarBusqueda = function() {
        $scope.tableParams.reload();
    };

    $scope.articulos = Articulo.query(function() {
        $scope.tableParams.reload();
    });

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10 // count per page
    }, {
        total: $scope.articulos.length, // length of data
        getData: function($defer, params) {
            var filteredData = $filter('filter')($scope.articulos, $scope.busqueda);
            var orderedData = params.sorting() ?
                $filter('orderBy')(filteredData, params.orderBy()) :
                filteredData;
            var total = orderedData.length;
            params.total(total);
            if (total <= params.count())
                params.page(1);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
});