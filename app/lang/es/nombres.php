<?php

return array(
	'inicio' => 'inicio',
	'quienes_somos'     => 'quienes<br>somos',
	'nuestros_servicios'     => 'nuestros<br>servicios',
	'internacionalizacion'     => 'internacionalización',
	'sistema_calidad'     => 'sistema<br>de calidad',
	'i_d_i'     => 'I+D+i',
	'contacto'     => 'contacto',
	'envianos_un_mensaje'     => 'envianos un mensaje',
	'nombre'     => 'nombre',
	'email'     => 'e-mail',
	'asunto'     => 'asunto',
	'mensaje'     => 'mensaje',
	'enviar'     => 'enviar',
	'todos'     => 'todos',
	'sobre_nosotros'     => 'sobre Nosotros',
	'oficina_central'     => 'oficina Central',
	'direccion'     => 'dirección',
	'telefono'     => 'teléfono',
	'seleccionar_categoria'     => 'seleccionar categoría',
	'articulos_publicados_planifica'     => 'artículos publicados',
	'proyectos_idi'     => 'proyectos de I+D+i',
	'monitorizacion_consumos'     => 'monitorización de consumos',
	'consumo_electrico_planifica'     => 'consumo eléctrico de las oficinas de Planifica.',
	'urbanismo_ordenacion'     => 'urbanismo y ordenación del territorio',
	'ingenieria_civil'     => 'ingeniería civil',
	'ingenieria_ambiental'     => 'ingeniería ambiental',
	'arquitectura'     => 'arquitectura',
	'ingenieria_industrial'     => 'ingeniería industrial',
	'trabajo_reciente'     => 'trabajos recientes',
	'nuestros_partners'     => 'nuestros partners',
	'proyecto'     => 'proyecto',
	'aplicaciones_web'     => 'aplicaciones web',
	'error'     => 'error',
	'no_encontrado_error'     => 'uups...te has perdido?',
	'volver_anterior'     => 'volver a la página anterior'
);

