<?php
class Noticia extends Eloquent
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
  protected $table = 'noticias';

  protected $fillable = array('fecha', 'id', 'titulo', 'texto', 'link', 'imagen', 'texto_boton');

  public function getTitulo() {
        if (App::getLocale() == 'es') {
            return $this->titulo;
        } elseif (App::getLocale() == 'en') {
            return $this->titulo;
        } elseif (App::getLocale() == 'fr') {
            return $this->titulo;
        }
    }

    public function getFecha() {
        if (App::getLocale() == 'es') {
            return $this->fecha;
        } elseif (App::getLocale() == 'en') {
            return $this->fecha;
        } elseif (App::getLocale() == 'fr') {
            return $this->fecha;
        }
    }

    public function getTexto() {
        if (App::getLocale() == 'es') {
            return $this->texto;
        } elseif (App::getLocale() == 'en') {
            return $this->texto;
        } elseif (App::getLocale() == 'fr') {
            return $this->texto;
        }
    }

    public function getLink() {
        if (App::getLocale() == 'es') {
            return $this->link;
        } elseif (App::getLocale() == 'en') {
            return $this->link;
        } elseif (App::getLocale() == 'fr') {
            return $this->link;
        }
    }

    public function getImagen() {
        if (App::getLocale() == 'es') {
            return $this->imagen;
        } elseif (App::getLocale() == 'en') {
            return $this->imagen;
        } elseif (App::getLocale() == 'fr') {
            return $this->imagen;
        }
    }

    public function getTexto_boton() {
        if (App::getLocale() == 'es') {
            return $this->texto_boton;
        } elseif (App::getLocale() == 'en') {
            return $this->texto_boton;
        } elseif (App::getLocale() == 'fr') {
            return $this->texto_boton;
        }
    }


}
