@extends('base')
@section('content')
<!-- Region 3 Wrap -->
<div class="container region3wrap">
  <!-- Main Slider -->
  <div class="mainslider-container responsive" >
    <div class="mainslider" >
      <ul>
        <!-- SLIDE DOWN -->
        <li data-transition="slotfade-horizontal" data-slotamount="1" data-masterspeed="300"  data-thumb="assets/images/sliders/sliderrevolution/thumbs/thumb1.jpg">
          <img src="assets/images/sliders/sliderrevolution/slides/slide_image1.jpg"  alt="">
          <div class="caption lfr" data-x="560" data-y="20" data-speed="700" data-start="800" data-easing="easeOutExpo"><img src="assets/images/sliders/sliderrevolution/slides/slide_image-ipad.png" alt=""></div>
          <div class="caption lfr" data-x="500" data-y="20" data-speed="700" data-start="1000" data-easing="easeOutExpo"><img src="assets/images/sliders/sliderrevolution/slides/slide_image-iphone.png" alt=""></div>
          <div class="caption lfl big_white"  data-x="100" data-y="130" data-speed="300" data-start="1200" data-easing="easeOutExpo">Amazing Touch Experience</div>
          <div class="caption lfl big_color"  data-x="100" data-y="167" data-speed="300" data-start="1300" data-easing="easeOutExpo">Fully Responsive</div>
          <div class="caption lfl medium_grey"  data-x="100" data-y="222" data-speed="300" data-start="1400" data-easing="easeOutExpo">Inventore veritatis et quasi architecto <br/>beatae dicta sed ut perspiciatis unde omnis<br/> iste natus laudantium.</div>
          <div class="caption lfb"  data-x="100" data-y="300" data-speed="300" data-start="1500" data-easing="easeOutExpo"><a href="#" class="button">Experience Now</a></div>
        </li>
        <!-- BOXFADE -->
        <li data-transition="boxfade" data-slotamount="5"  data-masterspeed="300" data-thumb="assets/images/sliders/sliderrevolution/thumbs/thumb2.jpg" >
          <img src="assets/images/sliders/sliderrevolution/slides/slide_image2.jpg" alt="">
          <div class="caption lfb big_white"  data-x="400" data-y="80" data-speed="900" data-start="1700" data-easing="easeOutBack">Speed and Precision</div>
          <div class="caption lft big_color"  data-x="400" data-y="117" data-speed="900" data-start="1900" data-easing="easeOutBack"><em>HTML5 &amp; CSS3</em></div>
          <div class="caption lfr medium_grey"  data-x="400" data-y="165" data-speed="300" data-start="2500" data-easing="easeOutExpo">Accusantium et doloremque veritatis<br/> architecto eaque ipsa quae ab illo<br/> inventore veritatis perspiciatis.</div>
          <div class="caption sfb" data-x="550" data-y="240" data-speed="1000" data-start="3500" data-easing="easeOutBack"><a href="#" class="button">Wanna Test Drive?</a></div>
        </li>
        <!-- CURTAIN 3 -->
        <li data-transition="curtain-3" data-slotamount="20"  data-masterspeed="300" data-thumb="assets/images/sliders/sliderrevolution/thumbs/thumb3.jpg">
          <img src="assets/images/sliders/sliderrevolution/slides/slide_image3.jpg" alt="" >
          <div class="caption lfb" data-x="360" data-y="235" data-speed="900" data-start="700" data-easing="easeOutExpo"><img src="assets/images/sliders/sliderrevolution/slides/hand.png" alt=""></div>
          <div class="caption sft big_color"  data-x="460" data-y="120" data-speed="900" data-start="1200" data-easing="easeOutBack">Easy and Fast Customization</div>
          <div class="caption sft medium_text black"  data-x="544" data-y="170" data-speed="900" data-start="1300" data-easing="easeOutBack">Buy TouchM on ThemeForest!</div>
        </li>
        <!-- SLIDE LEFT -->
        <li data-transition="slotslide-horizontal" data-slotamount="10"  data-masterspeed="300"  data-link="http://www.google.de" data-thumb="assets/images/sliders/sliderrevolution/thumbs/thumb4.jpg">
          <img src="assets/images/sliders/sliderrevolution/slides/slide_image4.jpg" alt=""  >
          <div class="caption lft big_white"  data-x="400" data-y="120" data-speed="900" data-start="1200" data-easing="easeOutBack">Modern Premium Theme</div>
          <div class="caption lfb" data-x="380" data-y="180" data-speed="900" data-start="1700" data-easing="easeOutBack"><img src="assets/images/sliders/sliderrevolution/slides/icon1.png" alt=""></div>
          <div class="caption lfb" data-x="540" data-y="180" data-speed="900" data-start="2000" data-easing="easeOutBack"><img src="assets/images/sliders/sliderrevolution/slides/icon2.png" alt=""></div>
          <div class="caption lfb" data-x="690" data-y="180" data-speed="900" data-start="2300" data-easing="easeOutBack"><img src="assets/images/sliders/sliderrevolution/slides/icon3.png" alt=""></div>
          <div class="caption lfr medium_grey"  data-x="400" data-y="330" data-speed="300" data-start="2600" data-easing="easeOutExpo">Wanna learn more?</div>
          <div class="caption lfr medium_text"  data-x="595" data-y="332" data-speed="300" data-start="2800" data-easing="easeOutExpo"><a href="#">Get in Touch</a></div>
        </li>
      </ul>
      <div class="tp-bannertimer"></div>
    </div>
  </div>
  <!-- End Main Slider -->
  <!-- Content Top -->
  <div class="row content_top">
    <div class="twelve columns">
      <h2>We create custom-built, marketing-driven website solutions and online environments that ensure your brand delivers on form and functionality.</h2>
    </div>
  </div>
  <!-- End Content Top -->
</div>
<!-- End Region 3 Wrap -->
<!-- Region 4 Wrap -->
<div class="container region4wrap">
  <div class="row maincontent">
    <!-- Main Content ( Middle Content) -->
    <div class="twelve columns">
      <!-- Services -->
      <div class="row">
        @foreach ($areas as $area)
        <!-- // // // // // // // // // // -->
        <div class="four columns">
          <div class="service">
            <a href="#">
            <div class="service-icon"><img src="assets/images/icons/service-responsive.png" width="120" height="60" alt="Modern"></div>
            <h2 class="service-main">{{ $area->getNombre() }}</h2>
            <p class="service-sub">{{ str_limit($area->getDescripcion(), $limit = 300, $end = '...') }}</p>
            </a>
          </div>
        </div>
        <!-- // // // // // // // // // // -->
        @endforeach
      </div>
      <!-- End Services -->
      <div class="row">
        <div class="twelve columns">
          <hr/>
        </div>
      </div>
      <!-- Recent Work -->
      <div class="row">
        <div class="twelve columns">
          <h3>{{ ucwords(trans('nombres.trabajo_reciente')) }}</h3>
          <!-- // // // // // // // // // // -->
          <div class="list_carousel">
            <div class="carousel_nav">
              <a class="prev" id="car_prev" href="#"><span>prev</span></a>
              <a class="next" id="car_next" href="#"><span>next</span></a>
            </div>
            <div class="clearfix"></div>
            <ul id="carousel-works">
              @foreach ($proyectos as $proyecto)
              <li>
                <div class="work-item contentHover">
                  <div class="content">
                    <div class="work-item-image">
                      @if ($proyecto->imagen)
                      <img src="uploads/images/proyectos/{{$proyecto->imagen->id}}.{{$proyecto->imagen->extension}}" alt=""/>
                      @else
                      <img src="assets/images/works/1.jpg" alt=""/>
                      @endif
                    </div>
                    <div class="work-item-content">
                      <h4><a href="{{ URL::to('servicios/proyecto/' . $proyecto->id) }}">{{ $proyecto->getNombre() }}</a></h4>
                      <p>{{ str_limit($proyecto->getDescripcion(), $limit = 100, $end = '...') }}</p>
                    </div>
                  </div>
                  <div class="hover-content">
                    <h3><a href="{{ URL::to('servicios/proyecto/' . $proyecto->id) }}">{{ $proyecto->getNombre() }}</a></h3>
                    <p>{{ str_limit($proyecto->getDescripcion(), $limit = 250, $end = '...') }}</p>
                    <div class="hover-links">
                      <a href="{{ URL::to('servicios/proyecto/' . $proyecto->id) }}" class="view-item"><span>&nbsp;</span></a>
                      @if ($proyecto->imagen)
                        <a class="titan-lb view-image" href="uploads/images/proyectos/{{$proyecto->imagen->id}}.{{$proyecto->imagen->extension}}" title="{{ $proyecto->getNombre() }}.">
                      @else
                        <a class="titan-lb view-image" href="images/works/1.jpg" title="{{ $proyecto->getNombre() }}.">
                      @endif
                      <span>&nbsp;</span></a>
                    </div>
                  </div>
                </div>
              </li>
              @endforeach
            </ul>
            <div class="clearfix"></div>
          </div>
          <!-- // // // // // // // // // // -->
        </div>
      </div>
      <!-- End Recent Work -->
      <div class="row">
        <div class="twelve columns">
          <hr/>
        </div>
      </div>
      <!-- Our Partners -->
      <div class="row">
        <div class="twelve columns">
          <h3>{{ ucwords(trans('nombres.nuestros_partners')) }}</h3>
          <ul class="clients_list">
            <li class="three"><a class="has-tipsy" href="#" title="Site Templates and Themes"><img src="assets/images/logos/1.png" alt="Apple"></a></li>
            <li class="three"><a class="has-tipsy" href="#" title="Awesome Stock Graphics"><img src="assets/images/logos/2.png" alt="EA"></a></li>
            <li class="three"><a class="has-tipsy" href="#" title="PHP, JS, Java and .NET"><img src="assets/images/logos/3.png" alt="Adidas"></a></li>
            <li class="three"><a class="has-tipsy" href="#" title="Stock Motion Graphics"><img src="assets/images/logos/4.png" alt="NASA"></a></li>
          </ul>
        </div>
      </div>
      <!-- End Our Partners -->
    </div>
    <!-- End Main Content ( Middle Content) -->
  </div>
</div>
<!-- End Region 4 Wrap -->
@stop