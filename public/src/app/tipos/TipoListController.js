tiposControllers.controller('TipoListController', function($scope, $location, Tipo, ngTableParams, sigoModal, $filter) {

    $scope.nuevoTipo = function() {
        $location.path(CONFIG.URL.TIPOS + CONFIG.URL.ADD);
    };

    $scope.editarTipo = function(id) {
        $location.path(CONFIG.URL.TIPOS + CONFIG.URL.EDIT + '/' + id);
    };

    $scope.eliminarTipo = function(tipo, posicion) {
        sigoModal.confirmar('¿Desea borrar el tipo ' + tipo.nombre + '?', 'Sí', 'No')
            .then(function() {
                    Tipo.delete({
                        id: tipo.id
                    }, function() {
                        console.log('Tipo ' + tipo.id + ' eliminado.');
                        $scope.tipos.splice($scope.tipos.indexOf(tipo), 1);
                        $scope.tableParams.reload();
                    }, function() {
                        sigoModal.alerta('No se ha podido eliminar el tipo', 'Aceptar');
                    });
                },
                function() {
                    console.log('cancel');
                });

    };

    $scope.actualizarBusqueda = function() {
        $scope.tableParams.reload();
    }

    $scope.tipos = Tipo.query(function() {
        $scope.tableParams.reload();
    });

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10 // count per page
    }, {
        total: $scope.tipos.length, // length of data
        getData: function($defer, params) {
            var filteredData = $filter('filter')($scope.tipos, $scope.busqueda);
            var orderedData = params.sorting() ?
                $filter('orderBy')(filteredData, params.orderBy()) :
                filteredData;
            var total = orderedData.length;
            params.total(total);
            if (total <= params.count())
                params.page(1);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
});