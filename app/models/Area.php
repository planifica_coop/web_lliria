<?php
class Area extends Eloquent
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pla_areas';

    protected $fillable = array('nombre_es', 'nombre_en', 'nombre_fr', 'descripcion_es', 'descripcion_en', 'descripcion_fr');
    
    public function tipos() {
        return $this->hasMany('Tipo', 'id_pla_areas');
    }

    public function proyectos() {
        return $this->hasManyThrough('Proyecto', 'ProyectoArea', 'id_pla_areas', 'id');
    }
    
    public function getNombre() {
        if (App::getLocale() == 'es') {
            return $this->nombre_es;
        } elseif (App::getLocale() == 'en') {
            return $this->nombre_en;
        } elseif (App::getLocale() == 'fr') {
            return $this->nombre_fr;
        }
    }
    
    public function getDescripcion() {
        if (App::getLocale() == 'es') {
            return $this->descripcion_es;
        } elseif (App::getLocale() == 'en') {
            return $this->descripcion_en;
        } elseif (App::getLocale() == 'fr') {
            return $this->descripcion_fr;
        }
    }
}
