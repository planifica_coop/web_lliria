sigoBootstrap.factory('sigoModal', function($q) {
    var modalHtml = '' +
        '<div class="modal fade" id="modal-confirm">' +
        '<div class="modal-dialog">' +
        ' <div class="modal-content">' +
        '<div class="modal-header">' +
        '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
        '<h4 id="modal-confirm-title" class="modal-title">Confirmar</h4>' +
        '</div>' +
        '<div class="modal-body">' +
        '<p id="modal-confirm-message" >One fine body&hellip;</p>' +
        '</div>' +
        '<div class="modal-footer">' +
        '<button id="modal-confirm-ok" type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
        '<button id="modal-confirm-cancel" type="button" class="btn btn-primary">Save changes</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    $('body').append(modalHtml);

    return {
        confirmar: function(mensaje, labelOk, labelCancel) {
            var defer = $q.defer();

            $('#modal-confirm-message').html(mensaje);
            $('#modal-confirm-title').html('Confirmar');
            if (labelOk)
                $('#modal-confirm-ok').html(labelOk);
            if (labelCancel)
                $('#modal-confirm-cancel').html(labelCancel);
            $('#modal-confirm-ok').on('click', function() {
                defer.resolve();
                $('#modal-confirm').modal('hide')
            });
            $('#modal-confirm-cancel').on('click', function() {
                defer.reject();
                $('#modal-confirm').modal('hide')
            });
            $('#modal-confirm').modal('show')
            return defer.promise;
        },
        alerta: function(mensaje, label) {
            var defer = $q.defer();

            $('#modal-confirm-message').html(mensaje);
            $('#modal-confirm-title').html('Alerta');
            if (label)
                $('#modal-confirm-ok').html(label);
            $('#modal-confirm-cancel').remove();
            $('#modal-confirm-ok').on('click', function() {
                defer.resolve();
                $('#modal-confirm').modal('hide')
            });
            $('#modal-confirm').modal('show')
            return defer.promise;
        }
    };
});