@extends('frontend.layout.base')

@section('contenido')

  <section id="page-title" class="page-title-video text-light"
           data-vide-bg="mp4:{{ url('frontend/video/encuesta') }}, poster:{{ url('frontend/video/encuesta_poster') }}"
           data-vide-options="position: 0% 70%"
           style="background: rgba(80,99,128,0.60);">
    <div class="container">
      <div class="page-title col-md-8">
        <h1>{{ trans('general.version_preliminar_seccion.titulo') }}</h1>
      </div>
      <div class="col-md-12">
        <h4 class="text-center" style="font-size: 2.5rem;font-weight: 100;">{{ trans('general.documentacion_seccion.subtitulo') }}</h4>
      </div>
    </div>
  </section>

  <section class="container">
    <div class="container-fluid container-fullscreen">
      <div class="text-middle text-center">
        <a href="http://mapifica.org/pmu_vinaros/index.html" class="button color" target="_blank" style="width: 300px;">Estudio de Tráfico 2012</a>
        <br>
        <a href="http://pepvinaros.particip.es/" class="button color" target="_blank" style="width: 300px;">Plan Especial</a>
        <br>
        <a href="http://edusi.vinaros.es/es/contenido/inicio" class="button color" target="_blank" style="width: 300px;">EDUSI Vinaròs</a>
      </div>
    </div>
  </section>

  <!-- <section class="container">
    <iframe src="http://mapifica.org/pmu_vinaros/index.html" style="border:none;height: 800px;width:100%;"></iframe>
  </section>
 -->

@endsection