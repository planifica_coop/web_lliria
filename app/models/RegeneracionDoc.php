<?php
class RegeneracionDoc extends Eloquent
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
  protected $table = 'regeneracion_docs';

  protected $fillable = array('fecha', 'id', 'titulo', 'link_1', 'link_2', 'link_3','link_4', 'link_5', 'text_link_1', 'text_link_2', 'text_link_3', 'text_link_4', 'text_link_5', 'descripcion');

  public function getTitulo() {
        if (App::getLocale() == 'es') {
            return $this->titulo;
        } elseif (App::getLocale() == 'en') {
            return $this->titulo;
        } elseif (App::getLocale() == 'fr') {
            return $this->titulo;
        }
    }

    public function getFecha() {
        if (App::getLocale() == 'es') {
            return $this->fecha;
        } elseif (App::getLocale() == 'en') {
            return $this->fecha;
        } elseif (App::getLocale() == 'fr') {
            return $this->fecha;
        }
    }

    public function getText_link_1() {
        if (App::getLocale() == 'es') {
            return $this->text_link_1;
        } elseif (App::getLocale() == 'en') {
            return $this->text_link_1;
        } elseif (App::getLocale() == 'fr') {
            return $this->text_link_1;
        }
    }

    public function getLink_1() {
        if (App::getLocale() == 'es') {
            return $this->link_1;
        } elseif (App::getLocale() == 'en') {
            return $this->link_1;
        } elseif (App::getLocale() == 'fr') {
            return $this->link_1;
        }
    }

    public function getText_link_2() {
        if (App::getLocale() == 'es') {
            return $this->text_link_2;
        } elseif (App::getLocale() == 'en') {
            return $this->text_link_2;
        } elseif (App::getLocale() == 'fr') {
            return $this->text_link_2;
        }
    }

    public function getText_link_3() {
        if (App::getLocale() == 'es') {
            return $this->text_link_3;
        } elseif (App::getLocale() == 'en') {
            return $this->text_link_3;
        } elseif (App::getLocale() == 'fr') {
            return $this->text_link_3;
        }
    }

    public function getText_link_4() {
        if (App::getLocale() == 'es') {
            return $this->text_link_4;
        } elseif (App::getLocale() == 'en') {
            return $this->text_link_4;
        } elseif (App::getLocale() == 'fr') {
            return $this->text_link_4;
        }
    }

    public function getText_link_5() {
        if (App::getLocale() == 'es') {
            return $this->text_link_5;
        } elseif (App::getLocale() == 'en') {
            return $this->text_link_5;
        } elseif (App::getLocale() == 'fr') {
            return $this->text_link_5;
        }
    }

    public function getLink_2() {
        if (App::getLocale() == 'es') {
            return $this->link_2;
        } elseif (App::getLocale() == 'en') {
            return $this->link_2;
        } elseif (App::getLocale() == 'fr') {
            return $this->link_2;
        }
    }

    public function getLink_3() {
        if (App::getLocale() == 'es') {
            return $this->link_3;
        } elseif (App::getLocale() == 'en') {
            return $this->link_3;
        } elseif (App::getLocale() == 'fr') {
            return $this->link_3;
        }
    }

    public function getLink_4() {
        if (App::getLocale() == 'es') {
            return $this->link_4;
        } elseif (App::getLocale() == 'en') {
            return $this->link_4;
        } elseif (App::getLocale() == 'fr') {
            return $this->link_4;
        }
    }

    public function getLink_5() {
        if (App::getLocale() == 'es') {
            return $this->link_5;
        } elseif (App::getLocale() == 'en') {
            return $this->link_5;
        } elseif (App::getLocale() == 'fr') {
            return $this->link_5;
        }
    }

     public function getDescripcion() {
        if (App::getLocale() == 'es') {
            return $this->descripcion;
        } elseif (App::getLocale() == 'en') {
            return $this->descripcion;
        } elseif (App::getLocale() == 'fr') {
            return $this->descripcion;
        }
    }


}
