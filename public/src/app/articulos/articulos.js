var articulos = angular.module('articulos', ['articulos.controllers', 'articulos.services']);
var articulosControllers = angular.module('articulos.controllers', ['ngTable', 'utils']);

articulos.config(function($routeProvider) {
    $routeProvider.when(CONFIG.URL.ARTICULOS, {
        templateUrl: 'src/app/articulos/articulos-list.html',
        controller: 'ArticuloListController'
    }).when(CONFIG.URL.ARTICULOS + CONFIG.URL.ADD, {
        templateUrl: 'src/app/articulos/articulos-add.html',
        controller: 'ArticuloAddController'
    }).when(CONFIG.URL.ARTICULOS + CONFIG.URL.EDIT + '/:id', {
        templateUrl: 'src/app/articulos/articulos-edit.html',
        controller: 'ArticuloEditController'
    })
});