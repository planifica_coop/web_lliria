<?php
class ProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $respuesta = Proyecto::with('areas')->with('tipos')->with('imagenes')->with('imagen');
        if(Input::has('sort')) {
            $respuesta->orderBy(Input::get('sort', 'desc'));
        }
        return $respuesta->get();
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        return Proyecto::create(Input::all());
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        return Proyecto::with('areas')->with('tipos')->with('imagenes')->with('imagen')->find($id);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $proyecto = Proyecto::find($id);
        $proyecto->update(Input::all());
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $proyecto = Proyecto::find($id)->delete();
    }
}
