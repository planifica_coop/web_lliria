/*
 * Directiva que muestra una alerta. Se pueden especificar difenretes tipos.
 */
sigoBootstrap.directive('sigoAlert', function() {
    return {
        restrict: 'E',
        templateUrl: 'src/common/sigo-bootstrap/alert.html',
        transclude: true,
        scope: {
            'tipo': '@'
        }
    };
});