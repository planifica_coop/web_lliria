<footer class="background-dark text-grey" id="footer">
  <div class="footer-content">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="footer-logo float-left">
            <img alt="" src="{{ asset('frontend/img/logo/logo_white.png') }}">
          </div>
          <p style="margin-top: 12px;">{{ trans('general.lema_footer') }}<a href="http://almussafes.planifica.org/contacto">contacto</a></p>
        </div>
      </div>
      <div class="seperator seperator-dark seperator-simple"></div>
      <div class="row">
        <div class="col-md-4">
          <h4 class="widget-title">{{ trans('general.donde_estamos') }}</h4>
        </div>
        <div class="col-md-8 direcciones">
          <div class="col-md-4">
            <h3>Castellón</h3>
            <p>
              Calle Poeta Guimerá, 7 - 5A
              <br>12001 - Castellón de la Plana
            </p>
          </div>
          <div class="col-md-4">
            <h3>Valencia</h3>
            <p>
              Calle del Almirante, 7 - Local 2
              <br>46003 - Valencia
            </p>
          </div>

        </div>

      </div>
    </div>
  </div>
  <div class="copyright-content">
    <div class="container">
      <div class="row">
        <div class="copyright-text col-md-5"> &copy; 2016 planifica - PLANIFICA INGENIEROS Y
          ARQUITECTOS, COOP. V.
        </div>
        <div class="copyright-text col-md-3  text-right">
          <a href="mailto:pmuslliria@planifica.org" style="text-transform: none;text-decoration: underline !important;">pmuslliria@planifica.org</a>
        </div>
        <div class="col-md-4">
          <div class="copyright-text text-right" style="text-transform: none;">
            <a href=" {{ url('aviso-legal') }}">{{ trans('general.aviso_legal') }}</a>&nbsp;&nbsp;
            <a href="{{ url('politica-cookies') }}">{{ trans('general.politica_cookies') }}</a>&nbsp;&nbsp;
            <a href="{{ url('politica-privacidad') }}">{{ trans('general.politica_privacidad') }}</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
