proyectosIdiControllers.controller('ProyectoIdiListController', function($scope, $location, ProyectoIdi, ngTableParams, sigoModal, $filter) {

    $scope.nuevoProyectoIdi = function() {
        $location.path(CONFIG.URL.PROYECTOS_IDI + CONFIG.URL.ADD);
    };

    $scope.editarProyectoIdi = function(id) {
        $location.path(CONFIG.URL.PROYECTOS_IDI + CONFIG.URL.EDIT + '/' + id);
    };

    $scope.eliminarProyectoIdi = function(proyectoIdi, posicion) {
        sigoModal.confirmar('¿Desea borrar el proyectoIdi ' + proyectoIdi.nombre + '?', 'Sí', 'No')
            .then(function() {
                    ProyectoIdi.delete({
                        id: proyectoIdi.id
                    }, function() {
                        console.log('ProyectoIdi ' + proyectoIdi.id + ' eliminada.');
                        $scope.proyectosIdi.splice($scope.proyectosIdi.indexOf(proyectoIdi), 1);
                        $scope.tableParams.reload();
                    }, function() {
                        sigoModal.alerta('No se ha podido eliminar el proyectoIdi.', 'Aceptar');
                    });
                },
                function() {
                    console.log('cancel');
                });

    };

    $scope.actualizarBusqueda = function() {
        $scope.tableParams.reload();
    }

    $scope.proyectosIdi = ProyectoIdi.query(function() {
        $scope.tableParams.reload();
    });

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10 // count per page
    }, {
        total: 0, // length of data
        getData: function($defer, params) {
            var filteredData = $filter('filter')($scope.proyectosIdi, $scope.busqueda);
            var orderedData = params.sorting() ?
                $filter('orderBy')(filteredData, params.orderBy()) :
                filteredData;
            var total = orderedData.length;
            params.total(total);
            if (total <= params.count())
                params.page(1);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
});