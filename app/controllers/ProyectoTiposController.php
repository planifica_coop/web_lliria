<?php
class ProyectoTiposController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($proyectoId) {
        return Proyecto::find($proyectoId)->tipos()->get();
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($proyectoId) {
        $proyectoTipos = new ProyectoTipo;
        $proyectoTipos->id_pla_proyectos = $proyectoId;
        $proyectoTipos->id_pla_tipos = Input::get('id_pla_tipos');
        $proyectoTipos->save();
        return $proyectoTipos;
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($proyectoId, $tipoId) {
        return Proyecto::find($proyectoId)->tipos()->where('id_pla_tipos', $tipoId);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($proyectoId, $tipoId) {
        $proyectoTipos = ProyectoTipo::where('id_pla_proyectos', $proyectoId)->where('id_pla_tipos', $tipoId)->delete();
    }
}
