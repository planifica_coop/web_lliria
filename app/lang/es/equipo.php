<?php

return [
    [
        "nombre" => "Gonzalo Valls",
        "titulacion" => "Ingeniero de Caminos",
        "descripcion" => "Director técnico. Dilatada experiencia en la asistencia técnica municipal."
    ],
    [
        "nombre" => "Rafael Ibañez",
        "titulacion" => "Arquitecto",
        "descripcion" => "Amplia experiencia en planificación y gestión urbanística."
    ],
    
    [
        "nombre" => "Pedro Millán",
        "titulacion" => "Ingeniero de Caminos",
        "descripcion" => "Especialista en hidráulica urbana y fluvial."
    ],

    [
        "nombre" => "Javier Carmona",
        "titulacion" => "Ingeniero Industrial",
        "descripcion" => "Especialista en movilidad urbana."
    ],
    [
        "nombre" => "Javier Millán",
        "titulacion" => "Ingeniero en Cartografía y Geodesia",
        "descripcion" => "Especialista en cartografía, SIG y webmapping."
    ],
    [
        "nombre" => "Ana Escoms",
        "titulacion" => "Arquitecta",
        "descripcion" => "Amplia experiencia en planificación urbana y territorial. Miembro de la Cátedra Divalterra."
    ],
    [
        "nombre" => "Pablo Valls",
        "titulacion" => "Doctor Ingeniero de Montes",
        "descripcion" => "Técnico en participación ciudadana y medio ambiente "
    ]

];
