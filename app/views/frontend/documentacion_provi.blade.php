@extends('frontend.layout.base')

@section('contenido')

  <section id="page-title" class="page-title-video text-light"
           data-vide-bg="mp4:{{ url('frontend/video/encuesta') }}, poster:{{ url('frontend/video/encuesta_poster') }}"
           data-vide-options="position: 0% 70%"
           style="background: rgba(90,182,178,0.60);">
    <div class="container">
      <div class="page-title col-md-8">
        <h1>{{ trans('general.documentacion_seccion.titulo') }}</h1>
      </div>
      <div class="col-md-12">
        <h4 class="text-center" style="font-size: 2.5rem;font-weight: 100;">{{ trans('general.documentacion_seccion.subtitulo') }}</h4>
      </div>
    </div>
  </section>

  <section class="p-t-40">
    <div class="container">
      <div id="tabs-05c" class="tabs color">
        <p class="lead">{{ trans('general.documentacion_seccion.texto') }}</p>
        <ul class="tabs-navigation">
          <li class="active"><a href="#doc_pgou">{{ trans('general.documentacion_seccion.doc_pgou') }}</a></li>
          <li><a href="#doc_participacion">{{ trans('general.documentacion_seccion.doc_participacion') }}</a></li>
          
        </ul>
        <div class="tabs-content">
          <div class="tab-pane active" id="doc_pgou">
            <ol class="ol-type1">
              @foreach ($articulos as $articulo)
                <li>
                  <strong>{{ $articulo->getNombre() }}</strong><br>
                  {{ $articulo->fecha }}<br>
                  Título del artículo: <a @if ($articulo->adjunto != null) href="{{ URL::to($articulo->adjunto->ruta) }}" @endif target="_blank">
                    {{ $articulo->getTitulo() }}
                  </a><br>
                  Autores: {{ $articulo->autores }}
                </li>
              @endforeach
            </ol>
          </div>
          <div class="tab-pane" id="doc_participacion">
            <h4>{{ trans('general.documentacion_seccion.doc_participacion') }}</h4>
            <ol class="ol-type1">
              <li>
                <strong>BLUECITY [INTERREG IVC].</strong><br>
                Sistema de indicadores para la evaluación de la gestión del ciclo integral del agua en entornos urbanos.
              </li>
              <li>
                <strong>AQUAVAL [Programa Life+].</strong><br>
                La gestión eficiente del agua de lluvia en entornos urbanos (<a target="_blank" href="http://www.aquavalproject.eu/">www.aquavalproject.eu/</a>).

              </li>
              <li>
                <strong>E2Stormed [Programa MED].</strong><br>
                Mejora de la eficiencia energética en el ciclo del agua en ciudades mediterráneas mediante el uso de tecnologías
                innovadoras para la gestión del agua de lluvia (<a target="_blank" href="http://www.e2stormed.eu/">www.e2stormed.eu</a>).
              </li>
            <ol>
          </div>
          
        </div>
      </div>
    </div>
  </section>

@endsection