@extends('base')
@section('content')
@include('barra-navegacion')
<!-- Region 4 Wrap -->
<div class="container region4wrap">
  <div class="row maincontent">
    <div class="twelve columns">
      <div class="page_title">
        <div class="row">
          <div class="twelve columns">
            <h1>{{ ucfirst(trans('nombres.internacionalizacion')) }}</h1>
          </div>
        </div>
      </div>
    </div>
    <div class="twelve columns">
      <div class="row">
        <div class="five columns">
          <div class="article_media">
            <img src="{{URL::to('assets/images/internacionalizacion/sede_bbaa.png') }}" >
          </div>
        </div>
        <div class="seven columns">
          <p>En 2009, con la colaboración del IVEX (Instituto Valenciano de la Exportación), PLANIFICA abre una oficina técnica en Buenos Aires. Los objetivos de esta internacionalización son</p>
          <ul class="disc">
            <li>Presentarse a licitaciones públicas en países de latinoamérica, con socios locales, extranjeros o incluso en solitario, especialmente en lictaciones de organismos multilaterales (BID, BM, ONU, EuropeAid,...)</li>
            <li>Potenciar la implantación en Argentina de constructores españoles interesados tanto en la promoción de edificaciones como de en el desarrollo urbanístico.</li>
            <li>Buscar colaboraciones con consultoras argentinas a las que ofrecer el Know How de PLANIFICA, especialmente es aspectos medioambientales y de sostenibilidad.</li>
            <li>Servir de puente a empresas españolas que quieran realizar una expansión en Argertina, ofreciéndioles un asesoramiento en base a la experiencia adquirida, y buscando sinergias que deriven en nuevas oportunidades de negocio.</li>
            <li>PLANIFICA argentina en los tres últimos años ha gestionado íntegramente un inmueble de 12 apartamentos en la calle Juan Ramirez de Velazco 1153 (donde se ubica la sede la empresa).</li>
          </ul>
          <p>PLANIFICA de la mano clientes habituales, está en proceso de redacción de algunos proyectos fuera de España:</p>
          <ul class="disc">
            <li>Proyecto de urbanización de 1.000 vivienda en Mauritania</li>
            <li>Proyecto de vivienda unifamiliar aislada en Suiza</li>
          </ul>
          <p>Por otra parte la empresa ha formado parte en diversas ocasiones de grupos de trabajo formados para la licitación a proyectos europeos, como:</p>
          <div class="row">
            <div class="six columns">
              <div class="six columns">
                <img src="{{ URL::to('assets/images/internacionalizacion/sudoe.jpg') }}">
              </div>
              <div class="six columns">
                AQUACONTROL
              </div>
            </div>
            <div class="six columns">
              <div class="six columns">
                <img src="{{ URL::to('assets/images/internacionalizacion/interregivc.gif') }}">
              </div>
              <div class="six columns">
                BLUECITY
              </div>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="six columns">
              <div class="six columns">
                <img src="{{ URL::to('assets/images/internacionalizacion/Life.JPG') }}">
              </div>
              <div>
                Life+ VINASTROM<br>Life+ AQUAVAL
              </div>
            </div>
            <div class="six columns">
              <div class="six columns">
                <img src="{{ URL::to('assets/images/internacionalizacion/sudoue2.jpg') }}">
              </div>
              <div class="six columns">
                BLUECHECK
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Region 4 Wrap -->
@stop