@extends('frontend.layout.base')

@section('contenido')
  <style>
    header {
      background: #5ab6b2 !important;
    }
  </style>
  <section class="p-t-20">
    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <!-- Los valores de nuestros productos   -->
          <h2 class="about-title">Aviso Legal</h2>

          <p>
            En cumplimiento de la Ley 34/2002, de 11 de julio, de servicios de la sociedad de la
            información y de comercio electrónico (LSSI), le informamos de
            que:

            PLANIFICA INGENIEROS Y ARQUITECTS COOP. V. (en adelante <b>PLANIFICA</b>) con CIF
            F12963930
            tiene su domicilio en la Calle San Vicente, 4 - 3er Piso - 12002
            Castellón de la Plana. Con inscripción en la Oficina territorial del Rgistro de Cooperativas en el Libro de Inscripciones de Sociedades Cooperativas con el número C-562 asiento 1 de fecha 01 de febrero de 2016
          </p>

          <h4>Contenido del portal</h4>

          <p>
            A través del portal <b>PLANIFICA</b> ofrece a los visitantes del mismo, información
            y herramientas ágiles de comunicación con <b>PLANIFICA</b>
            sin perjuicio de la diligencia empleada en la preparación y elaboración de los
            contenidos del portal, eventualmente pueden existir imprecisiones o
            erratas.
          </p>

          <h4>Condiciones de acceso al portal</h4>

          <p>
            El acceso al portal <b>planifica.org</b> tiene carácter gratuito y su visualización no
            exige previo registro. No obstante, la utilización de
            determinados servicios, puede requier como condición imprescindible, la completa
            cumplimentación de los correspondientes formularios por el Usuario,
            y
            la aceptación de las condiciones de utilización establecidas en el portal para cada uno
            de ellos, y especialmente lo dispuesto en el apartado
            referente a "Proteción de Datos de caracter personal".
          </p>

          <p>
            El Usuario acepta, de forma expresa y sin reservas, que el acceso y la utilización del
            portal, se hace bajo su única y exclusiva responsabilidad.
          </p>

          <h4>Derechos de propiedad sobre el portal</h4>

          <p>
            <b>PLANIFICA</b> es propietaria y/o licenciataria, de toda la información contenida
            en este portal, de su diseño gráfico, imágenes, bases de
            datos, índices, códigos fuentes, marcas y logotipos, estando protegidos conforme a lo
            dispuesto en la Ley de Propiedad Intelectual, y, en la Ley de
            Marcas. Su puesta a disposición y uso, no supone, en ningún caso, la cesión de su
            titularidad o la concesión de un derecho de uso a favor del
            Usuario,
            por lo que toda reproducción, copia, distribución total o parcial, o, comercialización,
            requerirá la autorización previa y escrita de <b>PLANIFICA</b>
          </p>

          <h4>Información y publicidad en el portal</h4>

          <p>
            La inclusión en el portal <b>planifica.org</b> de productos comercializables, tiene
            fines exclusivamente informativos y publicitarios.
          </p>

          <h4>Links o enlaces</h4>

          <p>
            La inclusión en el portal <b>planifica.org</b> de links o enlaces con otros portales,
            tiene una finalidad meramente informativa, y no supone que
            <b>PLANIFICA</b> recomiende y/o garantice dichos portales, sobre los que no ejerce
            control
            alguno, ni es responsable del contenido de los mismos.
          </p>


          <h4>Protección de datos de carácter personal</h4>

          <p>
            En cumplimiento de lo establecido en la Ley Orgánica 15/1999, de 13 de diciembre, de
            Protección de Datos de Carácter Personal, <b>PLANIFICA</b>
            le
            informa de que, los datos facilitados por el Usuario, a través de los distintos
            formularios incorporados en el portal <b>planifica.org</b>, serán
            tratados en los ficheros propiedad de <b>PLANIFICA</b>, que, a los efectos legales
            oportunos, se identifica como responsable de los mismos.
          </p>

          <p>
            <b>PLANIFICA</b> le informa de que la recogida de sus datos de carácter personal se
            produce con los siguientes fines: (i) gestionar y tramitar
            cada uno de los servicios solicitados por el Usuario (ii) comerciales y publicitarios,
            tanto de la empresa <b>PLANIFICA</b> como empresas de su
            grupo empresarial, respecto de productos, servicios, ofertas, promociones, y
            cualesquiera otras actividades publicitarias, y, (iii) realizar
            encuestas, estadísticas y análisis de mercado. El Usuario podrá ejercer los derechos de
            acceso, rectificación, cancelación y oposición de sus datos
            de
            carácter personal, en los términos establecidos en la LOPD, dirigiéndose por escrito a
            la siguiente dirección: <i><b>PLANIFICA</b>, Calle San
              Vicente, 4 - 3er Piso - 12002 Castellón de la Plana</i>
          </p>

          <p>
            En cumplimiento de lo dispuesto en la LOPD, <b>PLANIFICA</b> informa al Usuario de
            que en el supuesto de que facilite datos de carácter personal
            de terceras personas, <b>PLANIFICA</b>, informará a dichos terceros, del contenido
            de los datos facilitados, de la procedencia de los mismos, de
            la existencia y finalidad del fichero donde se contienen sus datos, de la posibilidad de
            ejercitar los derechos de acceso, rectificación,
            cancelación
            y oposición, así como de los datos identificativos de <b>PLANIFICA</b>.
          </p>

          <p>
            El Usuario declara, bajo su responsabilidad, haber leído el presente apartado
            íntegramente, que se reputa conocido y aceptado, expresa y plenamente,
            desde el momento en el que se realice un envío de datos, a través de los formularios
            incluidos en el portal <b>planifica.org</b> Si el Usuario no
            estuviere de acuerdo con el contenido del presente apartado, deberá abstenerse por
            consiguiente de realizar el envío del mismo y, por tanto, le
            agradeceremos que vuelva a leerlo antes de enviar cualquiera de los citados formularios.
          </p>

          <h4>Legislación aplicable y Jurisdicción</h4>

          <p>
            Para la resolución de todas las controversias o cuestiones relacionadas con el presente
            sitio web o de las actividades en él desarrolladas, será de
            aplicación la legislación española, a la que se someten expresamente las partes, siendo
            competentes para la resolución de todos los conflictos
            derivados o relacionados con su uso los Juzgados y Tribunales de Castellón de la Plana.
          </p>


        </div>

      </div>

    </div>
    </div>
  </section>
  <!-- END: FULL WIDTH PAGE -->

@endsection