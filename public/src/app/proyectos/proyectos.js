var TIPOS_CLIENTE = ['Administración Publica', 'Estudio Arquitectura', 'Constructor', 'Consultora Arquitectura', 'Agente Inmobliario', 'Promotor', 'Industria', 'Ingeniería', 'Particular', 'Local', 'Entidad Financiera', 'Otro'];

var proyectos = angular.module('proyectos', ['proyectos.controllers', 'proyectos.services', 'proyectos.utils']);
var proyectosControllers = angular.module('proyectos.controllers', ['textAngular', 'multi-select', 'leaflet-directive', 'utils', 'angularFileUpload', 'sigoBootstrap']);
var proyectosUtils = angular.module('proyectos.utils', []);

proyectos.constant("proyectosConf", {
    dir: 'src/app/proyectos/'
});

proyectos.config(function($routeProvider, proyectosConf) {
    $routeProvider.when(CONFIG.URL.PROYECTOS, {
        templateUrl: proyectosConf.dir + 'proyectos-list.html',
        controller: 'ProyectoListController'
    }).when(CONFIG.URL.PROYECTOS + CONFIG.URL.ADD, {
        templateUrl: proyectosConf.dir + 'proyectos-add.html',
        controller: 'ProyectoAddController',
        resolve: {
            areas: function(Area) {
                return Area.query().$promise.then(function(data) {
                    return data;
                });
            }
        }
    }).when(CONFIG.URL.PROYECTOS + CONFIG.URL.EDIT + '/:id', {
        templateUrl: proyectosConf.dir + 'proyectos-edit.html',
        controller: 'ProyectoEditController',
        resolve: {
            areas: function(Area) {
                return Area.query().$promise.then(function(data) {
                    return data;
                });
            }
        }
    }).when(CONFIG.URL.PROYECTOS + CONFIG.URL.VIEW + '/:id', {
        templateUrl: proyectosConf.dir + 'proyectos-detail.html',
        controller: 'ProyectoDetailController',
        resolve: {
            proyecto: function(Proyecto, $route) {
                return Proyecto.get({
                    id: $route.current.params.id
                }).$promise.then(function(data) {
                    return data;
                });
            }
        }
    });
});