areasControllers.controller('AreaAddController', function($scope, $location, $routeParams, Area, areas, sigoModal) {

    $scope.crearArea = function(areas) {
        console.log('Creada area.');
        Area.save({}, areas, function() {
            $location.path(CONFIG.URL.AREAS);
        }, function() {
            sigoModal.alerta('No se ha podido crear la area', 'Aceptar');
        });
    };

    $scope.cancelar = function() {
        $location.path(CONFIG.URL.AREAS);
    };

    $scope.areas = areas;
    $scope.textoBoton = 'AÑADIR';
});