proyectosIdiControllers.controller('ProyectoIdiAddController', function($scope, $rootScope, $location, $routeParams, ProyectoIdi, sigoModal) {

    $scope.crearProyectoIdi = function(proyectoIdi) {
        console.log('Creado proyectoIdi.');
        console.log(proyectoIdi);
        ProyectoIdi.save({}, proyectoIdi, function() {
            $rootScope.setForm();
            $location.path(CONFIG.URL.PROYECTOS_IDI);
        }, function() {
            sigoModal.alerta('No se ha podido crear el proyectoIdi', 'Aceptar');
        });
    };

    $scope.cancelar = function() {
        $location.path(CONFIG.URL.PROYECTOS_IDI);
    };

    $scope.textoBoton = 'AÑADIR';
    /* Validacion */
    $scope.submitted = false;
});