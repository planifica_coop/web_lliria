<?php

return [
    "quienes_somos" => "quiénes somos",
    "servicios" => "servicios",
    "proyectos" => "proyectos",
    "clientes" => "clientes",
    "innovacion" => "innovación",
    "contacto" => "contacto",
    "actualidad" => "actualidad",
    "transparencia" => "transparencia",
    "tramitacion" => "Planificación",
    "documentacion" => "documentación",
    "comunicacion" => "noticias",
    "preliminar" => "Otros planes",
    "cofinanciacion" => "Cofinanciación",

    "lema" => "La <b>planificación</b> del Plan de Movilidad Urbana Sostenible (PMUS) está recogida en el siguiente programa de trabajos:",
    "lema_footer" => "El Ayuntamiento de Llíria ha adjudicado a Planifica Ingenieros y Arquitectos la redacción del Plan de Movilidad Urbana Sostenible (PMUS) y el proyecto de caminos escolares. Puedes hacernos llegar tus sugerencias u opiniones a través del área de ",
    "lema_tramitacion" => "El Plan de Regeneración Urbana y Movilidad Sostenible de Vinaròs (PRUMS), es un Plan formado por dos aspectos relevantes para el planeamiento de las ciudades, por una parte la Regeneración Urbana y por otro la Movilidad Sostenible",

    'donde_estamos' => 'Dónde Estamos',
    'mostrar_todos' => 'Mostrar todos',
    'aviso_legal' => 'aviso legal',
    'politica_cookies' => 'política de cookies',
    'politica_privacidad' => 'política de privacidad',
    'nombre' => 'nombre',
    'nombre_placeholder' => 'introduce nombre',
    'email' => 'correo electrónico',
    'email_placeholder' => 'introduce correo electrónico',
    'comentario' => 'comentario',
    'comentario_placeholder' => 'introduce comentario',
    'enviar' => 'enviar',

    "home" => [
        "titulo" => "Vinaròs PARTICIPA",
        "subtitulo" => "Redacción del Plan de Movilidad Urbana Sostenible (PMUS) y proyecto de caminos escolares de Llíria",
        "boton" => "MAPA INTERACTIVO",
        "boton_encuesta" => "Encuesta sobre el Plan de Movilidad",
        "texto" => "<b>Tu opinión es importante:</b> El Plan General de Almussafes va a definir sus estrategias de crecimiento y desarrollo, es importante que todos actuemos y participemos en él para lograr la mejor de las opciones"
    ],

    "areas" => [
        "civil" => "Ingeniería Civil",
        "civil_texto" => "Proyectos de urbanización, depuradoras, encauzamientos, colectores, etc.",
        "agua" => "Ingeniería del Agua",
        "agua_texto" => "Estudios de inundabilidad, estudios hidráulicos, drenaje urbano y técnicas SuDS, etc.",
        "ambiental" => "Medio Ambiente y Paisaje",
        "ambiental_texto" => "Evaluación ambiental estratégica, impacto ambiental, paisaje, etc.",
        "urbanismo" => "Urbanismo y Ordenación del Territorio",
        "urbanismo_texto" => "Planes de acción territorial, planes generales, planes parciales, reparcelaciones, etc.",
        "mapas" => "Mapas Interactivos",
        "mapas_texto" => "IDEs locales; procesamiento cartográfico y difusión webmapping",
        "movilidad" => "Movilidad Urbana",
        "movilidad_texto" => "Planes de movilidad urbana sostenible",
    ],

    "clientes_seccion" => [
        'titulo' => 'clientes',
        'subtitulo' => 'Estos son algunos de los ayuntamientos y empresas que ya confian en
        nosotros',
    ],

    "tramitacion_seccion" => [
        'titulo' => 'planificación',
        'subtitulo' => '',
    ],

    "equipo_seccion" => [
        'titulo' => 'Nuestro Equipo',
        'subtitulo' => 'Ingenieros y Arquitectos con amplia experiencia y solvencia profesional. La empresa está integrada por técnicos altamente cualificados, expertos en diferentes áreas de la ingeniería civil, industrial y de la arquitectura.',
    ],


     "comunicacion_seccion" => [
        "titulo" => "Área de Noticias",
        "subtitulo" => "",
        "texto" => "PLANIFICA tiene como uno de sus principales objetivos el impulso de proyectos innovadores. Dentro de las actuaciones destaca la publicación de artículos científicos, la participación en congresos y foros o la colaboración con universidades públicas.",
        "noticias" => "Noticias",
        "proyectos_europeos" => "Proyectos Europeos",
        "colaboracion_universidad" => "Colaboración con universidades"
    ],

    "documentacion_seccion" => [
        "titulo" => "área de documentación",
        "subtitulo" => "",
        "texto" => "La documentación que se muestra es sólo parte del expediente del Plan de Movilidad Urbana Sostenible de Llíria (PMUS). La documentación completa puede consultarse en las oficinas del Ayuntamiento.",
        "doc_pgou" => "Documentación PMUS",
        "doc_participacion" => "Documentos participación ciudadana",
        "doc_regeneracion" => "Documentos Caminos escolares",
        "colaboracion_universidad" => "Colaboración con universidades"
    ],

    "version_preliminar_seccion" => [
        "titulo" => "Otros planes"
    ],

    "cofinanciacion_seccion" => [
        "titulo" => "Cofinanciación"
    ],

    "contacto_seccion" => [
        "titulo" => "Contacta con nosotros y danos tu opinión"
    ]
];