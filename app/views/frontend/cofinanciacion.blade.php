@extends('frontend.layout.base')

@section('contenido')

  <section id="page-title" class="page-title-video text-light"
           data-vide-bg="mp4:{{ url('frontend/video/encuesta') }}, poster:{{ url('frontend/video/encuesta_poster') }}"
           data-vide-options="position: 0% 70%"
           style="background: rgba(80,99,128,0.60);">
    <div class="container">
      <div class="page-title col-md-8">
        <h1>{{ trans('general.cofinanciacion_seccion.titulo') }}</h1>
      </div>
      <div class="col-md-12">
        <h4 class="text-center" style="font-size: 2.5rem;font-weight: 100;">{{ trans('general.documentacion_seccion.subtitulo') }}</h4>
      </div>
    </div>
  </section>

  <section class="container p-t-40 p-b-40">
    <div class="row">
      <img class="img-responsive col-md-8 col-md-offset-2" style="margin-top: 70px" src="{{ asset('frontend/img/logo/edusi_lliria.png') }}" alt="edusi_vinaros"><br>
    </div>
    <div class="row">
      <p class="text-justify lead">
        Los Fondos Estructurales y de Inversión Europeos son unos fondos que funcionan de modo conjunto a fin de apoyar la cohesión económica, social y territorial y alcanzar los objetivos de la Estrategia Europa 2020 de la Unión Europea (UE) para un crecimiento inteligente, sostenible e integrador. El Fondo Europeo de Desarrollo Regional (FEDER) es uno de ellos. 
      </p>
      <p class="text-justify lead">
        Por resolución de 18 de mayo de 2018, de la Presidencia de la Generalitat (DOGV 8301 de 23 de mayo de 2018) se resolvió la concesión de las ayudas convocadas por la citada resolución de 20 de noviembre de 2017, de la Presidencia de la Generalitat destinadas a subvencionar proyectos locales de actuación de los municipios de la Comunitat Valenciana de fomento de la movilidad urbana sostenible susceptibles de cofinanciación por los Programas Operativos FEDER de la Comunitat Valenciana 2014-2020.
      </p>
      <!-- <p class="text-justify lead">
        El Ayuntamiento de Vinaròs fue una de las entidades locales beneficiarias para la redacción del Plan de Movilidad Urbana Sostenible (PMUS) del municipio. 
      </p> -->
    </div>
  </section>

  <!-- <section class="container">
    <iframe src="http://mapifica.org/pmu_vinaros/index.html" style="border:none;height: 800px;width:100%;"></iframe>
  </section>
 -->

@endsection