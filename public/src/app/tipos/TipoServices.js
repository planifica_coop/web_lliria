var tiposServices = angular.module('tipos.services', ['ngResource']);

tiposServices.factory('Tipo', function($resource) {
    return $resource(CONFIG.URL.API + '/tipos/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        }
    });
});