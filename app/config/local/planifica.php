<?php

return array(
	'url' => '/planifica/public/',
	'urlAdjuntosImagenes' => '/planifica/public/uploads/images/proyectos/',
	'urlAdjuntosArticulos' => '/planifica/public/uploads/docs/articulos/',
);