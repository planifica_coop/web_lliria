<header id="header" class="header-dark header-transparent header-navigation-light">
  <div id="header-wrap">
    <div class="container">

      <!--LOGO-->
      <div id="logo" style="margin-right: 20px" class="m-t-10">
        <a target="_blank" href="{{ url ('http://www.lliria.es/') }}" class="logo"
           data-dark-logo="{{ asset('frontend/img/logo/logo_lliria.png') }}">
          <img src="{{ asset('frontend/img/logo/logo_lliria_negro.png') }}" alt="vinaros logo">
        </a>
        
      </div>
      <!--END: LOGO-->

      <!--MOBILE MENU -->
      <div class="nav-main-menu-responsive">
        <button class="lines-button x" type="button" data-toggle="collapse"
                data-target=".main-menu-collapse">
          <span class="lines"></span>
        </button>
      </div>
      <!--END: MOBILE MENU -->


      {{--<!--TOP SEARCH -->--}}
      {{--<div id="top-search"><a id="top-search-trigger"><i class="fa fa-search"></i><i class="fa fa-close"></i></a>--}}

      {{--<form action="search-results-page.html" method="get">--}}
      {{--<input type="text" name="q" class="form-control" value="" placeholder="Start typing & press  &quot;Enter&quot;">--}}
      {{--</form>--}}
      {{--</div>--}}
      {{--<!--END: TOP SEARCH -->--}}

      <!--NAVIGATION-->
      <div class="navbar-collapse collapse main-menu-collapse navigation-wrap">
        <div class="container">
          <nav id="mainMenu" class="main-menu mega-menu">
            <ul class="main-menu nav nav-pills">
              <li class=""><a href="{{ url('') }}"><i class="fa fa-home"></i></a>
              </li>
              
              <!-- <li>
                @if (strpos($view_name, 'home') !== false)
                  <a href="#tramitacion"><span style="">{{ trans('general.tramitacion') }}</span></a>
                @else
                  <a href="{{ url('/#tramitacion') }}"><span style="">{{ trans('general.tramitacion') }}</span></a>
                @endif
              </li> -->
              
               
              <li>
                <a href="{{ url('documentacion') }}">{{ trans('general.documentacion') }}</a>
              </li>

              <li>
                <a href="{{ url('comunicacion') }}">{{ trans('general.comunicacion') }}</a>
              </li>

              <!-- <li>
                <a href="{{ url('preliminar') }}">{{ trans('general.preliminar') }}</a>
              </li> -->

              <li>
                <a href="{{ url('cofinanciacion') }}">{{ trans('general.cofinanciacion') }}</a>
              </li>

              <li>
                <a href="{{ url('contacto') }}">{{ trans('general.contacto') }}</a>
              </li>
              {{--<li class="dropdown"><a href="#"><i class="fa fa-globe"></i></a>--}}
              {{--<ul class="dropdown-menu">--}}
              {{--@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)--}}
              {{--<li>--}}
              {{--<a rel="alternate" hreflang="{{$localeCode}}"--}}
              {{--href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">--}}
              {{--{{{ $properties['native'] }}}--}}
              {{--</a>--}}
              {{--</li>--}}
              {{--@endforeach--}}
              {{--</ul>--}}
              {{--</li>--}}
            </ul>
          </nav>
        </div>
      </div>
      <!--END: NAVIGATION-->
    </div>
  </div>
</header>
