<!DOCTYPE html>
{{--TODO cambiar multiidioma--}}
<html lang="{{ App::getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

  <link rel="shortcut icon" href="{{ url('/icon192.png') }}" sizes="192x192" type="image/x-icon">
  <title>PMUS Llíria</title>

  <!-- Bootstrap Core CSS -->
  {{--<link href="frontend/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">--}}
  {{--<link href="frontend/vendor/fontawesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">--}}
  {{--<link href="frontend/vendor/animateit/animate.min.css" rel="stylesheet">--}}
  <link href="{{ asset('frontend/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{ asset('frontend/vendor/fontawesome/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet">
  <link href="{{ asset('frontend/vendor/animateit/animate.min.css')}}" rel="stylesheet">
  <link href="{{ asset('frontend/vendor/cookie/cookiecuttr.css')}}" rel="stylesheet">


  <!-- Vendor css -->
  <link href="{{ asset('frontend/vendor/owlcarousel/owl.carousel.css') }}" rel="stylesheet">
  <link href="{{ asset('frontend/vendor/magnific-popup/magnific-popup.css') }}" rel="stylesheet">

  <!-- Template base -->
  <link href="{{ asset('frontend/css/theme-base.css') }}" rel="stylesheet">

  <!-- Template elements -->
  <link href="{{ asset('frontend/css/theme-elements.css') }}" rel="stylesheet">

  <!-- Responsive classes -->
  <link href="{{ asset('frontend/css/responsive.css') }}" rel="stylesheet">

  <!--[if lt IE 9]>
  <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
  <![endif]-->


  <!-- Template color -->
  <link href="{{ asset('frontend/css/color-variations/sigo.css') }}" rel="stylesheet" type="text/css" media="screen" title="sigo">

  <!-- LOAD GOOGLE FONTS -->
  {{--FIXME falta--}}
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600%7CRaleway:100,300,600,700,800" rel="stylesheet" type="text/css"/>
  {{--<link href='https://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>--}}

  <!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
  <link rel="stylesheet" property="stylesheet" href="{{ asset('frontend/vendor/rs-plugin/css/settings.css') }}" type="text/css" media="all"/>

  <!-- CSS CUSTOM STYLE -->
  <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/custom.css') }}" media="screen"/>

  <!--VENDOR SCRIPT-->
  <script src="{{ asset('frontend/vendor/jquery/jquery-1.11.2.min.js') }}"></script>
  <script src="{{ asset('frontend/vendor/plugins-compressed.js') }}"></script>

  <script src="{{ asset('frontend/vendor/cookie/jquery.cookie.js') }}"></script>
  <script src="{{ asset('frontend/vendor/cookie/jquery.cookiecuttr.js') }}"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-74633311-2"></script>

  <script>
    if(jQuery.cookie('cc_cookie_accept') == 'cc_cookie_accept') {
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-74633311-2');


    }
    $(document).ready(function () {
      $.cookieCuttr({
        cookieAnalytics: false,
        cookieMessage: 'Utilizamos cookies propias y de terceros para mejorar nuestros servicios. Si continua navegando, consideramos que acepta su uso. Puede cambiar la configuración u obtener más información <a href="{{ URL::to('legal/cookies')}}">aquí</a>',
        cookiePolicyLink: false,
        cookieAcceptButtonText: 'Acepto',
        cookieNotificationLocationBottom: true,
      });
    });
  </script>

</head>

<body class="wide">


<!-- WRAPPER -->
<div class="wrapper">

  <!-- HEADER -->
  @include('frontend.layout.header')
          <!-- END: HEADER -->


  @yield('contenido')

          <!-- FOOTER -->
  @include('frontend.layout.footer')
          <!-- END: FOOTER -->

</div>
<!-- END: WRAPPER -->

<!-- GO TOP BUTTON -->
<a class="gototop gototop-button" href="#"><i class="fa fa-chevron-up"></i></a>

<!-- Theme Base, Components and Settings -->
<script src="{{ asset('frontend/js/theme-functions.js') }}"></script>

<!-- Custom js file -->
<script src="{{ asset('frontend/js/custom.js') }}"></script>
@yield('javascript')

@if ( Config::get('app.debug') )
  {{--<script type="text/javascript">--}}
    {{--document.write('<script src="//localhost:35729/livereload.js?snipver=1" type="text/javascript"><\/script>')--}}
  {{--</script>--}}
@endif

</body>

</html>
