@extends('base')
@section('content')
@include('barra-navegacion')
<!-- Region 4 Wrap -->
<div class="container region4wrap">
  <div class="row maincontent">
    <div class="twelve columns">
      <div class="page_title">
        <div class="row">
          <div class="twelve columns">
            <h1>{{ ucwords(str_replace("<br>"," ",trans('nombres.sistema_calidad'))); }}</h1>
          </div>
        </div>
      </div>
    </div>
    <div class="twelve columns">
      <div class="row">
        <div class="five columns">
          <p>@lang('textos.sistema_calidad_texto')</p>
        </div>
        <div class="seven columns">
          <div class="article_media">
            <img src="{{URL::to('assets/images/calidad/calidad.png') }}" >
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Region 4 Wrap -->
@stop