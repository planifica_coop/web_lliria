articulosControllers.controller('ArticuloAddController', function($scope, $rootScope, $location, $routeParams, Articulo, sigoModal, $upload) {

    $scope.crearArticulo = function(articulo, adjunto) {
        console.log('Creando articulo ' + articulo.nombre_es + "...");

        if (adjunto != null) {
            var file = adjunto;
            $upload.upload({
                url: CONFIG.URL.API + '/adjuntos',
                file: file,
                fileFormDataName: 'adjunto',
            }).success(function(data, status, headers, config) {
                // file is uploaded successfully
                console.log(data);
                articulo.id_pla_adjuntos = data.id;
                Articulo.save({}, articulo, function(data) {
                    console.log('Articulo ' + data.nombre + ' creado.');
                    $location.path(CONFIG.URL.ARTICULOS);
                }, function() {
                    sigoModal.alerta('No se ha podido crear el articulo', 'Aceptar');
                });

            }).error(function() {
                
            });
        } else {
            articulo.id_pla_adjuntos = null;
            Articulo.save({}, articulo, function(data) {
                console.log('Articulo ' + data.nombre + ' creado.');
                $rootScope.setForm();
                $location.path(CONFIG.URL.ARTICULOS);
            }, function() {
                sigoModal.alerta('No se ha podido crear el articulo', 'Aceptar');
            });
        }
    };

    $scope.anyadirAdjunto = function($files) {
        $scope.adjunto = $files[0];
        $scope.adjuntoNombre = $files[0].name;
    };

    $scope.eliminarAdjunto = function(adjunto) {
        if ($scope.adjunto == null) {
            $scope.adjuntoEliminar = adjunto;
            $scope.articulo.id_pla_adjuntos = null;
        } else {
            $scope.adjunto = null;
            $scope.adjuntoNombre = "";
        }
    };

    $scope.cancelar = function() {
        $location.path(CONFIG.URL.ARTICULOS);
    };

    $scope.adjunto = null;
    $scope.adjuntoEliminar = null;
    $scope.adjuntoNombre = "";
    $scope.textoBoton = 'AÑADIR';
});