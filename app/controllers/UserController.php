<?php

class UserController extends BaseController {

    public function getLogin() {
        return View::make('users.login');        
    }

    public function postLogin()
    {
        try {
            $credentials = array(
                'email'    => Input::get('email'),
                'password' => Input::get('password') 
            );
            $user = Sentry::authenticate($credentials);
            if (!Sentry::check()) {
                return View::make('users.login');
            } else {
                Log::info('UserController: Usuario '.$user->email.' logeado con éxito.');
                return Redirect::to('admin');
            }            
        } catch (\Exception $e) {
            return View::make('users.login');
        }
    }

    public function getLogout() {
        Sentry::logout();
        return Redirect::to('/login');
    }

    // public function getCreateuser() {
    // // Create the group
    //     $group = Sentry::createGroup(array(
    //         'name'        => 'Admin',
    //         'permissions' => array(
    //             'admin' => 1,
    //         ),
    //     ));

    //     $user = Sentry::createUser(array(
    //         'email'     => 'admin',
    //         'password'  => 'admin',
    //         'activated' => true,            
    //     ));

    //     $adminGroup = Sentry::findGroupById(1);
    //     $user->addGroup($adminGroup);
    // }

}
