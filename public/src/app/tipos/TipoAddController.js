tiposControllers.controller('TipoAddController', function($scope, $rootScope, $location, $routeParams, Tipo, areas, sigoModal) {

    $scope.crearTipo = function(tipo) {
        console.log('Creando tipos ', tipo);
        Tipo.save({}, tipo, function(data) {
            $rootScope.setForm();
            $location.path(CONFIG.URL.TIPOS);
        }, function() {
            sigoModal.alerta('No se ha podido crear el tipo', 'Aceptar');
        });
    };

    $scope.cancelar = function() {
        $location.path(CONFIG.URL.TIPOS);
    };

    $scope.areas = areas;
    $scope.textoBoton = 'AÑADIR';
});