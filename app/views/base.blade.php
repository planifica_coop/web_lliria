<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />
  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" id="view" content="width=device-width minimum-scale=1, maximum-scale=1" />
  <title>planifica</title>
  <!-- Included CSS Files (Compressed) -->
  <link rel="stylesheet" href="{{URL::to('assets/stylesheets/foundation.min.css') }}">
  <link rel="stylesheet" href="{{URL::to('assets/stylesheets/app.css') }}">
  <link rel="stylesheet" href="{{URL::to('assets/stylesheets/style1.css') }}">
  <link rel="stylesheet" href="{{URL::to('assets/stylesheets/color1.css') }}">
  @yield('css')
  <!-- Plugins CSS -->
  <link href="{{URL::to('assets/plugins/titan/css/jquery.titanlighbox.css') }}" rel="stylesheet" type="text/css">
  <!-- Google Fonts -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Included JS Files (Compressed) -->
    <script src="{{URL::to('assets/javascripts/jquery.js') }}"></script>
    <script src="{{URL::to('assets/javascripts/foundation.min.js') }}"></script>
    <script src="{{URL::to('assets/javascripts/modernizr.foundation.js') }}"></script>
    <!-- Slider Revolution JS FILES  -->
    <script type="text/javascript" src="{{URL::to('assets/plugins/slider-revolution/jquery.themepunch.plugins.min.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('assets/plugins/slider-revolution/jquery.themepunch.revolution.min.js') }}"></script>
    <!-- Tooltips -->
    <script type="text/javascript" src="{{URL::to('assets/javascripts/jquery.tipsy.js') }}"></script>
    <!-- CarouFredSel plugin -->
    <script type="text/javascript"  src="{{URL::to('assets/javascripts/jquery.carouFredSel-6.0.3-packed.js') }}"></script>
    <!-- optionally include helper plugins -->
    <script type="text/javascript"  src="{{URL::to('assets/javascripts/jquery.touchSwipe.min.js') }}"></script>
    <!-- Titan Lightbox -->
    <script type="text/javascript" src="{{URL::to('assets/plugins/titan/js/prettify.js') }}"></script>
    <script type="text/javascript" src="{{URL::to('assets/plugins/titan/js/jquery.titanlighbox.js') }}"></script>
    <!-- Twitter -->
    <script type="text/javascript" src="{{URL::to('assets/javascripts/jquery.jtweetsanywhere-1.3.1.min.js') }}"></script>
    <!-- Scripts Initialize -->
    <script src="{{URL::to('assets/javascripts/app-head.js') }}"></script>
    <!-- IE Fix for HTML5 Tags -->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="main-wrapper">
      <!-- Region 2 Wrap -->
      <div class="container region2wrap">
        <div class="row">
          <!-- Logo -->
          <div class="three columns">
            <a href="{{ URL::to('') }}" id="logo">
              <img src="{{ URL::to('images/logo_planifica.png') }}" id="logo">
            </a>
            <div>
              <a hreflang="{{'es'}}" href="{{LaravelLocalization::getLocalizedURL('es') }}">castellano</a> | <a hreflang="{{'en'}}" href="{{LaravelLocalization::getLocalizedURL('en') }}">inglés</a>
            </div>
          </div>
          <!-- End Logo -->
          <!-- Main Navigation -->
          <div class="nine columns">
            <nav class="top-bar">
              <ul>
                <!-- Toggle Button Mobile -->
                <li class="name">
                  <h1><a href="#">{{ ucfirst(trans('textos.selecciona_pagina')) }}</a></h1>
                </li>
                <li class="toggle-topbar"><a href="#"></a></li>
                <!-- End Toggle Button Mobile -->
              </ul>
              <section><!-- Nav Section -->
              <ul class="right">
                <li>
                  <a class="{{ !Request::segment(2) ? 'active' : ''}}" href="{{URL::to('/') }}">{{ ucfirst(trans('nombres.inicio')) }}</a>
                </li>
                <li><a style="line-height: 25px;" class="{{Request::is('*/nosotros') ? 'active' : ''}}" href="{{URL::to('nosotros') }}">{{ ucfirst(trans('nombres.quienes_somos')) }}</a></li>
                <li><a style="line-height: 25px;" class="{{Request::is('*/servicios*') ? 'active' : ''}}" href="{{URL::to('servicios') }}">{{ ucfirst(trans('nombres.nuestros_servicios')) }}</a></li>
                <li><a class="{{Request::is('*/internacionalizacion') ? 'active' : ''}}" href="{{URL::to('internacionalizacion') }}">{{ ucfirst(trans('nombres.internacionalizacion')) }}</a></li>
                <li><a style="line-height: 25px;" class="{{Request::is('*/calidad') ? 'active' : ''}}" href="{{URL::to('calidad') }}">{{ ucfirst(trans('nombres.sistema_calidad')) }}</a></li>
                <li class="has-dropdown">
                  <a class="{{Request::is('*/idi') ? 'active' : ''}}" href="{{URL::to('idi') }}">{{ ucfirst(trans('nombres.i_d_i')) }}</a>
                  <ul class="dropdown">
                    <li><a href="{{URL::to('idi#articulos') }}">{{ ucfirst(trans('nombres.articulos_publicados_planifica')) }}</a></li>
                    <li><a href="{{URL::to('idi#proyectos_idi') }}">{{ ucfirst(trans('nombres.proyectos_idi')) }}</a></li>
                    <li><a href="{{URL::to('idi#aplicaciones_web') }}">{{ ucfirst(trans('nombres.aplicaciones_web')) }}</a></li>
                    <li><a href="{{URL::to('idi#monitorizacion') }}">{{ ucfirst(trans('nombres.monitorizacion_consumos')) }}</a></li>
                  </ul>
                </li>
                <li><a class="{{Request::is('*/contacto') ? 'active' : ''}}" href="{{URL::to('contacto') }}">{{ ucfirst(trans('nombres.contacto')) }}</a></li>
              </ul>
              </section><!-- End Nav Section -->
            </nav>
          </div>
          <!-- End Main Navigation -->
        </div>
      </div>
      <!-- End Region 2 Wrap -->
      <div class="sigoContent">
        @yield('content')
      </div>
      <!-- Region 9 Wrap -->
      <div class="container region9wrap">
        <!-- Footer -->
        <div class="row footer">
          <!-- // // // // // // // // // // -->
          <div class="four columns">
            <h4>{{ ucfirst(trans('nombres.sobre_nosotros')) }}</h4>
            <p>We say what we do, we do what we say, and we're looking forward to prove it to you.</p>
            <ul class="vcard">
              <li class="address">C/ San Vicente Nº4, 3 | 12002 - Castellón</li>
              <li class="tel"><a href="">(+34) 964 06 12 56</a></li>
              <li class="email"><a href="office@TouchM.com">info@planifica.org</a></li>
            </ul>
          </div>
          <!-- // // // // // // // // // // -->
        </div>
        <!-- End Footer -->
      </div>
      <!-- End Region 9 Wrap -->
      <!-- Region 10 Wrap -->
      <div class="container region10wrap">
        <div class="row footer_bottom">
          <!-- Bottom -->
          <!-- // // // // // // // // // // -->
          <div class="five columns">
            <p class="copyright">© 2012 TouchM by 4GraFx. All rights reserved</p>
          </div>
          <!-- // // // // // // // // // // -->
          <div class="seven columns">
            <ul class="link-list">
              <li><a href="{{URL::to('/') }}">{{ ucfirst(trans('nombres.inicio')) }}</a></li>
              <li><a href="{{URL::to('nosotros') }}">{{ ucfirst(trans('nombres.quienes_somos')) }}</a></li>
              <li><a href="{{URL::to('servicios') }}">{{ ucfirst(trans('nombres.nuestros_servicios')) }}</a></li>
              <li><a href="{{URL::to('internacionalizacion') }}">{{ ucfirst(trans('nombres.internacionalizacion')) }}</a></li>
              <li><a href="{{URL::to('calidad') }}">{{ ucfirst(trans('nombres.sistema_calidad')) }}</a></li>
              <li><a href="{{URL::to('contacto') }}">{{ ucfirst(trans('nombres.contacto')) }}</a></li>
            </ul>
          </div>
          <!-- // // // // // // // // // // -->
          <!-- Bottom -->
        </div>
      </div>
      <!-- End Region 10 Wrap -->
      <!-- Initialize JS Plugins -->
      <script src="{{URL::to('assets/javascripts/app.js') }}"></script>
      @yield('javascript')
    </div>
  </body>
</html>