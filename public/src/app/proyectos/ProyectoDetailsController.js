proyectosControllers.controller('ProyectoDetailController', function($scope, $location, $routeParams, proyecto) {

    $scope.cancelar = function() {
        $location.path(CONFIG.URL.PROYECTOS);
    };

    $scope.proyecto = proyecto;
});