var paneles = angular.module('paneles', []);

paneles.directive('panelNavegacion', function($translate, $filter, OpcionesService) {
    return {
        restrict: 'E',
        templateUrl: 'src/common/paneles/panel-navegacion.html',
        controller: function($scope, $location) {
            $scope.usuario = global.userEmail;
            $scope.opcion = 1;
            $scope.opciones = OpcionesService;
            $scope.lenguages = [{
                nombre: $filter('capitalize')($filter('translate')('ESPAÑOL')),
                lang: 'es_ES',
                imagen: 'assets/images/lang/ES.png'
            },
            {
                nombre: $filter('capitalize')($filter('translate')('INGLES')),
                lang: 'en_US',
                imagen: 'assets/images/lang/EN.png'
            }];
            $scope.setOpcion = function(opcion) {
                $scope.opcion = opcion.id;
                $location.path(opcion.url);
            };
            $scope.isSeleccionada = function(opcion) {
                return $location.path() === opcion.url || $location.path().indexOf(opcion.url + '/') > -1;
            };
            $scope.setLenguage = function(lenguage) {
                $translate.use(lenguage.lang);
            }
        }
    };
});

paneles.directive('panelBreadcrumb', function(OpcionesService) {
    return {
        restrict: 'E',
        templateUrl: 'src/common/paneles/panel-breadcrumb.html',
        controller: function($scope, $location) {
            $scope.opciones = [];
            var secciones = $location.path().split('/');
            $scope.opciones.push(OpcionesService[0]);
            for (i = 1; i < secciones.length; i++) {
                var result = $.grep(OpcionesService, function(e, index) {
                    if ("subOpciones" in e) {
                        for (j = 0; j < e.subOpciones.length; j++) {
                            if (e.subOpciones[j].url == ('/' + secciones[i])) {
                                $scope.opciones.push(e.subOpciones[j]);
                            }
                        }
                    }
                });


            }

            if (secciones[secciones.length - 1] == 'add') {
                $scope.opciones.push({
                    nombre: 'Añadir'
                });
            } else if (secciones[secciones.length - 2] == 'edit') {
                $scope.opciones.push({
                    nombre: 'Editar'
                });
            }
        }
    };
});