var areas = angular.module('areas', ['areas.controllers', 'areas.services']);

var areasControllers = angular.module('areas.controllers', ['ngTable', 'utils']);

areas.config(function($routeProvider) {
    $routeProvider.when(CONFIG.URL.AREAS, {
        templateUrl: 'src/app/areas/areas-list.html',
        controller: 'AreaListController'
    }).when(CONFIG.URL.AREAS + CONFIG.URL.ADD, {
        templateUrl: 'src/app/areas/areas-add.html',
        controller: 'AreaAddController',
        resolve: {
            areas: function(Area) {
                return Area.query();
            }
        }
    }).when(CONFIG.URL.AREAS + CONFIG.URL.EDIT + '/:id', {
        templateUrl: 'src/app/areas/areas-edit.html',
        controller: 'AreaEditController',
        resolve: {
            areas: function(Area) {
                return Area.query();
            }
        }
    })
});