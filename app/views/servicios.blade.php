@extends('base')
@section('css')
<link rel="stylesheet" href="{{URL::to('assets/stylesheets/offcanvas.css') }}">
@stop
@section('javascript')
<!-- Isotope Portfolio -->
<script type="text/javascript" src="{{ URL::to('assets/javascripts/jquery.isotope.min.js') }}"></script>
<script>
  $("body").addClass("off-canvas");
  $(window).load(function(){
      var $container = $('#container');
      $container.isotope({
        itemSelector : '.element',
        // disable resizing
        resizable: false,
        // set columnWidth to a percentage of container width
        masonry: {
          columnWidth: $container.width() / 12
        },
      });
      $container.isotope({
        filter: '.proyecto'
    });
      
      // update columnWidth on window resize
      $(window).smartresize(function(){
        $container.isotope({
          // set columnWidth to a percentage of container width
          masonry: {
            columnWidth: $container.width() / 12
          }
        });
      });
        var $optionSets = $('#options .option-set'),
            $optionLinks = $optionSets.find('a');
        $optionLinks.click(function(){
          var $this = $(this);
          // don't proceed if already selected
          if ( $this.hasClass('selected') ) {
            return false;
          }
          var $optionSet = $this.parents('.option-set');
          $optionSet.find('.selected').removeClass('selected');
          $this.addClass('selected');
    
          // make option object dynamically, i.e. { filter: '.my-filter-class' }
          var options = {},
              key = $optionSet.attr('data-option-key'),
              value = $this.attr('data-option-value');
          // parse 'false' as false boolean
          value = value === 'false' ? false : value;
          options[ key ] = value;
          if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
            // changes in layout modes need extra logic
            changeLayoutMode( $this, options )
          } else {
            // otherwise, apply new options
            $container.isotope( options );
          }
          
          return false;
        });
    });

</script>
<!-- FlexSlider -->
<script defer src="{{ URL::to('assets/plugins/flexislider/jquery.flexslider.js') }}"></script>
<!-- Initialize JS Plugins -->
<script src="{{ URL::to('assets/javascripts/jquery.offcanvas.js') }}"></script>
@stop
@section('content')
@include('barra-navegacion')
<!-- Region 4 Wrap -->
<div class="container region4wrap">
  <div class="row maincontent">
    <div class="twelve columns">
      <div class="page_title">
        <h1>{{ ucwords(str_replace("<br>"," ",trans('nombres.nuestros_servicios'))); }}</h1>
      </div>
    </div>
    <!-- Off Canvas -->
    <div class="twelve columns">
      <header id="header" class="row">
        <div class="twelve columns phone-two">
          <p class="show-for-small">
          <a class='sidebar-button button sidebarButton top20' href="#sidebar" >@lang('textos.seleccionar_categoria')</a>
          </p>
        </div>
      </header>
      <div class="row">
        <section role="main">
          <div class="row">
            <!-- Portfolio Content -->
            <div class="twelve columns">
              <div class="row">
                <div id="container" class="clickable variable-sizes clearfix">
                  @foreach ($areas as $area)
                  <div class="columns element logo area{{ $area->id }} @foreach ($area->tipos as $tipo)tipo{{ $tipo->id }} @endforeach clearfix">
                    {{$area->getDescripcion()}}
                  </div>
                  @endforeach
                  @foreach ($proyectos as $proyecto)
                  
                  @if ($proyecto->visible)
                  <!-- Portfolio Item -->
                  <div class="four columns element logo print web proyecto @foreach ($proyecto->areas as $area)area{{ $area->id }} @endforeach @foreach($proyecto->tipos as $tipo)tipo{{ $tipo->id }} @endforeach">
                    <div class="portfolio-item">
                      <div class="portfolio-item-image image-overlay">
                        @if ($proyecto->imagen)
                          <a class="titan-lb" data-titan-group="gallery" href="{{ URL::to('uploads/images/proyectos/' . $proyecto->imagen->id . '.' . $proyecto->imagen->extension) }}" title="{{ $proyecto->getNombre() }}.">
                          <img src="{{ URL::to('uploads/images/proyectos/' . $proyecto->imagen->id . '.' . $proyecto->imagen->extension) }}" alt="" />
                        @else
                          <a class="titan-lb" data-titan-group="gallery" href="{{ URL::to('assets/images/works/1.jpg') }}" title="{{ $proyecto->getNombre() }}.">
                          <img src="{{ URL::to('assets/images/works/1.jpg') }}" alt="" />
                        @endif
                        <span class="overlay-icon item-zoom"></span>
                        </a>
                      </div>
                      <div class="portfolio-item-content">
                        <h5 class="title"><a href="{{ URL::to('servicios/proyecto/' . $proyecto->id) }}">{{ $proyecto->getNombre() }}</a></h5>
                        <p>
                        @foreach ($proyecto->areas as $key => $area)
                          {{ $area->getNombre() }}
                          @if ($key < (count($proyecto->areas)-1))
                            /
                          @endif
                        @endforeach
                        </p>
                      </div>
                    </div>
                  </div>
                  <!-- End Portfolio Item -->
                  @endif
                  @endforeach
                </div>
              </div>
            </div>
            <!-- End Portfolio Content -->
          </div>
        </section>
        <section id="sidebar" role="complementary">
          <!-- Portfolio Navigation -->
          <div class="sidebar-widget">
            <section id="options">
              <ul id="filters" class="accordion option-set side-options top20" data-option-key="filter">
                <li><a href="#filter" data-option-value=".proyecto" class="accordion-title">{{ ucfirst(trans('nombres.todos')) }}</a></li>
                @foreach ($areas as $area)
                <li><a href="#filter" data-option-value=".area{{$area->id}}" class="accordion-title" stus="">{{ $area->getNombre()}}</a>
                  <div class="accordion-content" style="display: none;">
                    @foreach ($area->tipos as $tipo)
                    <a href="#filter" data-option-value=".tipo{{$tipo->id}}">
                    {{$tipo->getNombre()}}
                    </a>
                    @endforeach
                  </div></li>
                  @endforeach
                </ul>
              </section>
            </div>
            <!-- End Portfolio Navigation -->
          </section>
        </div>
      </div>
      <!-- End Off Canvas -->
    </div>
  </div>
  @stop