-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 02-12-2014 a las 13:59:43
-- Versión del servidor: 5.5.40-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `planifica`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '{"admin":1}', '2014-08-19 09:07:35', '2014-08-19 09:07:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2012_12_06_225921_migration_cartalyst_sentry_install_users', 1),
('2012_12_06_225929_migration_cartalyst_sentry_install_groups', 1),
('2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot', 1),
('2012_12_06_225988_migration_cartalyst_sentry_install_throttle', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pla_adjuntos`
--

CREATE TABLE IF NOT EXISTS `pla_adjuntos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ruta` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `nombre` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Volcado de datos para la tabla `pla_adjuntos`
--

INSERT INTO `pla_adjuntos` (`id`, `ruta`, `created_at`, `updated_at`, `nombre`) VALUES
(18, '/docs/articulos/download.jpg', '2014-09-09 14:14:58', '2014-09-09 14:14:58', 'download.jpg'),
(19, '/docs/articulos/GIF005.gif', '2014-09-09 14:15:16', '2014-09-09 14:15:16', 'GIF005.gif'),
(20, '/docs/articulos/AngularJS_Succinctly.pdf', '2014-09-15 11:30:39', '2014-09-15 11:30:39', 'AngularJS_Succinctly.pdf'),
(21, '/docs/articulos/AngularJS_Succinctly.pdf', '2014-09-15 11:31:02', '2014-09-15 11:31:02', 'AngularJS_Succinctly.pdf'),
(22, '/docs/articulos/AngularJS_Succinctly.pdf', '2014-09-15 11:33:46', '2014-09-15 11:33:46', 'AngularJS_Succinctly.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pla_areas`
--

CREATE TABLE IF NOT EXISTS `pla_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_es` text NOT NULL,
  `nombre_en` text NOT NULL,
  `nombre_fr` text NOT NULL,
  `descripcion_es` text NOT NULL,
  `descripcion_en` text NOT NULL,
  `descripcion_fr` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `pla_areas`
--

INSERT INTO `pla_areas` (`id`, `nombre_es`, `nombre_en`, `nombre_fr`, `descripcion_es`, `descripcion_en`, `descripcion_fr`, `created_at`, `updated_at`) VALUES
(1, 'urbanismo y ordenación del territorio', '', '', 'PLANIFICA gestiona y redacta planes urbanísticos y de ordenación del territorio (planes de acción territorial, planes generales, planes parciales, planes de reforma interior, planes parciales y de reforma interior modificativos de la ordenación estructural, planes especial, catálogos de bienes y espacios protegidos, estudios de detalle,etc.)\r\nAsimismo la empresa ofrece servicios enfocados a la viabilidad económica de los desarrollos urbanísticos: estudios de mercado, estudios de sostenibilidad económica, dossieres de venta, etc.', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'ingeniería civil', '', '', 'PLANIFICA cuenta con amplia experiencia en la redacción de proyectos de ingeniería civil, especialmente proyectos de urbanización, tanto nuevos desarrollos como remodelaciones del espacio urbano existente. Igualmente redacta proyectos de infraestructuras asociadas al desarrollo urbano, como: accesos, redes de servicios, pequeñas depuradoras, instalaciones de alumbrado exterior, sistema de recogida de residuos sólidos urbanos, etc. Cuando se requiere también se realizan direcciones de obra y coordinaciones de seguridad y salud. Incluso las gestiones necesarias con constructoras.', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'ingeniería ambiental', '', '', 'PLANIFICA ofrece servicios relacionados con los problemas ambientales derivados de la transformación del suelo, esenciales para garantizar la sostenibilidad de los desarrollos urbanos, abarcando aspectos como la movilidad, impacto ambiental, paisaje, ciclo integlal del agua, energía, etc. Así redacta estudios de tráfico y movilidad, estudios acústicos, estudios de impacto ambiental e informes de sostenibilidad ambiental, estudios de paisaje, estudios de integración paisajística...', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'ingeniería industrial', '', '', 'Dentro del área de ingeniería industrial se realiza el diseño y cálculo de todo tipo de instalaciones (eléctricas, agua caliente sanitaria, climatización, telecomunicaciones, evacuación de aguas pluviales, seguridad, etc.) tanto de los edificios cómo de las urbanizaciones. Además la empresa tiene una amplia experiencia en la legalización de todo tipo actividades e instalaciones, realizando desde la redacción de estudios de consultoría y viabilidad, gestión de licencias y permisos, proyectos básicos y de ejecución, asistencias técnicas a la construcción, direcciones de obra, etc.\r\n\r\nEntre los objetivos de este área se encuentra la expansión hacia nuevos campos de actividad relacionados con las energías renovables y el ahorro energético.\r\n\r\nConstrucción de 4 naves industriales para Monte 2010 en polígono industrial SUPOI-05 en Almazora (Castellón)\r\nAlumbrado, instalación eléctrica en BT y MT del proyecto de Urbanización del sector industrial SUI 06 del PGOU de Vinaroz (Castellón).\r\nProyectos de instalación eléctrica, aire acondicionado, licencia ambiental, gas, fontanería, saneamiento, energía solar térmica y dirección de obra en Club Deportivo IMAPALA, con 28 PISTAS de Padel, restaurante, vestuarios etc..en el Grao de Castellón.\r\nProyecto de Instalaciones para Edificio de 100 viviendas y 10 apartamentos con sotano y garaje en la parcela 7 del PRI sector SRC-IBM en la Pobla de Vallbona ( Valencia)\r\nProyecto de instalaciones para conjunto de 10 bloques para 290 viviendas, con zona ajardinada, piscina garaje y trasteros en finca del Sector-03 SUR-Villa Dolores en Castellón.\r\nProyecto de instalación eléctrica, aire acondicionado, licencia ambiental, gas, fontanería saneamineto, energía solar térmica y Telcomunicaciones en Centro de Negocios “La Huella”, con un total de 42 despachos ubicado junto al centro comercial La Salera en Catellón.', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'arquitectura', '', '', 'Dentro del área de arquitectura se llevan a cabo los trabajos relacionados con la edificación, habiendo realizado proyectos edificatorios de todo tipo y en toda su extensión (viviendas, industriales, comerciales, administrativos, deportivos,…). Las actividades son: estudio de solares y viabilidad económica de promociones, anteproyectos, proyectos básicos, proyectos de ejecución (incluido el diseño y cálculo de estructuras y de todas las instalaciones del edificio), direcciones de obra y coordinaciones de seguridad y salud. Gestión integral de construcción y promoción. Para determinados clientes se ofrece un servicio integral, ejerciendo la figura de promotor y/o contratista principal de la obra.\r\n\r\nResidencial edificio Mozart, 24 viviendas en Castellón.\r\nResidencial Viure, 36 viviendas en Castellón.\r\nResidencial Pobla Vallbona, 212 viviendas y barrio residencial en la Pobla de Vallbona (Valencia)\r\nResidencial Vinaros,136 viviendas y locales en Vinaroz (Castellón)\r\nResidencial La volta, 70 viviendas en Peñiscola (Castellón)\r\nResidencial Llanells, 20 viviendas en Peñiscola (Castellón)\r\nResidencial Grao, 25 viviendas en el Grao de Castellón.\r\nResidencial Grao II, 15 viviendas en el Grao de Castellón.\r\nResidencial 82 adosados en Benicassim (Castellón).\r\nResidencial 80 viviendas en altura Benicassim (Castellón).\r\nResidencial Viella, 6 apartamentos en Villa (Andorra).\r\nHotel Majestic (4*)de 160 habitaciones, en Vinaroz (Castellón).\r\nApartahotel de 12 unidades en Buenos Aires (Argentina)\r\nHospital Jaime I en Castellón.\r\nColegio Privado Británico Elians en Castellón.\r\nColegio Público Dean Marti en Oropesa (Castellón).\r\nBiblioteca Pública en Villareal (Castellón)\r\nEstamos especializados principalmente en la gestión de proyectos de viviendas, habiendo proyectado y construido un total de más de 4.500 viviendas de todo tipo y contamos con amplia experiencia de construcción y contratación por cuenta propia, en menor volumen, lo que nos permite un buen control de los precios y gestión del proyecto durante la construcción.', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pla_articulos`
--

CREATE TABLE IF NOT EXISTS `pla_articulos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_es` text NOT NULL,
  `fecha` text NOT NULL,
  `descripcion` text,
  `titulo_es` text NOT NULL,
  `autores` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `id_pla_adjuntos` int(11) DEFAULT NULL,
  `nombre_en` text NOT NULL,
  `nombre_fr` text NOT NULL,
  `titulo_en` text NOT NULL,
  `titulo_fr` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `pla_articulos`
--

INSERT INTO `pla_articulos` (`id`, `nombre_es`, `fecha`, `descripcion`, `titulo_es`, `autores`, `created_at`, `updated_at`, `id_pla_adjuntos`, `nombre_en`, `nombre_fr`, `titulo_en`, `titulo_fr`) VALUES
(1, 'I CONGRESO NACIONAL DE URBANISMO Y ORDENACIÓN DEL TERRITORIO', 'Bilbao 7,8, 9 de Mayo de 2.008', '', 'INTEGRACIÓN DE LAS AGUAS PLUVIALES EN EL PAISAJE URBANO: UN VALOR SOCIAL A FOMENTAR', 'SARA PERALES; Ing. Caminos, C. y P. \r\nConsultoría (PME enginyería), GONZALO VALLS. Ing. Caminos, C. y P. Consultoría (PLANIFICA)', '0000-00-00 00:00:00', '2014-09-10 09:52:26', NULL, '', '', '', ''),
(3, 'INTERNATIONAL CONGRESS ON ACOUSTICS', 'Sydney 23-27 de Agosto de 2.010', NULL, 'MULTISOURCE INDUSTRIAL PLANT INVERSE NOISE MODELLING AND ASSESSMENT AGAINST ISO 8297', 'Daniel Castro Cirac (1), Jorge Reverter García (2), Michael Kidner (3)\n(1) VIPAC Engineers & Scientists Ltd., Sydney, Australia \n(2) Planifica Urbanismo y Gestión S.L., Castelló, Spain\n(3) VIPAC Engineers & Scientists Ltd., Adelaide, Australia', '2014-09-02 10:01:45', '2014-09-10 09:52:33', NULL, '', '', '', ''),
(4, 'II CONGRESO NACIONAL DE URBANISMO Y ORDENACIÓN DEL TERRITORIO', 'Madrid 24 de Marzo de 2.011', NULL, 'EL POLÍGONO INDUSTRIAL PÚBLICO SOTERRANYES (VINARÒS); EJEMPLO DEL NUEVO DISEÑO URBANO DÓNDE LA INTEGRACIÓN PAISA JI ST IC A Y MEDIOAMBIENTAL SE ELEVAN A CRITERIOS DEL MISMO RANGO QUE LOS TRADICIONALES DE ECONOMÍA, FUNCIONALIDAD Y BELLEZA', 'GONZALO VALLS BENAVIDES. Ing. Caminos, C. y P. Consultoría; ELISABETH QUINTANA SEGUÍ. Arquitecta y Paisajista. \nConsultoría. ANA ARNAU PALTOR Arquitecta municipal. Administración Pública.', '2014-09-02 10:02:16', '2014-09-10 09:52:50', NULL, '', '', '', ''),
(5, 'URBAN FLOOD RISK MANAGEMENT (UFRIM). Aproaches to enhance resilience of communities. INTERNATIONAL SYMPOSIUM', '21st – 23rd September 2011 – Graz, Austria', NULL, 'URBAN PLANNING AND WATER CORRIDORS. A CASE STUDY OF INTEGRATION OF THE HYDROLOGICAL CHARACTERISTICS INTO THE SITE LAYOUT', 'SARA PERALES; Ing. Caminos, C. y P.\nConsultoría (PME enginyería), GONZALO VALLS. Ing. Caminos, C. y P. Consultoría (PLANIFICA); PETER COOMBS; Civil Engineer, software (MICRO DRAINAGE)', '2014-09-02 10:02:48', '2014-09-10 09:54:23', NULL, '', '', '', ''),
(6, 'III SEMANA INTERNACIONAL DEL ANÁLISIS DE RIESGOS, SEGURIDAD DE PRESAS Y GESTIÓN DE INFRAESTRUCTURAS CRÍTICAS', '17-18 Octubre 2011, Valencia, España.', NULL, 'MAPAS DE PELIGROSIDAD POR INUNDACIÓN PLUVIAL: HERRAMIENTA Y CASOS DE ESTUDIO', 'SARA PERALES; Ing. Caminos, C. y P.\nConsultoría (PME enginyería), GONZALO VALLS. Ing. Caminos, C. y P. Consultoría (PLANIFICA); PETER COOMBS; Civil Engineer, software (MICRO DRAINAGE).', '2014-09-02 10:03:11', '2014-09-10 09:54:26', NULL, '', '', '', ''),
(7, 'REVISTA PAISEA', '1er Trimestre 2012', NULL, 'SISTEMAS DE DRENAJE SOSTENIBLE (SuDS)', 'SARA PERALES; Ing. Caminos, C. y P.\nConsultoría (PME enginyería), GONZALO VALLS. Ing. Caminos, C. y P. Consultoría (PLANIFICA)', '2014-09-02 10:03:31', '2014-09-10 09:54:37', NULL, '', '', '', ''),
(8, 'III JORNADAS DE INGENIERÍA DEL AGUA', '23 y 24 de Octubre 2013', NULL, 'BLUECITY: SISTEMA DE INDICADORES PARA LA EVALUACIÓN DE LA GESTIÓN DEL CICLO INTEGRAL DEL AGUA EN ENTORNOS URBANOS', 'V. Agost Alcón, J. Carmona Esteve, G. Valls Benavides (PLANIFICA)\nSara Perales (PME enginyería)', '2014-09-02 10:03:55', '2014-09-10 09:54:35', NULL, '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pla_imagenes`
--

CREATE TABLE IF NOT EXISTS `pla_imagenes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  `extension` text NOT NULL,
  `id_pla_proyectos` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Volcado de datos para la tabla `pla_imagenes`
--

INSERT INTO `pla_imagenes` (`id`, `nombre`, `extension`, `id_pla_proyectos`, `created_at`, `updated_at`) VALUES
(3, 'download.jpg', 'jpg', 3, '2014-09-04 12:01:16', '2014-09-04 12:01:16'),
(4, 'download.jpg', 'jpg', 3, '2014-09-04 12:01:30', '2014-09-04 12:01:30'),
(14, 'GIF005.gif', 'gif', 1, '2014-09-05 08:34:44', '2014-09-05 08:34:44'),
(15, 'GIF005.gif', 'gif', 1, '2014-09-05 08:35:51', '2014-09-05 08:35:51'),
(26, 'download.jpg', 'jpg', 1, '2014-09-08 09:12:00', '2014-09-08 09:12:00'),
(27, 'proyecto.jpg', 'jpg', 19, '2014-09-09 14:24:32', '2014-09-09 14:24:32'),
(28, 'proyecto.jpg', 'jpg', 19, '2014-09-09 15:37:23', '2014-09-09 15:37:23'),
(29, 'proyecto.jpg', 'jpg', 42, '2014-09-09 16:12:32', '2014-09-09 16:12:32'),
(30, 'proyecto.jpg', 'jpg', 19, '2014-09-10 11:36:00', '2014-09-10 11:36:00'),
(31, 'proyecto.jpg', 'jpg', 45, '2014-09-10 11:38:07', '2014-09-10 11:38:07'),
(32, 'proyecto.jpg', 'jpg', 1, '2014-09-17 11:40:15', '2014-09-17 11:40:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pla_proyectos`
--

CREATE TABLE IF NOT EXISTS `pla_proyectos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_es` text NOT NULL,
  `nombre_en` text NOT NULL,
  `nombre_fr` text NOT NULL,
  `municipio` text NOT NULL,
  `region` text NOT NULL,
  `importe_adjudicacion` double NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `cliente_nombre` text NOT NULL,
  `cliente_tipo` text NOT NULL,
  `descripcion_es` text NOT NULL,
  `descripcion_en` text NOT NULL,
  `descripcion_fr` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `tiene_ficha` tinyint(1) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `id_pla_imagenes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Volcado de datos para la tabla `pla_proyectos`
--

INSERT INTO `pla_proyectos` (`id`, `nombre_es`, `nombre_en`, `nombre_fr`, `municipio`, `region`, `importe_adjudicacion`, `fecha_inicio`, `fecha_fin`, `cliente_nombre`, `cliente_tipo`, `descripcion_es`, `descripcion_en`, `descripcion_fr`, `created_at`, `updated_at`, `tiene_ficha`, `visible`, `lat`, `lng`, `id_pla_imagenes`) VALUES
(1, 'Redacción de los documentos técnicos necesarios para el desarrollo del nuevo polígono industrial "Soterranyes" en Vinaròs (Castellón)', 'df', 'sdf', 'Vinaròs', 'Castellón (Comunidad Valenciana)', 349.429, '2010-04-01', '2010-06-01', 'Ayuntamiento de Vinaròs', 'Administración Publica', '<p>Redacción de todos los documentos técnicos y jurídicos necesarios para...</p>', '<p>Descripcin ingles</p>', '', '0000-00-00 00:00:00', '2014-09-17 11:40:24', 1, 1, 39.990799335838, -0.06317138671875, 32),
(3, 'PGOU de Argelita (Castellón) | redacción', '', '', 'Argelita', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-08-25 11:16:42', '2014-08-28 11:58:21', 0, 0, 0, 0, 0),
(4, 'PGOU de Cirat (Castellón) | redacción', '', '', 'Cirat', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-08-25 11:17:21', '2014-09-08 11:16:43', 0, 0, 0, 0, 0),
(5, 'PGOU de Villanueva de Viver (Castellón) | redacción', '', '', 'Villanueva de Vive', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-08-25 11:17:50', '2014-08-28 10:42:04', 0, 0, 0, 0, 0),
(6, 'PGOU de Fuente la Reina (Castellón) | redacción', '', '', 'Fuente la Reina', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-08-25 11:18:04', '2014-08-28 10:42:17', 0, 0, 0, 0, 0),
(7, 'PGOU de Torreblanca (Castellón) | colaboración', '', '', 'Torreblanca', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-08-25 11:18:21', '2014-08-28 10:42:33', 0, 0, 0, 0, 0),
(8, 'PGOU de Sta Magdalena de Pullpis (Castellón) | colaboración', '', '', 'Sta Magdalena de Pullpis', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-08-25 11:18:36', '2014-08-28 10:42:45', 0, 0, 0, 0, 0),
(9, 'PGOU de Borriol (Castellón) | colaboración', '', '', 'Borriol', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-08-25 11:18:52', '2014-08-28 10:42:55', 0, 0, 0, 0, 0),
(15, 'Proyecto de Urbanización de la UE1 del Sector Industrial ZIN 1 del PGOU de Vilanova d''Alcolea (Castellón)', '', '', 'Vilanova Alcolea', 'Castellón (Comunidad Valenciana)', 30000, '2010-06-01', '2010-09-01', 'Vía Taller Arquitectura', 'Estudio Arquitectura', '<p>Se trata de un nuevo polígono industrial, tanto para naves tipo nido como para grandes naves. Se encuentra ubicado en las inmediaciones del Aeropueto de Castellón, contando con una buena conexión con la red de carreteras de la provincia (CV-13 y CV-10).</p>', '', '', '2014-08-28 10:50:03', '2014-09-03 09:17:48', 1, 1, 0, 0, 0),
(18, 'Plan Parcial del Sector en suelo urbanizable no pormenorizado residencial, SUNPOR 6, de Almassora (Castellón). 176.594 m²', '', '', 'Almassora', 'Castellón (Comunidad Valenciana)', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-09-01 11:45:43', '2014-09-03 08:57:30', 0, 0, 0, 0, 0),
(19, 'sdf', 'sdf', 'sdf', 'sdf', 'sdf', 0, '0000-00-00', '0000-00-00', 'sdf', 'Administración Publica', '', '', '', '2014-09-03 08:43:16', '2014-09-17 10:19:38', 1, 1, 0, 0, 27),
(20, 'Plan Parcial de mejora y Anteproyecto de "Sol de Riu Golf", reclasificación a suelo urbanizable residencial, Vinaròs (Castellón). 1.097.842 m²', '', '', 'Vinaròs', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-09-03 09:04:52', '2014-09-05 11:45:01', 0, 0, 0, 0, 0),
(21, 'Texto Refunfido del Plan Parcial en suelo urbanizable residencial SUR 9, Vinaròs (Castellón). 154.315 m²', '', '', 'Vinaròs', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-09-03 09:05:25', '2014-09-03 09:05:25', 0, 0, 0, 0, 0),
(22, 'Texto Refundido del Plan Parcial en suelo urbanizable residencial SUR 13, Vinaròs (Castellón)', '', '', 'Vinaròs', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-09-03 09:05:55', '2014-09-03 09:05:55', 0, 0, 0, 0, 0),
(23, 'Texto Refundido del Plan Parcial en suelo urbanizable residencial del Sector "Les Salines", Vinaròs (Castellón). 346.963 m²', '', '', 'Vinaròs', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-09-03 09:06:17', '2014-09-03 09:06:17', 0, 0, 0, 0, 0),
(24, 'Modificaciones puntuales de planes generales: SUI-13 en Castellón; Camping en Vinaròs', '', '', 'Vinaròs', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-09-03 09:06:56', '2014-09-03 09:06:56', 0, 0, 0, 0, 0),
(25, 'Plan Director de Aguas Pluviales y Residuales de Vinaròs (Castellón)', '', '', 'Vinaròs', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-09-03 09:07:17', '2014-09-03 09:07:17', 0, 0, 0, 0, 0),
(26, 'Plan Director de de infraestructuras de la Costa Norte de Vinaròs (Castellón)', '', '', 'Vinaròs', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-09-03 09:07:45', '2014-09-03 09:07:45', 0, 0, 0, 0, 0),
(27, 'Estudios de detalle y planes de reforma interior en pequeñas actuaciones en la provincia de Castellón', '', '', 'Castellón', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-09-03 09:08:05', '2014-09-03 09:08:05', 0, 0, 0, 0, 0),
(28, 'Declaraciones de Interés Comunitario de actuaciones terciarias', '', '', 'Castellón', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-09-03 09:08:34', '2014-09-03 09:08:34', 0, 0, 0, 0, 0),
(29, 'Estudios de viabilidad económica de diversas actuaciones urbanísticas', '', '', 'Castellón', 'Castellón', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-09-03 09:08:55', '2014-09-03 09:08:55', 0, 0, 0, 0, 0),
(30, 'Proyecto de Urbanización del Sector Doña Blanca Golf en Torreblanca (Castellón)\0', '', '', 'Torreblanca', 'Castellón (Comunidad Valenciana)', 100500, '2010-04-01', '2011-03-01', 'Valdar', 'Ingeniería', '<p>Proyecto de urbanización</p>', '', '', '2014-09-03 09:15:04', '2014-09-03 09:20:24', 1, 1, 40.219650296673, 0.19689559936523, 0),
(44, 'sfdfd', '', '', 'fdg', 'fdg', 0, '0000-00-00', '0000-00-00', '', '', '', '', '', '2014-09-10 08:26:51', '2014-09-10 08:26:51', 0, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pla_proyectos_areas`
--

CREATE TABLE IF NOT EXISTS `pla_proyectos_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pla_proyectos` int(11) NOT NULL,
  `id_pla_areas` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Volcado de datos para la tabla `pla_proyectos_areas`
--

INSERT INTO `pla_proyectos_areas` (`id`, `id_pla_proyectos`, `id_pla_areas`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 2, 2, '2014-08-27 09:54:28', '2014-08-27 09:54:28'),
(18, 3, 1, '2014-08-27 10:33:07', '2014-08-27 10:33:07'),
(19, 13, 1, '2014-08-27 14:45:51', '2014-08-27 14:45:51'),
(20, 2, 4, '2014-08-28 08:58:08', '2014-08-28 08:58:08'),
(21, 11, 2, '2014-08-28 10:03:07', '2014-08-28 10:03:07'),
(22, 12, 2, '2014-08-28 10:03:58', '2014-08-28 10:03:58'),
(23, 14, 1, '2014-08-28 10:04:35', '2014-08-28 10:04:35'),
(24, 4, 1, '2014-08-28 10:41:53', '2014-08-28 10:41:53'),
(25, 5, 1, '2014-08-28 10:42:04', '2014-08-28 10:42:04'),
(26, 6, 1, '2014-08-28 10:42:17', '2014-08-28 10:42:17'),
(27, 7, 1, '2014-08-28 10:42:33', '2014-08-28 10:42:33'),
(28, 8, 1, '2014-08-28 10:42:45', '2014-08-28 10:42:45'),
(29, 9, 1, '2014-08-28 10:42:55', '2014-08-28 10:42:55'),
(30, 1, 2, '2014-08-28 10:46:31', '2014-08-28 10:46:31'),
(31, 15, 2, '2014-08-28 10:50:03', '2014-08-28 10:50:03'),
(32, 18, 1, '2014-09-01 11:49:48', '2014-09-01 11:49:48'),
(33, 20, 1, '2014-09-03 09:04:52', '2014-09-03 09:04:52'),
(34, 21, 1, '2014-09-03 09:05:25', '2014-09-03 09:05:25'),
(35, 22, 1, '2014-09-03 09:05:55', '2014-09-03 09:05:55'),
(36, 23, 1, '2014-09-03 09:06:17', '2014-09-03 09:06:17'),
(37, 24, 1, '2014-09-03 09:06:56', '2014-09-03 09:06:56'),
(38, 25, 1, '2014-09-03 09:07:18', '2014-09-03 09:07:18'),
(39, 26, 1, '2014-09-03 09:07:45', '2014-09-03 09:07:45'),
(40, 27, 1, '2014-09-03 09:08:05', '2014-09-03 09:08:05'),
(41, 28, 1, '2014-09-03 09:08:34', '2014-09-03 09:08:34'),
(42, 29, 1, '2014-09-03 09:08:55', '2014-09-03 09:08:55'),
(43, 30, 2, '2014-09-03 09:15:04', '2014-09-03 09:15:04'),
(44, 19, 1, '2014-09-08 09:33:07', '2014-09-08 09:33:07'),
(45, 19, 2, '2014-09-08 10:16:28', '2014-09-08 10:16:28'),
(46, 19, 3, '2014-09-08 10:21:16', '2014-09-08 10:21:16'),
(47, 36, 1, '2014-09-09 16:04:06', '2014-09-09 16:04:06'),
(48, 37, 1, '2014-09-09 16:04:29', '2014-09-09 16:04:29'),
(49, 44, 1, '2014-09-10 08:26:51', '2014-09-10 08:26:51'),
(50, 44, 2, '2014-09-10 08:26:51', '2014-09-10 08:26:51'),
(51, 46, 2, '2014-09-12 10:38:47', '2014-09-12 10:38:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pla_proyectos_idi`
--

CREATE TABLE IF NOT EXISTS `pla_proyectos_idi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_es` text NOT NULL,
  `descripcion_es` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `nombre_en` text NOT NULL,
  `nombre_fr` text NOT NULL,
  `descripcion_en` text NOT NULL,
  `descripcion_fr` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `pla_proyectos_idi`
--

INSERT INTO `pla_proyectos_idi` (`id`, `nombre_es`, `descripcion_es`, `created_at`, `updated_at`, `nombre_en`, `nombre_fr`, `descripcion_en`, `descripcion_fr`) VALUES
(1, 'CONTRATO PARA PROYECTO DE INVESTIGACIÓN Y DESARROLLO CON EL GRUPO DE INVESTIGACIÓN AiA (APPLYUNG INTELLIGENT AGENTS) DE LA UNIVERSIDAD JAUME I', 'Desarrollo de un modelo inteligente de simulación de tráfico microscópico para el entorno e la población de Oropesa', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', ''),
(2, 'CONVENIO DE COLABORACIÓN ENTRE PLANIFICA Y EDINN-M2 EN EL MARCO DEL PROGRAMA EUROPEO DE LUCHA CONTRA EL CAMBIO CLIMÁTICO PARA LA COLABORACIÓN ENTRE EMPRESAS Y UNIVERSIDADES: CLIMATE KIC - PROGRAMA PIP', 'Aplicación de la herramienta de software edinn M2 a proyecto urbanístico para la eficiencia energética en la urbanización del SUR 02 de Rossell (Castellón)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pla_proyectos_tipos`
--

CREATE TABLE IF NOT EXISTS `pla_proyectos_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pla_proyectos` int(11) NOT NULL,
  `id_pla_tipos` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Volcado de datos para la tabla `pla_proyectos_tipos`
--

INSERT INTO `pla_proyectos_tipos` (`id`, `id_pla_proyectos`, `id_pla_tipos`, `created_at`, `updated_at`) VALUES
(2, 1, 1, '2014-08-27 09:47:45', '2014-08-27 09:47:45'),
(3, 2, 1, '2014-08-27 09:50:32', '2014-08-27 09:50:32'),
(4, 1, 2, '2014-08-27 11:04:30', '2014-08-27 11:04:30'),
(6, 2, 6, '2014-08-27 11:05:17', '2014-08-27 11:05:17'),
(7, 13, 1, '2014-08-27 14:45:51', '2014-08-27 14:45:51'),
(8, 2, 7, '2014-08-28 08:58:00', '2014-08-28 08:58:00'),
(9, 11, 4, '2014-08-28 10:03:07', '2014-08-28 10:03:07'),
(10, 12, 4, '2014-08-28 10:03:59', '2014-08-28 10:03:59'),
(11, 14, 1, '2014-08-28 10:04:35', '2014-08-28 10:04:35'),
(12, 3, 1, '2014-08-28 10:41:38', '2014-08-28 10:41:38'),
(13, 4, 1, '2014-08-28 10:41:53', '2014-08-28 10:41:53'),
(14, 5, 1, '2014-08-28 10:42:04', '2014-08-28 10:42:04'),
(15, 6, 1, '2014-08-28 10:42:17', '2014-08-28 10:42:17'),
(16, 7, 1, '2014-08-28 10:42:33', '2014-08-28 10:42:33'),
(17, 8, 1, '2014-08-28 10:42:45', '2014-08-28 10:42:45'),
(18, 9, 1, '2014-08-28 10:42:55', '2014-08-28 10:42:55'),
(19, 1, 5, '2014-08-28 10:46:31', '2014-08-28 10:46:31'),
(20, 15, 5, '2014-08-28 10:50:03', '2014-08-28 10:50:03'),
(22, 18, 2, '2014-09-03 08:57:30', '2014-09-03 08:57:30'),
(23, 20, 2, '2014-09-03 09:04:52', '2014-09-03 09:04:52'),
(24, 21, 2, '2014-09-03 09:05:25', '2014-09-03 09:05:25'),
(25, 22, 2, '2014-09-03 09:05:55', '2014-09-03 09:05:55'),
(26, 23, 2, '2014-09-03 09:06:17', '2014-09-03 09:06:17'),
(27, 24, 3, '2014-09-03 09:06:56', '2014-09-03 09:06:56'),
(28, 25, 3, '2014-09-03 09:07:18', '2014-09-03 09:07:18'),
(29, 26, 3, '2014-09-03 09:07:45', '2014-09-03 09:07:45'),
(30, 27, 3, '2014-09-03 09:08:05', '2014-09-03 09:08:05'),
(31, 28, 3, '2014-09-03 09:08:34', '2014-09-03 09:08:34'),
(32, 29, 3, '2014-09-03 09:08:56', '2014-09-03 09:08:56'),
(33, 30, 5, '2014-09-03 09:15:04', '2014-09-03 09:15:04'),
(34, 19, 2, '2014-09-08 09:33:07', '2014-09-08 09:33:07'),
(35, 19, 1, '2014-09-08 09:33:07', '2014-09-08 09:33:07'),
(36, 19, 3, '2014-09-08 10:18:01', '2014-09-08 10:18:01'),
(37, 19, 5, '2014-09-08 10:21:16', '2014-09-08 10:21:16'),
(38, 36, 2, '2014-09-09 16:04:06', '2014-09-09 16:04:06'),
(39, 36, 1, '2014-09-09 16:04:06', '2014-09-09 16:04:06'),
(40, 37, 1, '2014-09-09 16:04:29', '2014-09-09 16:04:29'),
(41, 44, 1, '2014-09-10 08:26:51', '2014-09-10 08:26:51'),
(42, 46, 5, '2014-09-12 10:38:47', '2014-09-12 10:38:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pla_tipos`
--

CREATE TABLE IF NOT EXISTS `pla_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_es` text NOT NULL,
  `nombre_en` text NOT NULL,
  `nombre_fr` text NOT NULL,
  `descripcion_es` text NOT NULL,
  `descripcion_en` text NOT NULL,
  `descripcion_fr` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `id_pla_areas` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `pla_tipos`
--

INSERT INTO `pla_tipos` (`id`, `nombre_es`, `nombre_en`, `nombre_fr`, `descripcion_es`, `descripcion_en`, `descripcion_fr`, `created_at`, `updated_at`, `id_pla_areas`) VALUES
(1, 'Planes Generales', '', '', '', '', '', '0000-00-00 00:00:00', '2014-09-01 10:09:51', 1),
(2, 'Planes Parciales', '0', '0', '', '0', '0', '0000-00-00 00:00:00', '2014-08-25 11:39:16', 1),
(3, 'Otros instrumentos de planeamiento', '', '0', '', '', '', '0000-00-00 00:00:00', '2014-09-03 12:00:52', 1),
(5, 'Proyectos de urbanización', '0', '0', '', '0', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2),
(6, 'Otros proyectos de infraestrucutras', '0', '0', '', '0', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2),
(7, 'Direcciones de obra y css', '', '', '', '', '', '0000-00-00 00:00:00', '2014-09-03 10:22:45', 2),
(8, 'Estudios de impacto y sostenibilidad ambiental', '', '', '', '', '', '0000-00-00 00:00:00', '2014-09-03 12:00:40', 3),
(9, 'Estudios de movilidad urbana sostenible', '0', '0', '', '0', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3),
(10, 'Estudios acústicos', '0', '0', '', '0', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3),
(11, 'Estudios de recursos hídricos e hidrológicos-hidráulicos', '0', '0', '', '0', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3),
(12, 'Estudios de paisaje e integración paisajística', '0', '0', '', '0', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `throttle`
--

CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `ip_address`, `attempts`, `suspended`, `banned`, `last_attempt_at`, `suspended_at`, `banned_at`) VALUES
(1, 1, '127.0.0.1', 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_activation_code_index` (`activation_code`),
  KEY `users_reset_password_code_index` (`reset_password_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `permissions`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$HeTSd5qiCpaHkLhK6efmh.VFFzC4Fz6SRstzPFQMKNkO9yvlGqzO.', NULL, 1, NULL, NULL, '2014-12-02 11:51:23', '$2y$10$92zKDpk4ob4q3Uc0jmGW2u86WpzdWBmhnkHrQ6zNcjUc9xtalX7bq', NULL, NULL, NULL, '2014-08-19 09:07:35', '2014-12-02 11:51:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
(1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
