<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::when('admin*', 'sentry');
Route::when('api*', 'sentry');

Route::get('/login', function () {
    return View::make('users.login');
});

Route::group(array(
//    'prefix' => LaravelLocalization::setLocale(),
    'prefix' => 'old',
//    'before' => 'LaravelLocalizationRedirectFilter'
    ), function () {
    
    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
    Route::get('/', function () {
        return View::make('inicio')
                ->with('proyectos', Proyecto::where('visible', 1)->orderBy('fecha_fin', 'desc')->take(8)->get())
                ->with('areas', Area::all());
    });
    Route::get('/nosotros', function () {
        return View::make('quienes-somos');
    });
    Route::get('/servicios', function () {
        return View::make('servicios')
                ->with('areas', Area::all())
                ->with('tipos', Tipo::all())
                ->with('proyectos', Proyecto::all());
    });
    Route::get('servicios/proyecto/{id}', function ($id) {
        $proyecto = Proyecto::find($id);
        $anterior = Proyecto::where('visible', 1)->where('id', '<', $proyecto->id)->max('id');
        $siguiente = Proyecto::where('visible', '==', true)->where('id', '>', $proyecto->id)->min('id');
        return View::make('proyecto')
                ->with('proyecto', $proyecto)
                ->with('idSiguiente', $siguiente)
                ->with('idAnterior', $anterior);
    });
    Route::get('/internacionalizacion', function () {
        return View::make('internacionalizacion');
    });
    Route::get('/calidad', function () {
        return View::make('calidad');
    });
    Route::get('/idi', function () {
        return View::make('idi')
                ->with('articulos', Articulo::with('adjunto')->get())
                ->with('proyectosIdi', ProyectoIdi::all());
    });
    // Route::get('/contacto', function () {
    //     return View::make('contacto');
    // });
    Route::controller('contacto', 'ContactoController');
});

/** OTHER PAGES THAT SHOULD NOT BE LOCALIZED **/

Route::get('admin/', function () {
    return View::make('admin.admin')
            ->with('userEmail', Sentry::getUser()->email);
});

Route::group(array(
//    'prefix' => 'web',
), function () {

    Route::get('/', function () {
        return View::make('frontend.home')
            ->with('proyectos', Proyecto::where('visible', 1)->orderBy('fecha_fin', 'desc')->take(8)->get())
            ->with('areas', Area::all());
    });


     Route::get('comunicacion', function () {
        return View::make('frontend.comunicacion')
            ->with('articulos', Articulo::with('adjunto')->get())
            ->with('proyectosIdi', ProyectoIdi::all())
            ->with('noticias', Noticia::orderBy('fecha', 'desc')->get());
            
    }); 

     Route::get('documentacion', function () {
        return View::make('frontend.documentacion')
            ->with('articulos', Articulo::with('adjunto')->get())
            ->with('proyectosIdi', ProyectoIdi::all())
            ->with('pgou_docs', PgouDoc::orderBy('fecha', 'asc')->get())
            ->with('participacion_docs', ParticipacionDoc::orderBy('fecha', 'asc')->get())
            ->with('regeneracion_docs', RegeneracionDoc::orderBy('fecha', 'asc')->get());
            
    }); 

    Route::get('preliminar', function () {
        return View::make('frontend.preliminar');
    });

    Route::get('cofinanciacion', function () {
        return View::make('frontend.cofinanciacion');
    });    

    Route::get('contacto', function () {
        return View::make('frontend.contacto');
    });


    Route::post('contacto', function () {
        try {
            $datos = Input::all();
            Mail::send('contacto.email', $datos, function ($m) use ($datos) {
                $m->from('alcaladexivert@planifica.org', 'Web de planifica');
                $m->to('alcaladexivert@planifica.org')->subject('Web de planifica');
            });
            return Response::json(['success' => true]);
        } catch (Exception $e) {
            return $e;
            return Response::json(['success' => false]);
        }
    });

    Route::get('politica-cookies', function () {
        return View::make('frontend.politica_cookies');
    });

    Route::get('politica-privacidad', function () {
        return View::make('frontend.politica_privacidad');
    });

    Route::get('aviso-legal', function () {
        return View::make('frontend.aviso_legal');
    });
});

Route::controller('user', 'UserController');

Route::resource('api/v1/areas', 'AreaController');
Route::resource('api/v1/tipos', 'TipoController');
Route::resource('api/v1/proyectos', 'ProyectoController');
Route::resource('api/v1/proyectos.areas', 'ProyectoAreasController');
Route::resource('api/v1/proyectos.tipos', 'ProyectoTiposController');
Route::resource('api/v1/articulos', 'ArticuloController');
Route::resource('api/v1/proyectos_idi', 'ProyectoIdiController');
Route::resource('api/v1/proyectos.imagenes', 'ImagenController');
Route::resource('api/v1/adjuntos', 'AdjuntoController');
