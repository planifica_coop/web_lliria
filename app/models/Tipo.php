<?php
class Tipo extends Eloquent
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pla_tipos';

    protected $fillable = array('nombre_es', 'nombre_en', 'nombre_fr', 'descripcion_es', 'descripcion_en', 'descripcion_fr', 'id_pla_areas');
    
    public function area() {
        return $this->belongsTo('Area', 'id_pla_areas');
    }
    
    public function getNombre() {
        if (App::getLocale() == 'es') {
            return $this->nombre_es;
        } elseif (App::getLocale() == 'en') {
            return $this->nombre_en;
        } elseif (App::getLocale() == 'fr') {
            return $this->nombre_fr;
        }
    }
    
    public function getDescripcion() {
        if (App::getLocale() == 'es') {
            return $this->descripcion_es;
        } elseif (App::getLocale() == 'en') {
            return $this->descripcion_en;
        } elseif (App::getLocale() == 'fr') {
            return $this->descripcion_fr;
        }
    }
}
