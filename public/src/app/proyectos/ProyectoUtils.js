proyectosUtils.factory('ProyectoUtils', function($q, Proyecto, $upload) {
    return {
        /**
         * Actualiza vía RESTful las areas de un Proyecto.
         * @param proyecto Instancia del proyecto que debe ser actualizado.
         * @param areasAnyadir Array con las areas que se deben añadir al proyecto.
         * @param areasEliminar Array con las areas que se deben eliminar del proyecto.
         */
        updateAreas: function(proyecto, areasAnyadir, areasEliminar) {
            for (area in areasAnyadir) {
                Proyecto.addArea({
                    id: proyecto.id,
                    id_pla_areas: areasAnyadir[area].id
                }, function() {
                    console.log('Areas añadidas.')
                });
            }
            for (area in areasEliminar) {
                Proyecto.deleteArea({
                    id: proyecto.id,
                    idArea: areasEliminar[area].id
                }, function() {
                    console.log('Areas eliminadas.')
                });
            }
        },
        /**
         * Actualiza vía RESTful los tipos de un Proyecto.
         * @param proyecto Instancia del proyecto que debe ser actualizado.
         * @param tiposAnyadir Array con los tipos que se deben añadir al proyecto.
         * @param tiposEliminar Array con los tipos que se deben eliminar del proyecto.
         */
        updateTipos: function(proyecto, tiposAnyadir, tiposEliminar) {
            for (tipo in tiposAnyadir) {
                Proyecto.addTipo({
                    id: proyecto.id,
                    id_pla_tipos: tiposAnyadir[tipo].id
                }, function() {
                    console.log('Tipos añadidos.')
                });
            }
            for (tipo in tiposEliminar) {
                Proyecto.deleteTipo({
                    id: proyecto.id,
                    idTipo: tiposEliminar[tipo].id
                }, function() {
                    console.log('Tipos eliminados.')
                });
            }
        },
        updateImagenes: function(proyecto, imagenesAnyadir, imagenesEliminar) {
            for (var i = 0; i < imagenesAnyadir.length; i++) {
                var file = imagenesAnyadir[i];
                $upload.upload({
                    url: CONFIG.URL.API + '/proyectos/' + proyecto.id + '/imagenes', //upload.php script, node.js route, or servlet url
                    //method: 'POST' or 'PUT',
                    //headers: {'header-key': 'header-value'},
                    //withCredentials: true,
                    // data: {
                    //     myObj: $scope.myModelObj
                    // },
                    file: file, // or list of files ($files) for html5 only
                    //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
                    // customize file formData name ('Content-Disposition'), server side file variable name. 
                    //fileFormDataName: myFile, //or a list of names for multiple files (html5). Default is 'file' 
                    // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
                    //formDataAppender: function(formData, key, val){}
                }).progress(function(evt) {
                    console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                }).success(function(data, status, headers, config) {
                    // file is uploaded successfully
                    console.log(data);
                    return true;
                }).error(function() {
                    return false;
                });
            }
            for (imagen in imagenesEliminar) {
                Proyecto.deleteImagen({
                    id: proyecto.id,
                    idImagen: imagenesEliminar[imagen].id
                }, function() {
                    console.log('Imagenes eliminadas.')
                });
            }
        },
        /**
         * Determina si un objeto esta marcado y si se debe añadir o eliminar de la base de datos.
         * @param data Objeto del que se desea comprobar.
         * @param anyadir Array con los objetos marcados para añadir al proyecto.
         * @param eliminar Array con los objetos marcados para eliminar del proyecto.
         */
        estaMarcado: function(data, anyadir, eliminar) {
            if (data.ticked) {
                var result = $.grep(eliminar, function(e, index) {
                    var res = e.id == data.id;
                    if (res) {
                        eliminar.splice(index, 1);
                    }
                    return res;
                });
                if (result.length == 0) {
                    anyadir.push(data);
                }
            } else {
                var result = $.grep(anyadir, function(e, index) {
                    res = e.id == data.id;
                    if (res) {
                        anyadir.splice(index, 1);
                    };
                    return res;
                });
                if (result.length == 0) {
                    eliminar.push(data);
                }
            }
        }
    };
});