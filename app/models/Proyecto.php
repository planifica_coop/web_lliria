<?php

class Proyecto extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pla_proyectos';

    protected $fillable = array('numero_expediente', 'nombre_es', 'nombre_en', 'nombre_fr', 'municipio', 'region', 'importe_adjudicacion', 'fecha_inicio', 'fecha_fin', 'cliente_nombre', 'cliente_tipo', 'descripcion_es', 'descripcion_en', 'descripcion_fr', 'tiene_ficha', 'visible', 'lat', 'lng', 'id_pla_imagenes', 'importe_ejecucion_pem');

    public function imagen()
    {
        return $this->belongsTo('Imagen', 'id_pla_imagenes');
    }

    public function areas()
    {
        return $this->belongsToMany('Area', 'pla_proyectos_areas', 'id_pla_proyectos', 'id_pla_areas');
    }

    public function tipos()
    {
        return $this->belongsToMany('Tipo', 'pla_proyectos_tipos', 'id_pla_proyectos', 'id_pla_tipos');
    }

    public function imagenes()
    {
        return $this->hasMany('Imagen', 'id_pla_proyectos');
    }

    public function getTieneFichaAttribute($value)
    {
        return $value == true;
        // return boolval($value);
    }

    public function getVisibleAttribute($value)
    {
        return $value == true;
        // return boolval($value);
    }

    public function getImporteAdjudicacionAttribute($value)
    {
        return doubleval($value);
    }

    public function getImporteEjecucionPemAttribute($value)
    {
        return doubleval($value);
    }

    public function getLatAttribute($value)
    {
        return doubleval($value);
    }

    public function getLngAttribute($value)
    {
        return doubleval($value);
    }

    public function getNombre()
    {
        if (App::getLocale() == 'es') {
            return $this->nombre_es;
        } elseif (App::getLocale() == 'en') {
            return $this->nombre_en;
        } elseif (App::getLocale() == 'fr') {
            return $this->nombre_fr;
        }
    }

    public function getDescripcion()
    {
        if (App::getLocale() == 'es') {
            return $this->descripcion_es;
        } elseif (App::getLocale() == 'en') {
            return $this->descripcion_en;
        } elseif (App::getLocale() == 'fr') {
            return $this->descripcion_fr;
        }
    }
}