var resumen = angular.module('resumen', ['nvd3ChartDirectives']);

app.config(function($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'src/app/resumen/resumen.html',
        controller: 'ResumenController',
        resolve: {
            areas: function(Area) {
                return Area.query({
                    include: 'proyectos'
                }).$promise.then(function(data) {
                    return data;
                });
            },
            tipos: function(Tipo) {
                return Tipo.query().$promise.then(function(data) {
                    return data;
                });
            },
            proyectos: function(Proyecto) {
                return Proyecto.query({
                    sort: 'fecha_fin'
                }).$promise.then(function(data) {
                    return data;
                });
            },
            articulos: function(Articulo) {
                return Articulo.query().$promise.then(function(data) {
                    return data;
                });
            },
            proyectosIdi: function(ProyectoIdi) {
                return ProyectoIdi.query().$promise.then(function(data) {
                    return data;
                });
            }
        }
    });
});

resumen.controller('ResumenController', function($scope, $location, areas, proyectos, tipos, articulos, proyectosIdi) {
    

    $scope.xFunction = function() {
        return function(d) {
            return d.nombre_es;
        };
    }
    $scope.yFunction = function() {
        return function(d) {
            return d.proyectos.length;
        };
    }

    $scope.descriptionFunction = function() {
        return function(d) {
            return d.key;
        }
    }
    $scope.areas = areas;
    $scope.proyectos = proyectos;
    $scope.tipos = tipos;
    $scope.articulos = articulos;
    $scope.proyectosIdi = proyectosIdi;
});