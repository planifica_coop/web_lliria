<?php
class ProyectoIdi extends Eloquent
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pla_proyectos_idi';
    
    public function getNombre() {
        if (App::getLocale() == 'es') {
            return $this->nombre_es;
        } elseif (App::getLocale() == 'en') {
            return $this->nombre_en;
        } elseif (App::getLocale() == 'fr') {
            return $this->nombre_fr;
        }
    }
    
    public function getDescripcion() {
        if (App::getLocale() == 'es') {
            return $this->descripcion_es;
        } elseif (App::getLocale() == 'en') {
            return $this->descripcion_en;
        } elseif (App::getLocale() == 'fr') {
            return $this->descripcion_fr;
        }
    }
}
