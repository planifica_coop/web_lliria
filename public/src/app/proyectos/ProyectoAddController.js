proyectosControllers.controller('ProyectoAddController', function($scope, $rootScope, $location, $routeParams, Proyecto, areas, sigoModal, ProyectoUtils, $timeout) {
    $scope.areas = areas;
    $scope.areasSelecTipos = [];
    $scope.areasAnyadir = [];
    $scope.tiposAnyadir = [];
    $scope.imagenesAnyadir = [];
    $scope.imagenes = [];
    $scope.imagenesUrl = [];
    $timeout(function() {
        $scope.alertasImagenes.push({
            tipo: 'warning',
            mensaje: 'Las imagenes deben tener un ratio alrededor de 701 x 500.'
        });
    });

    angular.extend($scope, {
        castellon: {
            lat: 39.9874595,
            lng: -0.048063,
            zoom: 14
        },
        markers: {}
    });

    $scope.datepickerOptions = {
        format: 'YYYY-mm-dd',
        language: 'es',
        autoclose: true,
        weekStart: 0
    }

    $scope.anyadirImagen = function($files) {
        console.log('Añadiendo imagen ' + $files[0] + "...");
        FileAPI.getInfo($files[0], function(err, info) {
            if (!err) {
                console.log(info.width, info.height);
                var ratio = info.width / info.height;
                if (info.width > info.height && ratio > 1.3 && ratio < 1.5) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($files[0]);
                    var loadFile = function(fileReader, index) {
                        fileReader.onload = function(e) {
                            $timeout(function() {
                                $scope.imagenesUrl.push(e.target.result);
                            });
                        }
                    }(fileReader);
                    $scope.imagenesAnyadir.push($files[0]);
                } else {
                    $timeout(function() {
                        $scope.alertasImagenes.push({
                            tipo: 'danger',
                            mensaje: 'Las imagenes deben tener un ratio alrededor de 701 x 500.'
                        });
                    });
                }
            } else {
                $timeout(function() {
                    $scope.alertasImagenes.push({
                        tipo: 'danger',
                        mensaje: 'Las imagenes deben tener un ratio alrededor de 701 x 500.'
                    });
                });
            }
        });
        console.log('Imagenes Anyadir', $scope.imagenesAnyadir);
        console.log('Imagenes Eliminar', $scope.imagenesEliminar);
    };

    $scope.eliminarImagen = function(pos, imagen) {
        console.log('Eliminar', $scope.imagenesAnyadir.indexOf(imagen));
        var posAnyadir = $scope.imagenesAnyadir.indexOf(imagen);
        if (posAnyadir > -1) {
            $scope.imagenesAnyadir.splice(posAnyadir, 1);
        } else {
            $scope.imagenesEliminar.push(imagen);
            $scope.imagenes.splice(pos, 1);
        };
        console.log('Imagenes Anyadir', $scope.imagenesAnyadir);
        console.log('Imagenes Eliminar', $scope.imagenesEliminar);
    };

    $scope.crearProyecto = function(proyecto, areasAnyadir, tiposAnyadir, imagenesAnyadir) {
        console.log('Creado proyecto ' + proyecto.nombre_es + '...');
        Proyecto.save({}, proyecto, function(data) {
            ProyectoUtils.updateAreas(data, areasAnyadir)
            ProyectoUtils.updateTipos(data, tiposAnyadir);
            if (imagenesAnyadir && !ProyectoUtils.updateImagenes(data, imagenesAnyadir)) {
                $scope.alertasImagenes.push({
                    tipo: 'danger',
                    mensaje: 'Las imagenes deben tener un ratio alrededor de 701 x 500.'
                });
            }
            $rootScope.setForm();
            $location.path(CONFIG.URL.PROYECTOS);
        }, function() {
            sigoModal.alerta('No se ha podido crear el proyecto.', 'Aceptar');
        });
    };

    $scope.cancelar = function() {
        $location.path(CONFIG.URL.PROYECTOS);
    };

    $scope.actualizarArea = function(data, areasSelec, tiposSelec) {
        ProyectoUtils.estaMarcado(data, $scope.areasAnyadir, []);
        // Falta borrar los tipos de esta area
        $scope.actualizarTipos(areasSelec);
    };

    $scope.actualizarTipos = function(areasSelec) {
        $scope.areasSelecTipos = [];
        for (area in areasSelec) {
            $scope.areasSelecTipos.push({
                nombre: '<strong>' + areasSelec[area].nombre_es + '</strong>',
                multiSelectGroup: true
            });
            for (tipo in areasSelec[area].tipos) {
                $scope.areasSelecTipos.push(areasSelec[area].tipos[tipo]);
            }
            $scope.areasSelecTipos.push({
                multiSelectGroup: false
            });
        }
        console.log('Areas Anyadir:', $scope.areasAnyadir);
        console.log('Areas Eliminar:', $scope.areasEliminar);
    };

    $scope.actualizarTipo = function(data) {
        ProyectoUtils.estaMarcado(data, $scope.tiposAnyadir, []);
        console.log('Tipos Anyadir:', $scope.tiposAnyadir);
        console.log('Tipos Eliminar:', $scope.tiposEliminar);
    };

    $scope.alertasImagenes = [];
    $scope.textoBoton = 'AÑADIR';

    $scope.$on("leafletDirectiveMap.click", function(event, args) {
        var leafEvent = args.leafletEvent;
        if ($scope.markers.mainMarker) {
            $scope.markers.mainMarker.lat = leafEvent.latlng.lat;
            $scope.markers.mainMarker.lng = leafEvent.latlng.lng;
        } else {
            $scope.markers.mainMarker = {
                lat: leafEvent.latlng.lat,
                lng: leafEvent.latlng.lng,
                focus: true,
                draggable: true
            }
        }
    });

    $scope.tipos_cliente = TIPOS_CLIENTE;

    /* Validacion */
    $scope.submitted = false;
});