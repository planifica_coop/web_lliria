@extends('base')
@section('css')
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
@stop
@section('content')
@include('barra-navegacion')
<!-- Region 4 Wrap -->
<div class="container region4wrap">
  <div class="row maincontent">
    <div class="twelve columns">
      <div class="page_title">
        <div class="row">
          <div class="twelve columns">
            <h1>{{ $proyecto->getNombre() }}</h1>
          </div>
        </div>
      </div>
    </div>
    <div class="twelve columns">
      <div class="row">
        <div class="twelve columns">
          <h4>Áreas de actividad</h4>
          <div align="center">
            @foreach ($proyecto->areas as $key => $area)
              {{ $area->getNombre() }}
              @if ($key < (count($proyecto->areas)-1))
               |
              @endif
            @endforeach
          </div>
          <br>
        </div>
      </div>
      <div class="row">
        <div class="six columns">
          <h4>Localización</h4>
          <ul class="disc">
            <li>Municipio {{ $proyecto->municipio }}</li>
            <li>Región {{ $proyecto->region }}</li>
          </ul>
          <h4>Importe Adjudicación</h4>
          <p>{{ $proyecto->importe_adjudicacion }} €</p>
          <h4>Inicio de los trabajos</h4>
          <p>{{ $proyecto->fecha_inicio }}</p>
          <h4>Finalización de los trabajos</h4>
          <p>{{ $proyecto->fecha_fin }}</p>
          <h4>Cliente</h4>
          <ul class="disc">
            <li>Empresa o institución {{ $proyecto->cliente_nombre }}</li>
            <li>Tipo {{ $proyecto->cliente_tipo }}</li>
          </ul>
        </div>
        <div class="six columns">
          <h4>Localización de la actuación</h4>
          <div id="map" style="width: 100%; height: 300px"></div>
        </div>
      </div>
    </div>
    <div class="twelve columns">
      <hr>
      <div class="row">
        <div class="six columns">
          <section class="slider">
            <div class="gallery-slider flexslider">
              <ul class="slides">
                @foreach ($proyecto->imagenes as $key => $imagen)
                  <li data-thumb="{{URL::to('uploads/images/proyectos/'.$imagen->id.'.'.$imagen->extension) }}">
                    <img src="{{URL::to('uploads/images/proyectos/'.$imagen->id.'.'.$imagen->extension) }}" alt="" />
                  </li>
                @endforeach
              </ul>
            </div>
          </section>
        </div>
        <div class="six columns">
          <h4>Descripción</h4>
          <p>{{ $proyecto->getDescripcion() }}</p>
          <div class="project-pagination">
            @if ($idAnterior != null)
            <a title="Previous Project" href="{{ URL::to('servicios/proyecto/' . $idAnterior) }}" class="has-tipsy left_pagination"></a>
            @endif
            <a title="View All Projects" href="{{ URL::to('servicios') }}" class="has-tipsy all_pagination"></a>
            @if ($idSiguiente != null)
            <a title="Next Project" href="{{ URL::to('servicios/proyecto/' . $idSiguiente) }}" class="has-tipsy right_pagination"></a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('javascript')
<!-- FlexSlider -->
<script src="{{URL::to('plugins/flexislider/jquery.flexslider.js') }}"></script>
<script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
<script>
    var map = L.map('map').setView([{{ $proyecto->lat }}, {{ $proyecto->lng }}], 13);
    var marker = L.marker([{{ $proyecto->lat }}, {{ $proyecto->lng }}]).addTo(map);
    L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
      maxZoom: 18,
      attribution: '',
      id: 'examples.map-i86knfo3'
    }).addTo(map);

    $('.gallery-slider').flexslider({
      animation: "slide",
      controlNav: "thumbnails",
      start: function(slider){
        $('body').removeClass('loading');
      }
    });

</script>
@stop