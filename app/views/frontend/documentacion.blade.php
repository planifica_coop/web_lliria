@extends('frontend.layout.base')

@section('contenido')

  <section id="page-title" class="page-title-video text-light"
           data-vide-bg="mp4:{{ url('frontend/video/encuesta') }}, poster:{{ url('frontend/video/encuesta_poster') }}"
           data-vide-options="position: 0% 70%"
           style="background: rgba(80,99,128,0.60);">
    <div class="container">
      <div class="page-title col-md-8">
        <h1>{{ trans('general.documentacion_seccion.titulo') }}</h1>
      </div>
      <div class="col-md-12">
        <h4 class="text-center" style="font-size: 2.5rem;font-weight: 100;">{{ trans('general.documentacion_seccion.subtitulo') }}</h4>
      </div>
    </div>
  </section>

  <section class="p-t-40">
    <div class="container">
      <div id="tabs-05c" class="tabs color">
        <p class="lead">{{ trans('general.documentacion_seccion.texto') }}</p>
        <ul class="tabs-navigation">
          
          
          <li class="active"><a href="#doc_pgou">{{ trans('general.documentacion_seccion.doc_pgou') }}</a></li>
          <li><a href="#doc_regeneracion">{{ trans('general.documentacion_seccion.doc_regeneracion') }}</a></li>
          <li><a href="#doc_participacion">{{ trans('general.documentacion_seccion.doc_participacion') }}</a></li>
          
        </ul>
        <div class="tabs-content">
          
          <div class="tab-pane" id="doc_participacion">
            <ol class="ol-type1">
              @foreach ($participacion_docs as $participacion_doc)
                <li>
                  <strong>{{ $participacion_doc->getTitulo() }}</strong><br>
                  <span>{{ $participacion_doc->getDescripcion() }}</span>
                  
                  @if ($participacion_doc->getLink_1() != null)
                  <ul>
                    <li>    
                      <a href="{{ $participacion_doc->getLink_1() }}" target="_blank">
                        {{ $participacion_doc->getText_link_1() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                  @if ($participacion_doc->getLink_2() != null)
                  <ul>
                    <li>    
                      <a href="{{ $participacion_doc->getLink_2() }}" target="_blank">
                        {{ $participacion_doc->getText_link_2() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                  @if ($participacion_doc->getLink_3() != null)
                  <ul>
                    <li>    
                      <a href="{{ $participacion_doc->getLink_3() }}" target="_blank">
                        {{ $participacion_doc->getText_link_3() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                  @if ($participacion_doc->getLink_4() != null)
                  <ul>
                    <li>    
                      <a href="{{ $participacion_doc->getLink_4() }}" target="_blank">
                        {{ $participacion_doc->getText_link_4() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                  @if ($participacion_doc->getLink_5() != null)
                  <ul>
                    <li>    
                      <a href="{{ $participacion_doc->getLink_5() }}" target="_blank">
                        {{ $participacion_doc->getText_link_5() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                </li>
                <hr>
              @endforeach
            <ol>
          </div>


          <div class="tab-pane active" id="doc_pgou">
            <ol class="ol-type1">
              @foreach ($pgou_docs as $pgou_doc)
                <li>
                  <strong>{{ $pgou_doc->getTitulo() }}</strong><br>
                  <span>{{ $pgou_doc->getDescripcion() }}</span>
      
                  @if ($pgou_doc->getLink_1() != null)
                  <ul>
                    <li>    
                      <a href="{{ $pgou_doc->getLink_1() }}" target="_blank">
                        {{ $pgou_doc->getText_link_1() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                  @if ($pgou_doc->getLink_2() != null)
                  <ul>
                    <li>    
                      <a href="{{ $pgou_doc->getLink_2() }}" target="_blank">
                        {{ $pgou_doc->getText_link_2() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                  @if ($pgou_doc->getLink_3() != null)
                  <ul>
                    <li>    
                      <a href="{{ $pgou_doc->getLink_3() }}" target="_blank">
                        {{ $pgou_doc->getText_link_3() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                  @if ($pgou_doc->getLink_4() != null)
                  <ul>
                    <li>    
                      <a href="{{ $pgou_doc->getLink_4() }}" target="_blank">
                        {{ $pgou_doc->getText_link_4() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                  @if ($pgou_doc->getLink_5() != null)
                  <ul>
                    <li>    
                      <a href="{{ $pgou_doc->getLink_5() }}" target="_blank">
                        {{ $pgou_doc->getText_link_5() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                  @if ($pgou_doc->getLink_6() != null)
                  <ul>
                    <li>    
                      <a href="{{ $pgou_doc->getLink_6() }}" target="_blank">
                        {{ $pgou_doc->getText_link_6() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                </li>
                <hr>
              @endforeach
            </ol>
          </div>


          <div class="tab-pane" id="doc_regeneracion">
            <ol class="ol-type1">
              @foreach ($regeneracion_docs as $regeneracion_doc)
                <li>
                  <strong>{{ $regeneracion_doc->getTitulo() }}</strong><br>
                  <span>{{ $regeneracion_doc->getDescripcion() }}</span>
                  
                  @if ($regeneracion_doc->getLink_1() != null)
                  <ul>
                    <li>    
                      <a href="{{ $regeneracion_doc->getLink_1() }}" target="_blank">
                        {{ $regeneracion_doc->getText_link_1() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                  @if ($regeneracion_doc->getLink_2() != null)
                  <ul>
                    <li>    
                      <a href="{{ $regeneracion_doc->getLink_2() }}" target="_blank">
                        {{ $regeneracion_doc->getText_link_2() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                  @if ($regeneracion_doc->getLink_3() != null)
                  <ul>
                    <li>    
                      <a href="{{ $regeneracion_doc->getLink_3() }}" target="_blank">
                        {{ $regeneracion_doc->getText_link_3() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                  @if ($regeneracion_doc->getLink_4() != null)
                  <ul>
                    <li>    
                      <a href="{{ $regeneracion_doc->getLink_4() }}" target="_blank">
                        {{ $regeneracion_doc->getText_link_4() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                  @if ($regeneracion_doc->getLink_5() != null)
                  <ul>
                    <li>    
                      <a href="{{ $regeneracion_doc->getLink_5() }}" target="_blank">
                        {{ $regeneracion_doc->getText_link_5() }}
                      </a><br>
                    </li>
                  </ul>
                  @endif
                </li>
                <hr>
              @endforeach
            <ol>
          </div>
          
        </div>
      </div>
    </div>
  </section>

@endsection