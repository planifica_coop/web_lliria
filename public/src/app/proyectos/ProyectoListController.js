proyectosControllers.controller('ProyectoListController', function($scope, $location, $filter, ngTableParams, Proyecto, sigoModal) {

    $scope.nuevoProyecto = function() {
        $location.path(CONFIG.URL.PROYECTOS + CONFIG.URL.ADD);
    };

    $scope.editarProyecto = function(id) {
        $location.path(CONFIG.URL.PROYECTOS + CONFIG.URL.EDIT + '/' + id);
    };

    $scope.eliminarProyecto = function(proyecto, posicion) {
        console.log(proyecto)
        sigoModal.confirmar('¿Desea borrar el proyecto ' + proyecto.nombre_es + '?', 'Sí', 'No')
            .then(function() {
                    Proyecto.delete({
                        id: proyecto.id
                    }, function() {
                        console.log('Proyecto ' + proyecto.nombre + ' eliminada.');
                        $scope.proyectos.splice($scope.proyectos.indexOf(proyecto), 1);
                        $scope.tableParams.reload();
                    }, function() {
                        sigoModal.alerta('No se ha podido eliminar el proyecto', 'Aceptar');
                    });
                },
                function() {
                    console.log('cancel');
                });

    };

    $scope.verProyecto = function(id) {
        $location.path(CONFIG.URL.PROYECTOS + CONFIG.URL.VIEW + '/' + id);
    };

    $scope.actualizarBusqueda = function() {
        $scope.tableParams.reload();
    };

    $scope.proyectos = Proyecto.query(function(data) {
        console.log(data);
        $scope.tableParams.reload();
    });

    $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10 // count per page
    }, {
        total: 0, // length of data
        getData: function($defer, params) {
            var filteredData = $filter('filter')($scope.proyectos, $scope.busqueda);
            var orderedData = params.sorting() ?
                $filter('orderBy')(filteredData, params.orderBy()) :
                filteredData;
            var total = orderedData.length;
            params.total(total);
            if (total <= params.count())
                params.page(1);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
});