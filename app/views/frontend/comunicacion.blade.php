@extends('frontend.layout.base')

@section('contenido')

  <section id="page-title" class="page-title-video text-light"
           data-vide-bg="mp4:{{ url('frontend/video/encuesta') }}, poster:{{ url('frontend/video/encuesta_poster') }}"
           data-vide-options="position: 0% 70%"
           style="background: rgba(80,99,128,0.60);">
    <div class="container">
      <div class="page-title col-md-8">
        <h1>{{ trans('general.comunicacion_seccion.titulo') }}</h1>
      </div>
      <div class="col-md-12">
        <h4 class="text-center" style="font-size: 2.5rem;font-weight: 100;">{{ trans('general.comunicacion_seccion.subtitulo') }}</h4>
      </div>
    </div>
  </section>

  <section class="container">
   @foreach ($noticias as $noticia)
      <div class="row justify-content-start">

             <!--Featured image-->
        
                  <div class="col-sm-6">
                      <div class="view overlay hm-white-light z-depth-1-half">
                          <img src="{{ $noticia->getImagen() }}" class="img-fluid" alt="" style="width:100%">
                          <div class="mask"></div>
                      </div>
                  </div>
      <!--/.Featured image-->
       
       <!--Second column-->
          <div class="col-sm-6">
              <!--Excerpt-->

            <h4>{{ $noticia->getTitulo() }}</h4>
             <p>{{ $noticia->getTexto() }}</p>
             <p><em>{{ $noticia->getFecha() }}</em></p>
             @if ($noticia->getLink() != null)
              <a href="{{ $noticia->getLink() }}" target="_blank" class="button color" role="button">{{ $noticia->getTexto_boton() }}</a>
             @endif
              
      </div>
          <!--/Second column-->
         
      </div>
      <br><br>
      <hr>
   @endforeach
  </section>

@endsection
