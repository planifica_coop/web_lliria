<?php
class AdjuntoController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return Adjunto::all();
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $destinationPath = $_SERVER['DOCUMENT_ROOT'] . Config::get('planifica.urlAdjuntosArticulos');
        
        $adjunto = Input::file('adjunto');
        $nombre = $adjunto->getClientOriginalName();
        $extension = $adjunto->getClientOriginalExtension();
        $upload_success = $adjunto->move($destinationPath, $nombre);
        
        if ($upload_success) {
            $adjunto = new Adjunto;
            $adjunto->ruta = "/uploads/docs/articulos/" . $nombre;
            $adjunto->nombre = $nombre;
            $adjunto->save();
            return $adjunto;
        } else {
            return Response::json(['error' => ['message' => 'No se ha podido subir el fichero.']], 400);
        }
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        return Articulo::find($id);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $hayError = false;
        $destinationPath = $_SERVER['DOCUMENT_ROOT'] . Config::get('planifica.urlAdjuntosImagenes');
        $adjunto = Adjunto::find($id);
        if (File::delete($destinationPath . $adjunto->ruta)) {
            if ($adjunto->delete()) {
                return Response::json('success', 200);
            } else {
                return Response::json(['error' => ['message' => 'No se ha podido eliminar el adjunto.']], 400);
            }
        } else {
            return Response::json(['error' => ['message' => 'No se ha podido eliminar el fichero correspondiente al adjunto.']], 400);
        }
    }
}
