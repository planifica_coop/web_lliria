var proyectosIdiServices = angular.module('proyectosIdi.services', ['ngResource']);

proyectosIdiServices.factory('ProyectoIdi', function($resource) {
    return $resource(CONFIG.URL.API + '/proyectos_idi/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        }
    });
});