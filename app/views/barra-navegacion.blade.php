<!-- Region 3 Wrap -->
<div class="container region3wrap">
  <!-- Content Top -->
  <div class="row content_top">
    <div class="nine columns">
      <ul class="breadcrumbs">
        <li><a href="">{{ trans('nombres.inicio') }}</a></li>
        {{ Request::is('*/nosotros') ? '<li class="current"><a href="nosotros">'.str_replace("<br>"," ",trans('nombres.quienes_somos')).'</a></li>' : '' }}
        {{ Request::is('*/servicios*') ? '<li class="current"><a href="servicios">'.str_replace("<br>"," ",trans('nombres.nuestros_servicios')).'</a></li>' : '' }}
        {{ Request::is('*/internacionalizacion') ? '<li class="current"><a href="internacionalizacion">'.trans('nombres.internacionalizacion').'</a></li>' : '' }}
        {{ Request::is('*/calidad') ? '<li class="current"><a href="calidad">'.str_replace("<br>"," ",trans('nombres.sistema_calidad')).'</a></li>' : '' }}
        {{ Request::is('*/idi') ? '<li class="current"><a href="idi">'.trans('nombres.i_d_i').'</a></li>' : '' }}
        {{ Request::is('*/contacto') ? '<li class="current"><a href="contacto">'.trans('nombres.contacto').'</a></li>' : '' }}
        {{ Request::is('*/proyecto/*') ? '<li class="current"><a href="proyecto">'.trans('nombres.proyecto').'</a></li>' : '' }}
        {{ Request::is('*/error') ? '<li class="current"><a href="contacto">'.trans('nombres.error').'</a></li>' : '' }}
      </ul>
    </div>
    <div class="three columns">
      <div class="row">
        <div class="twelve columns">
          <!-- <div class="row collapse top_search">
            <div class="ten mobile-three columns">
              <input type="text" placeholder="Buscar" />
            </div>
            <div class="two mobile-one columns">
              <a href="#" class="button icon-search"></a>
            </div>
          </div> -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Content Top -->
</div>
<!-- End Region 3 Wrap