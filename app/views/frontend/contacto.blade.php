@extends('frontend.layout.base')

@section('contenido')

<section id="page-title" class="page-title-video text-light p-t-100 p-b-40" data-vide-bg="mp4:{{ url('frontend/video/computer') }}, poster:{{ url('frontend/video/computer') }}" data-vide-options="position: 50% 50%"
         style="background: rgba(80,99,128,0.60);">
  <div class="container">
    <div class="page-title col-md-12 text-center">
      <h2 class="m-b-20">{{ trans('general.contacto_seccion.titulo') }}</h2>
    </div>
    <div class="col-md-12 text-center text-lg">
      <h1><a href="mailto:pmuslliria@planifica.org" class="button large transparent rounded contacto-mail text-lg"
             style="
                    /*font-size: 4vw;*/
                    text-transform: none;
                    line-height: 0rem;
                    padding: 3rem;text-transform: none;"
                >
          pmuslliria@planifica.org
        </a></h1>
    </div>
  </div>
</section>

<section class="p-t-40">

  <div class="container">
    <div class="row">
      <div class="col-md-4 col-xs-12 contacto-direcciones">
        <h3>Dónde estamos</h3><br>
        <h4>Llíria (Valencia)</h4>
        <h5>Departamento de Urbanismo del Ayuntamiento de Llíria</h5>

        <p>Pl. Mayor, 1<br>
          46160, Llíria (Valencia)<br>
          <div>
          Web: lliria.es</p>
          </div><br>
        <h4></h4>
        <h5>PLANIFICA</h5>

        <p>Calle Poeta Guimerá, 7 - 5A<br>
          12001, Castellón<br>

        <div>
          Telf: (+34) 964 06 12 56<br>
          Email: pmuslliria@planifica.org<br>
          Web: planifica.org</p>
        </div>
      </div>

      <div class="col-md-8 col-xs-12">
        <form id="contacto" action="" class="form-gray-fields" role="form" method="post">
          <div class="row">
            <div class="col-md-5">
              <div class="form-group">
                <label class="upper" for="name">{{ ucfirst(trans('general.nombre')) }}</label>
                <input type="text" class="form-control" name="name" placeholder="{{ ucfirst(trans('general.nombre_placeholder')) }}" id="name"
                       minlength="2" required
                        >
              </div>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <label class="upper" for="email">{{ ucfirst(trans('general.email')) }}</label>
                <input type="email" class="form-control email" name="email" placeholder="{{ ucfirst(trans('general.email_placeholder')) }}"
                       id="email"
                       aria-required="true" required
                        >
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label class="upper" for="comment">{{ ucfirst(trans('general.comentario')) }}</label>
                <textarea class="form-control" name="comment" rows="9" placeholder="{{ ucfirst(trans('general.comentario_placeholder')) }}"
                          id="comment"
                          aria-required="true" required
                        ></textarea>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group text-left">
                <button class="button small icon-left color" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;{{ ucfirst(trans('general.enviar')) }}
                </button>
                 <span id="span-terminos">
                   <input name="terminos" type="checkbox" value="1" id="terminos" class="required" style="margin-left: 10px;" required>
                  Acepto la <a target="_blank" href="{{ url('aviso-legal') }}"
                               style="">política de
                     protección de datos</a>
                   </span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 p-b-20">
              <div id="contacto-loading" style="display: none">
                <div class="text-center" style="background: #DEDEDE;padding: 15px;">
                  <img width="50px" src="{{ url('frontend/img/svg-loaders/three-dots.svg') }}">&nbsp;&nbsp;Enviando mensaje...
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div id="contacto-success" class="contacto-mensajes">
                Tu mensaje se ha enviado con éxito. En breve nos pondremos en contacto contigo.
              </div>
              <div id="contacto-fail" class="contacto-mensajes">
                Hay un porblema al enviar el mensaje. Envianos un correo a <b>info@planifica.org</b>
              </div>

            </div>
          </div>
          <div class="clear"></div>
          <div class="row">
            <div class="col-md-12">
              <p class="textarea-block" style="font-size: 11px;line-height: 20px;">
                Protección de datos personales: <b>PLANIFICA</b> le informa de que la recogida de sus datos de carácter personal se produce con los
                siguientes
                fines: (i) gestionar y tramitar cada uno de los servicios solicitados por el Usuario (ii) comerciales y publicitario respecto de productos,
                servicios,
                ofertas, promociones, y cualesquiera otras actividades publicitarias, y, (iii) realizar encuestas, estadísticas y análisis de mercado. El
                Usuario
                podrá ejercer los derechos de acceso, rectificación, cancelación y oposición de sus datos de carácter personal, en los términos establecidos en
                la
                LOPD, dirigiéndose por escrito a la siguiente dirección: PLANIFICA, C/San Vicente, 4 3º 12002 - Castellón, o bien enviando un email a la
                dirección
                info@planifica.org.
              </p>
            </div>
          </div>
        </form>
        <script type="text/javascript">
          $(document).ready(function () {
            $("#contacto").validate({
              submitHandler: function (form) {
                $('#span-terminos').removeClass('error')
                sendmail();
              },
              invalidHandler: function (event, validator) {
                if ($(this).find('#terminos').is(':checked')) {
                  $('#span-terminos').removeClass('error')
                } else {
                  $('#span-terminos').addClass('error')
                }
              }
            });
          });

          function sendmail() {
            $('#contacto-loading').show();
            var data = $('#contacto').serializeArray();
            var url = "{{ url('contacto')}}";
            $.ajax({
              cache: false,
              url: url,
              data: data,
              type: 'POST',
              success: function (data) {
                if (data.success == true) {
                  $('#contacto-loading').hide();
                  $('#contacto-fail').hide();
                  $('#contacto-success').show();
                  $('form')[0].reset();
                } else {
                  $('#contacto-loading').hide();
                  $('#contacto-fail').show();
                }
              },
              error: function (ajaxContext) {
                console.log(ajaxContext);
                alert(ajaxContext.responseText)
              }
            });
          }
          $(function () {
            $('#contact').addClass('current');

            $('#reserva-success').hide();
            $('#reserva-loading').hide();
            $('#reserva-massages').hide();

          });
        </script>
      </div>

    </div>
    <!--END: Default Form-->
    <hr class="space">

  </div>

</section>

@endsection