@extends('frontend.layout.base')

@section('contenido')
  <style>
    header {
      background: #5AB6B2 !important;
    }
  </style>
  <section class="p-t-20">
    <div class="container">

      <!-- Los valores de nuestros productos   -->
      <h4 class="about-title">Política de Cookies</h4>

      <div class="about-title-right">

        <h4></h4>

        <p>
          Cookie es un fichero que se descarga en su ordenador al acceder a determinadas páginas
          web. Las cookies permiten a una página web, entre otras cosas, almacenar y recuperar
          información sobre los hábitos de navegación de un usuario o de su equipo y, dependiendo de
          la información que contengan y de la forma en que utilice su equipo, pueden utilizarse
          para reconocer al usuario.. El navegador del usuario memoriza cookies en el disco duro
          solamente durante la sesión actual ocupando un espacio de memoria mínimo y no perjudicando
          al ordenador. Las cookies no contienen ninguna clase de información personal específica, y
          la mayoría de las mismas se borran del disco duro al finalizar la sesión de navegador (las
          denominadas cookies de sesión).
        </p>

        <p>
          La mayoría de los navegadores aceptan como estándar a las cookies y, con independencia de
          las mismas, permiten o impiden en los ajustes de seguridad las cookies temporales o
          memorizadas.
        </p>

        <p>
          Sin su expreso consentimiento –mediante la activación de las cookies en su navegador–
          <b>PLANIFICA</b> no enlazará en las cookies los datos memorizados con sus datos personales
          proporcionados en el momento del registro u en cualquier otro proceso incluido en la web.
        </p>


        <h4>¿Qué tipos de cookies utiliza esta página web?</h4>

        <p>
          <b>Cookies técnicas:</b> Son aquéllas que permiten al usuario la navegación a través de
          una página web, plataforma o aplicación y la utilización de las diferentes opciones o
          servicios que en ella existan como, por ejemplo, controlar el tráfico y la comunicación de
          datos, identificar la sesión, acceder a partes de acceso restringido, recordar los
          elementos que integran un pedido, realizar el proceso de compra de un pedido, realizar la
          solicitud de inscripción o participación en un evento, utilizar elementos de seguridad
          durante la navegación, almacenar contenidos para la difusión de videos o sonido o
          compartir contenidos a través de redes sociales.
        </p>

        <p>
          <b>Cookies de personalización:</b> Son aquéllas que permiten al usuario acceder al
          servicio con algunas características de carácter general predefinidas en función de una
          serie de criterios en el terminal del usuario como por ejemplo serian el idioma, el tipo
          de navegador a través del cual accede al servicio, la configuración regional desde donde
          accede al servicio, etc.
        </p>

        <p>
          <b>Cookies de análisis:</b> Son aquéllas que bien tratadas por nosotros o por terceros,
          nos permiten cuantificar el número de usuarios y así realizar la medición y análisis
          estadístico de la utilización que hacen los usuarios del servicio ofertado. Para ello se
          analiza su navegación en nuestra página web con el fin de mejorar la oferta de productos o
          servicios que le ofrecemos.
        </p>
        <!--
           <p>
          <b>Cookies publicitarias:</b> Son aquéllas que, bien tratadas por nosotros o por terceros, nos permiten gestionar de la forma más eficaz posible la oferta de los espacios publicitarios que hay en la página web, adecuando el contenido del anuncio al contenido del servicio solicitado o al uso que realice de nuestra página web. Para ello podemos analizar sus hábitos de navegación en Internet y podemos mostrarle publicidad relacionada con su perfil de navegación.
          </p>

          <p>
          <b>Cookies de publicidad comportamental:</b> Son aquéllas que permiten la gestión, de la forma más eficaz posible, de los espacios publicitarios que, en su caso, el editor haya incluido en una página web, aplicación o plataforma desde la que presta el servicio solicitado. Estas cookies almacenan información del comportamiento de los usuarios obtenida a través de la observación continuada de sus hábitos de navegación, lo que permite desarrollar un perfil específico para mostrar publicidad en función del mismo.
          </p>
      -->

        <p>
          <b>Cookies de terceros:</b> Los sitios web de <b>PLANIFICA</b> pueden utilizar
          servicios de terceros que, por cuenta de <b>PLANIFICA</b>, recopilaran información con
          fines estadísticos, de uso del sitio web por parte del usuario y para la prestacion de
          otros servicios relacionados con la actividad del sitio web y otros servicios de Internet.
        </p>

        <p>
          En particular, <u>este sitio web utiliza Google Analytics y Google Maps</u>, ambos
          servicios web prestados por Google, Inc. con domicilio en los Estados Unidos con sede
          central en 1600 Amphitheatre Parkway, Mountain View, California 94043. Para la prestación
          de estos servicios, estos utilizan cookies que recopilan la información, incluida la
          dirección IP del usuario, que será transmitida, tratada y almacenada por Google en los
          términos fijados en la Web Google.com. Incluyendo la posible transmisión de dicha
          información a terceros por razones de exigencia legal o cuando dichos terceros procesen la
          información por cuenta de Google.
        </p>

        <p>
          <b><i><u>El Usuario acepta expresamente, por la utilización de este sitio web, el
                tratamiento de la información recabada en la forma y con los fines anteriormente
                mencionados.</u></i></b> Y asimismo reconoce conocer la posibilidad de rechazar el
          tratamiento de tales datos o información rechazando el uso de Cookies mediante la
          selección de la configuración apropiada a tal fin en su navegador. Si bien esta opción de
          bloqueo de Cookies en su navegador puede no permitirle el uso pleno de todas las
          funcionalidades del sitio web.
        </p>
        <h4>¿Cómo rechazar las cookies?</h4>

        <p>
          Puede usted permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la
          configuración de las opciones del navegador instalado en su ordenador:
        </p>
        <ul style="margin-left:50px">
          <li>
            <u><a href="http://windows.microsoft.com/en-us/windows-vista/block-or-allow-cookies">Microsoft
                Windows Explorer</a></u>
          </li>
          <li>
            <u><a href="https://support.google.com/chrome/bin/answer.py?hl=en&amp;answer=95647&amp;p=cpn_cookies">Google
                Chrome</a></u>
          </li>
          <li>
            <u><a href="http://support.mozilla.org/en-US/kb/Enabling%20and%20disabling%20cookies">Mozilla
                Firefox</a></u>
          </li>
          <li>
            <u><a href="http://docs.info.apple.com/article.html?path=Safari/5.0/en/9277.html">Apple
                Safari</a></u><br>
          </li>
        </ul>
        <p>
          Si tiene dudas sobre esta política de cookies, puede contactar con nosotros en
          info@planifica.org
        </p>

        <h4>¿Qué ocurre si se deshabilitan las Cookies?</h4>

        <p>
          En el caso de bloquear o no aceptar la instalación de cookies es posible que ciertos
          servicios ofrecidos por nuestro sitio web que necesitan su uso queden deshabilitados y,
          por lo tanto, no estén disponibles para ústed por lo que no podrá aprovechar por completo
          todo lo que nuestras webs y aplicaciones le ofrecen. Es posible también que la calidad de
          funcionamiento de la página web pueda disminuir.
        </p>


      </div>
  </section>
  <!-- END: FULL WIDTH PAGE -->

@endsection