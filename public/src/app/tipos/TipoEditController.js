tiposControllers.controller('TipoEditController', function($scope, $rootScope, $location, $routeParams, Tipo, areas, sigoModal) {

    $scope.updateTipo = function(tipos) {
        console.log('Actualizada tipos.', $scope.tipo);
        tipos.$update().then(function(response) {
            $rootScope.setForm();
            $location.path(CONFIG.URL.TIPOS);
        }, function() {
            sigoModal.alerta('No se ha podido guardar el tipo', 'Aceptar');
        });
    };

    $scope.cancelar = function() {
        $location.path(CONFIG.URL.TIPOS);
    };

    $scope.areas = areas;
    $scope.textoBoton = "EDITAR";
    $scope.tipo = Tipo.get({
        id: $routeParams.id
    });
});