<?php
class NoticiaController extends Controller
{
    private function persistirProyectoIdi($proyectoIdi) {
        $inputs = Input::all();
        foreach ($inputs as $key => $value) {
            $proyectoIdi->$key = $value;
        }
        $proyectoIdi->save();
        return $proyectoIdi;
    }

    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index() {
        return ProyectoIdi::all();
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store() {
        return ProyectoIdi::create(Input::all());
    }
    
    /**
    * Display the specified resource.
    *
        * @param  int  $id
    * @return Response
    */
    public function show($id) {
        return ProyectoIdi::find($id);
    }
    
    /**
    * Update the specified resource in storage.
    *
        * @param  int  $id
    * @return Response
    */
    public function update($id) {
        $proyectoIdi = ProyectoIdi::find($id);
        $proyectoIdi->update(Input::all());
    }
    
    /**
    * Remove the specified resource from storage.
    *
        * @param  int  $id
    * @return Response
    */
    public function destroy($id) {
        $proyectoIdi = ProyectoIdi::find($id)->delete();
    }
}