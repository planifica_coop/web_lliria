<?php
class AreaController extends Controller {

    public function index() {
        $respuesta = Area::with('tipos');
        if (Input::has('include')) {
            $includes = explode(',',Input::get('include'));
            foreach ($includes as $include) {
                $respuesta->with($include);
            }
        }
        return $respuesta->get();
    }
    
    public function store() {
        return Area::create(Input::all());
    }
    
    public function show($id) {
        return Area::find($id);
    }
    
    public function update($id) {
        $area = Area::find($id);
        $area->update(Input::all());
    }
    
    public function destroy($id) {
        $area = Area::find($id)->delete();
    }
}
