@extends('base')
@section('css')
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
@stop
@section('content')
@include('barra-navegacion')

<!-- Region 4 Wrap -->
<div class="container region4wrap">
  <div class="row maincontent contact">
    <div class="twelve columns">  
      <div class="page_title">
        <div class="row">
          <div class="twelve columns">
            <h1>{{ ucfirst(trans('nombres.contacto')) }}</h1>
          </div>
        </div>
      </div>
    </div>
    <div class="twelve columns">          
      <div class="map_location">
        <div id="map_canvas"></div>
      </div>        
    </div>
    <div class="four columns">
      <h3>{{ ucfirst(trans('nombres.oficina_central')) }}</h3>
      <p>{{ ucfirst(trans('textos.oficina_central_mas_info')) }}</p>
        <div class="contact-details">
          <p><span>{{ ucfirst(trans('nombres.direccion')) }}:</span> C/ San Vicente, Nº 4, 3, 12002, Castellón</p>
          <p><span>{{ ucfirst(trans('nombres.telefono')) }}:</span> +34 964 061 256</p>
          <p><span>Fax:</span> +34 964 061 265</p>
          <p><span>{{ ucfirst(trans('nombres.email')) }}:</span> info@planifica.org</p>
        </div> 
    </div>
   <div class="eight columns">
      <h3>{{ ucfirst(trans('nombres.envianos_un_mensaje')) }}</h3>
      <div id="respuesta">
    </div>
      <form id="formulario">
        <div class="row">
          <div class="four columns">
          <div class="name-field">
            <label >{{ ucfirst(trans('nombres.nombre')) }}</label>
            <input type="text" name="nombre" required/>
            </div>
          </div>
          <div class="four columns">
          <div class="email-field">
            <label>{{ ucfirst(trans('nombres.email')) }}</label>
            <input type="email" name="email" required/>
            </div>
          </div>
          <div class="four columns">
            <label>{{ ucfirst(trans('nombres.asunto')) }}</label>
            <input type="text" name="asunto" required/>
          </div>
        </div>
        <div class="row">
          <div class="twelve columns">
            <label>{{ ucfirst(trans('nombres.mensaje')) }}</label>
            <textarea name="mensaje" required></textarea>
          </div>
        </div>
        <button type="submit" class="medium button">{{ ucfirst(trans('nombres.enviar')) }}</button>
    </form>
   </div>
  </div>
</div>
<!-- End Region 4 Wrap -->
@stop

@section('javascript')
<script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
<script>
    var map = L.map('map_canvas').setView([39.9853881, -0.0411603], 16);
    var marker = L.marker([39.9853881, -0.0411603]).addTo(map);
    L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
      maxZoom: 18,
      attribution: '',
      id: 'examples.map-i86knfo3'
    }).addTo(map);
    $( "#formulario" ).submit(function( event ) {
    event.preventDefault();
    $.ajax({
        url: '{{URL::to('contacto/contacto')}}',
        type: 'post',
        dataType: 'json',
        data: $('#formulario').serialize(),
        success: function(data) {
          if (data) {
            $("#respuesta").html('<div data-alert class="alert-box success">El mensaje ha sido enviado.<a href="#" class="close">&times;</a></div>');
          } else {
            $("#respuesta").html('<div data-alert class="alert-box alert">No se ha podido enviar el mensaje.<a href="#" class="close">&times;</a></div>');
          }
        }
    });
});
</script>
@stop