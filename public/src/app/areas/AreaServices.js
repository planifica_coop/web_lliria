var areasServices = angular.module('areas.services', ['ngResource']);

areasServices.factory('Area', function($resource) {
    return $resource(CONFIG.URL.API + '/areas/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        }
    });
});