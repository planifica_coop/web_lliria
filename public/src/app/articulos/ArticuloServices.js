var articulosServices = angular.module('articulos.services', ['ngResource']);

articulosServices.factory('Articulo', function($resource) {
    return $resource(CONFIG.URL.API + '/articulos/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        }
    });
});

articulosServices.factory('Adjunto', function($resource) {
    return $resource(CONFIG.URL.API + '/adjuntos/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        }
    });
});